﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnAddAuthor_Click(object sender, EventArgs e)
        {
            String name, firstName;
            int spaceId = tbName.Text.IndexOf(' ');
            if (spaceId > 0)
            {
                firstName = tbName.Text.Substring(0, spaceId + 1);
                name = tbName.Text.Substring(spaceId+1);
            }
            else
            {
                firstName = String.Empty;
                name = tbName.Text;
            }

            DataClasses1DataContext db = new DataClasses1DataContext();

            Autor autor = new Autor();
            autor.Nazwisko = name;
            autor.Imie = firstName;

            db.Autors.InsertOnSubmit(autor);
            db.SubmitChanges();
        }

        private void btnLoadAuthors_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            tbResult.Clear();
            StringBuilder sb = new StringBuilder();
            foreach (Autor autor in db.Autors)
            {
                sb.Append(autor.Imie);
                sb.Append("\t");
                sb.Append(autor.Nazwisko);
                sb.AppendLine();
            }
            tbResult.Text = sb.ToString();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            var results =
                from a in db.Autors
                where a.Nazwisko.StartsWith(tbNameStarts.Text)
                select a;

            tbResult.Clear();
            StringBuilder sb = new StringBuilder();
            foreach (Autor autor in results)
            {
                sb.Append(autor.Imie);
                sb.Append("\t");
                sb.Append(autor.Nazwisko);
                sb.AppendLine();
            }
            tbResult.Text = sb.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            int n = 0;
            foreach (Autor autor in db.Autors)
            {
                if (autor.Nazwisko.Contains(' '))
                {
                    autor.Nazwisko = autor.Nazwisko.TrimStart();
                    ++n;
                }
            }
            if (n > 0)
            {
                db.SubmitChanges();
                MessageBox.Show("Usunieto spacje z " + n + " nazwisk.");
            }
            else
                MessageBox.Show("Nie znaleziono spacji wiodących w nazwiskach");
        }
    }
}
