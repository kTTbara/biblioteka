﻿namespace Biblioteka
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bIBLIOTEKADataSet = new Biblioteka.BIBLIOTEKADataSet();
            this.bIBLIOTEKADataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.autorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.autorTableAdapter = new Biblioteka.BIBLIOTEKADataSetTableAdapters.AutorTableAdapter();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.btnExecute = new System.Windows.Forms.Button();
            this.btnAddAuthor = new System.Windows.Forms.Button();
            this.btnLoadAuthors = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNameStarts = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bIBLIOTEKADataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bIBLIOTEKADataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.autorBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // bIBLIOTEKADataSet
            // 
            this.bIBLIOTEKADataSet.DataSetName = "BIBLIOTEKADataSet";
            this.bIBLIOTEKADataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bIBLIOTEKADataSetBindingSource
            // 
            this.bIBLIOTEKADataSetBindingSource.DataSource = this.bIBLIOTEKADataSet;
            this.bIBLIOTEKADataSetBindingSource.Position = 0;
            // 
            // autorBindingSource
            // 
            this.autorBindingSource.DataMember = "Autor";
            this.autorBindingSource.DataSource = this.bIBLIOTEKADataSet;
            // 
            // autorTableAdapter
            // 
            this.autorTableAdapter.ClearBeforeFill = true;
            // 
            // tbResult
            // 
            this.tbResult.Location = new System.Drawing.Point(155, 43);
            this.tbResult.Multiline = true;
            this.tbResult.Name = "tbResult";
            this.tbResult.Size = new System.Drawing.Size(438, 188);
            this.tbResult.TabIndex = 0;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(212, 308);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(191, 20);
            this.tbName.TabIndex = 1;
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(409, 385);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(75, 23);
            this.btnExecute.TabIndex = 2;
            this.btnExecute.Text = "Wykonaj";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // btnAddAuthor
            // 
            this.btnAddAuthor.Location = new System.Drawing.Point(409, 305);
            this.btnAddAuthor.Name = "btnAddAuthor";
            this.btnAddAuthor.Size = new System.Drawing.Size(91, 23);
            this.btnAddAuthor.TabIndex = 3;
            this.btnAddAuthor.Text = "Dodaj autora";
            this.btnAddAuthor.UseVisualStyleBackColor = true;
            this.btnAddAuthor.Click += new System.EventHandler(this.btnAddAuthor_Click);
            // 
            // btnLoadAuthors
            // 
            this.btnLoadAuthors.Location = new System.Drawing.Point(599, 208);
            this.btnLoadAuthors.Name = "btnLoadAuthors";
            this.btnLoadAuthors.Size = new System.Drawing.Size(97, 23);
            this.btnLoadAuthors.TabIndex = 4;
            this.btnLoadAuthors.Text = "Wczytaj autorów";
            this.btnLoadAuthors.UseVisualStyleBackColor = true;
            this.btnLoadAuthors.Click += new System.EventHandler(this.btnLoadAuthors_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(212, 289);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Wpisz imię i nazwisko autora:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 370);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(201, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Autorzy o nazwisku zaczynającym się od:";
            // 
            // tbNameStarts
            // 
            this.tbNameStarts.Location = new System.Drawing.Point(215, 385);
            this.tbNameStarts.Name = "tbNameStarts";
            this.tbNameStarts.Size = new System.Drawing.Size(188, 20);
            this.tbNameStarts.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(614, 348);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 57);
            this.button1.TabIndex = 8;
            this.button1.Text = "Usuń wiodące spacje z nazwisk autorów";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbNameStarts);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLoadAuthors);
            this.Controls.Add(this.btnAddAuthor);
            this.Controls.Add(this.btnExecute);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.tbResult);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bIBLIOTEKADataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bIBLIOTEKADataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.autorBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bIBLIOTEKADataSetBindingSource;
        private BIBLIOTEKADataSet bIBLIOTEKADataSet;
        private System.Windows.Forms.BindingSource autorBindingSource;
        private BIBLIOTEKADataSetTableAdapters.AutorTableAdapter autorTableAdapter;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.Button btnAddAuthor;
        private System.Windows.Forms.Button btnLoadAuthors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbNameStarts;
        private System.Windows.Forms.Button button1;
    }
}

