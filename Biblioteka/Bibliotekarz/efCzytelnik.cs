//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bibliotekarz
{
    using System;
    using System.Collections.Generic;
    
    public partial class efCzytelnik
    {
        public efCzytelnik()
        {
            this.Wypozyczenie = new HashSet<efWypozyczenie>();
            this.Kara = new HashSet<efKara>();
            this.Rezerwacja = new HashSet<efRezerwacja>();
        }
    
        public int IdCzytelnika { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public Nullable<System.DateTime> DataUrodzenia { get; set; }
        public string Plec { get; set; }
        public string Miasto { get; set; }
        public string Ulica { get; set; }
        public string NrPosesji { get; set; }
        public string Email { get; set; }
        public string NrTelefonu { get; set; }
        public Nullable<System.DateTime> DataRejestracji { get; set; }
    
        public virtual ICollection<efWypozyczenie> Wypozyczenie { get; set; }
        public virtual ICollection<efKara> Kara { get; set; }
        public virtual ICollection<efRezerwacja> Rezerwacja { get; set; }
    }
}
