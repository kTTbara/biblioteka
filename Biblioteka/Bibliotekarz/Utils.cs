﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public class Utils
    {

        public static void ShowExceptionMsgBox(Exception ex)
        {
            MessageBox.Show(
                "Złapano wyjątek:\n" + ex.Message + "\n\n" + ex.StackTrace,
                "Wyjątek", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowExclamationMsgBox(String text, String caption = "")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public static void ShowInfoMsgBox(String text, String caption = "Informacja")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void ShowErrorMsgBox(String text, String caption = "Błąd")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Oblicz różnicę data w latach które upłyneły od daty firstDate
        /// (starsza data) do daty secondDate (młodsza).
        /// </summary>
        public static int DatesDiffInYears(DateTime firstDate, DateTime secondDate)
        {
            int years = secondDate.Year - firstDate.Year;
            if (firstDate > secondDate.AddYears(-years)) years--;
            return years;
        }
    }
}
