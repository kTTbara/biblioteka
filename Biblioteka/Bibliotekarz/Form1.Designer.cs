﻿namespace Bibliotekarz
{
    partial class App
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabCtrl = new System.Windows.Forms.TabControl();
            this.tabSummary = new System.Windows.Forms.TabPage();
            this.tabLibrarians = new System.Windows.Forms.TabPage();
            this.tabNewLibrarian = new System.Windows.Forms.TabPage();
            this.tabReaders = new System.Windows.Forms.TabPage();
            this.tabRegisterReader = new System.Windows.Forms.TabPage();
            this.tabBorrowBook = new System.Windows.Forms.TabPage();
            this.tabProlongBorrow = new System.Windows.Forms.TabPage();
            this.tabReturnBook = new System.Windows.Forms.TabPage();
            this.tabTitles = new System.Windows.Forms.TabPage();
            this.tabNewEdition = new System.Windows.Forms.TabPage();
            this.tabEditions = new System.Windows.Forms.TabPage();
            this.tabPublishers = new System.Windows.Forms.TabPage();
            this.tabAuthors = new System.Windows.Forms.TabPage();
            this.tabAddBooks = new System.Windows.Forms.TabPage();
            this.tabBooksByPublisher = new System.Windows.Forms.TabPage();
            this.tabTitleEditionsLastBorrow = new System.Windows.Forms.TabPage();
            this.tabReadersSummary = new System.Windows.Forms.TabPage();
            this.tabBorrowsSummary = new System.Windows.Forms.TabPage();
            this.tabExportBorrows = new System.Windows.Forms.TabPage();
            this.tabRepairBook = new System.Windows.Forms.TabPage();
            this.tabCtrl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCtrl
            // 
            this.tabCtrl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabCtrl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabCtrl.Controls.Add(this.tabSummary);
            this.tabCtrl.Controls.Add(this.tabLibrarians);
            this.tabCtrl.Controls.Add(this.tabNewLibrarian);
            this.tabCtrl.Controls.Add(this.tabReaders);
            this.tabCtrl.Controls.Add(this.tabRegisterReader);
            this.tabCtrl.Controls.Add(this.tabBorrowBook);
            this.tabCtrl.Controls.Add(this.tabProlongBorrow);
            this.tabCtrl.Controls.Add(this.tabReturnBook);
            this.tabCtrl.Controls.Add(this.tabTitles);
            this.tabCtrl.Controls.Add(this.tabNewEdition);
            this.tabCtrl.Controls.Add(this.tabEditions);
            this.tabCtrl.Controls.Add(this.tabPublishers);
            this.tabCtrl.Controls.Add(this.tabAuthors);
            this.tabCtrl.Controls.Add(this.tabAddBooks);
            this.tabCtrl.Controls.Add(this.tabRepairBook);
            this.tabCtrl.Controls.Add(this.tabBooksByPublisher);
            this.tabCtrl.Controls.Add(this.tabTitleEditionsLastBorrow);
            this.tabCtrl.Controls.Add(this.tabReadersSummary);
            this.tabCtrl.Controls.Add(this.tabBorrowsSummary);
            this.tabCtrl.Controls.Add(this.tabExportBorrows);
            this.tabCtrl.Location = new System.Drawing.Point(5, 5);
            this.tabCtrl.Multiline = true;
            this.tabCtrl.Name = "tabCtrl";
            this.tabCtrl.SelectedIndex = 0;
            this.tabCtrl.Size = new System.Drawing.Size(1018, 565);
            this.tabCtrl.TabIndex = 0;
            // 
            // tabSummary
            // 
            this.tabSummary.Location = new System.Drawing.Point(4, 73);
            this.tabSummary.Name = "tabSummary";
            this.tabSummary.Padding = new System.Windows.Forms.Padding(3);
            this.tabSummary.Size = new System.Drawing.Size(1010, 488);
            this.tabSummary.TabIndex = 15;
            this.tabSummary.Text = "Biblioteka";
            this.tabSummary.UseVisualStyleBackColor = true;
            // 
            // tabLibrarians
            // 
            this.tabLibrarians.Location = new System.Drawing.Point(4, 25);
            this.tabLibrarians.Name = "tabLibrarians";
            this.tabLibrarians.Padding = new System.Windows.Forms.Padding(3);
            this.tabLibrarians.Size = new System.Drawing.Size(1010, 536);
            this.tabLibrarians.TabIndex = 0;
            this.tabLibrarians.Text = "Bibliotekarze";
            this.tabLibrarians.UseVisualStyleBackColor = true;
            // 
            // tabNewLibrarian
            // 
            this.tabNewLibrarian.Location = new System.Drawing.Point(4, 25);
            this.tabNewLibrarian.Name = "tabNewLibrarian";
            this.tabNewLibrarian.Padding = new System.Windows.Forms.Padding(3);
            this.tabNewLibrarian.Size = new System.Drawing.Size(1010, 536);
            this.tabNewLibrarian.TabIndex = 1;
            this.tabNewLibrarian.Text = "Zatrudnij bibliotekarza";
            this.tabNewLibrarian.UseVisualStyleBackColor = true;
            // 
            // tabReaders
            // 
            this.tabReaders.Location = new System.Drawing.Point(4, 25);
            this.tabReaders.Name = "tabReaders";
            this.tabReaders.Padding = new System.Windows.Forms.Padding(3);
            this.tabReaders.Size = new System.Drawing.Size(1010, 536);
            this.tabReaders.TabIndex = 14;
            this.tabReaders.Text = "Czytelnicy";
            this.tabReaders.UseVisualStyleBackColor = true;
            // 
            // tabRegisterReader
            // 
            this.tabRegisterReader.Location = new System.Drawing.Point(4, 25);
            this.tabRegisterReader.Name = "tabRegisterReader";
            this.tabRegisterReader.Padding = new System.Windows.Forms.Padding(3);
            this.tabRegisterReader.Size = new System.Drawing.Size(1010, 536);
            this.tabRegisterReader.TabIndex = 2;
            this.tabRegisterReader.Text = "Rejestracja czytelnika";
            this.tabRegisterReader.UseVisualStyleBackColor = true;
            // 
            // tabBorrowBook
            // 
            this.tabBorrowBook.Location = new System.Drawing.Point(4, 25);
            this.tabBorrowBook.Name = "tabBorrowBook";
            this.tabBorrowBook.Padding = new System.Windows.Forms.Padding(3);
            this.tabBorrowBook.Size = new System.Drawing.Size(1010, 536);
            this.tabBorrowBook.TabIndex = 3;
            this.tabBorrowBook.Text = "Wypożyczenie";
            this.tabBorrowBook.UseVisualStyleBackColor = true;
            // 
            // tabProlongBorrow
            // 
            this.tabProlongBorrow.Location = new System.Drawing.Point(4, 25);
            this.tabProlongBorrow.Name = "tabProlongBorrow";
            this.tabProlongBorrow.Size = new System.Drawing.Size(1010, 536);
            this.tabProlongBorrow.TabIndex = 19;
            this.tabProlongBorrow.Text = "Przedłużenie wypożyczenia";
            this.tabProlongBorrow.UseVisualStyleBackColor = true;
            // 
            // tabReturnBook
            // 
            this.tabReturnBook.Location = new System.Drawing.Point(4, 25);
            this.tabReturnBook.Name = "tabReturnBook";
            this.tabReturnBook.Padding = new System.Windows.Forms.Padding(3);
            this.tabReturnBook.Size = new System.Drawing.Size(1010, 536);
            this.tabReturnBook.TabIndex = 13;
            this.tabReturnBook.Text = "Zwrot książki";
            this.tabReturnBook.UseVisualStyleBackColor = true;
            // 
            // tabTitles
            // 
            this.tabTitles.Location = new System.Drawing.Point(4, 25);
            this.tabTitles.Name = "tabTitles";
            this.tabTitles.Padding = new System.Windows.Forms.Padding(3);
            this.tabTitles.Size = new System.Drawing.Size(1010, 536);
            this.tabTitles.TabIndex = 10;
            this.tabTitles.Text = "Tytuły";
            this.tabTitles.UseVisualStyleBackColor = true;
            // 
            // tabNewEdition
            // 
            this.tabNewEdition.Location = new System.Drawing.Point(4, 25);
            this.tabNewEdition.Name = "tabNewEdition";
            this.tabNewEdition.Padding = new System.Windows.Forms.Padding(3);
            this.tabNewEdition.Size = new System.Drawing.Size(1010, 536);
            this.tabNewEdition.TabIndex = 4;
            this.tabNewEdition.Text = "Dodaj wydanie";
            this.tabNewEdition.UseVisualStyleBackColor = true;
            // 
            // tabEditions
            // 
            this.tabEditions.Location = new System.Drawing.Point(4, 25);
            this.tabEditions.Name = "tabEditions";
            this.tabEditions.Padding = new System.Windows.Forms.Padding(3);
            this.tabEditions.Size = new System.Drawing.Size(1010, 536);
            this.tabEditions.TabIndex = 8;
            this.tabEditions.Text = "Wydania";
            this.tabEditions.UseVisualStyleBackColor = true;
            // 
            // tabPublishers
            // 
            this.tabPublishers.Location = new System.Drawing.Point(4, 49);
            this.tabPublishers.Name = "tabPublishers";
            this.tabPublishers.Padding = new System.Windows.Forms.Padding(3);
            this.tabPublishers.Size = new System.Drawing.Size(1010, 512);
            this.tabPublishers.TabIndex = 6;
            this.tabPublishers.Text = "Wydawcy";
            this.tabPublishers.UseVisualStyleBackColor = true;
            // 
            // tabAuthors
            // 
            this.tabAuthors.Location = new System.Drawing.Point(4, 49);
            this.tabAuthors.Name = "tabAuthors";
            this.tabAuthors.Padding = new System.Windows.Forms.Padding(3);
            this.tabAuthors.Size = new System.Drawing.Size(1010, 512);
            this.tabAuthors.TabIndex = 11;
            this.tabAuthors.Text = "Autorzy";
            this.tabAuthors.UseVisualStyleBackColor = true;
            // 
            // tabAddBooks
            // 
            this.tabAddBooks.Location = new System.Drawing.Point(4, 49);
            this.tabAddBooks.Name = "tabAddBooks";
            this.tabAddBooks.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddBooks.Size = new System.Drawing.Size(1010, 512);
            this.tabAddBooks.TabIndex = 7;
            this.tabAddBooks.Text = "Dodaj książki";
            this.tabAddBooks.UseVisualStyleBackColor = true;
            // 
            // tabBooksByPublisher
            // 
            this.tabBooksByPublisher.Location = new System.Drawing.Point(4, 49);
            this.tabBooksByPublisher.Name = "tabBooksByPublisher";
            this.tabBooksByPublisher.Padding = new System.Windows.Forms.Padding(3);
            this.tabBooksByPublisher.Size = new System.Drawing.Size(1010, 512);
            this.tabBooksByPublisher.TabIndex = 9;
            this.tabBooksByPublisher.Text = "Zestawienie wydawnictw wg wydanych tytułów";
            this.tabBooksByPublisher.UseVisualStyleBackColor = true;
            // 
            // tabTitleEditionsLastBorrow
            // 
            this.tabTitleEditionsLastBorrow.Location = new System.Drawing.Point(4, 49);
            this.tabTitleEditionsLastBorrow.Name = "tabTitleEditionsLastBorrow";
            this.tabTitleEditionsLastBorrow.Padding = new System.Windows.Forms.Padding(3);
            this.tabTitleEditionsLastBorrow.Size = new System.Drawing.Size(1010, 512);
            this.tabTitleEditionsLastBorrow.TabIndex = 12;
            this.tabTitleEditionsLastBorrow.Text = "Zestawienie tytułów wg wypożyczeń";
            this.tabTitleEditionsLastBorrow.UseVisualStyleBackColor = true;
            // 
            // tabReadersSummary
            // 
            this.tabReadersSummary.Location = new System.Drawing.Point(4, 49);
            this.tabReadersSummary.Name = "tabReadersSummary";
            this.tabReadersSummary.Padding = new System.Windows.Forms.Padding(3);
            this.tabReadersSummary.Size = new System.Drawing.Size(1010, 512);
            this.tabReadersSummary.TabIndex = 16;
            this.tabReadersSummary.Text = "Statystyki czytelników";
            this.tabReadersSummary.UseVisualStyleBackColor = true;
            // 
            // tabBorrowsSummary
            // 
            this.tabBorrowsSummary.Location = new System.Drawing.Point(4, 49);
            this.tabBorrowsSummary.Name = "tabBorrowsSummary";
            this.tabBorrowsSummary.Padding = new System.Windows.Forms.Padding(3);
            this.tabBorrowsSummary.Size = new System.Drawing.Size(1010, 512);
            this.tabBorrowsSummary.TabIndex = 17;
            this.tabBorrowsSummary.Text = "Statystyki wypożyczeń";
            this.tabBorrowsSummary.UseVisualStyleBackColor = true;
            // 
            // tabExportBorrows
            // 
            this.tabExportBorrows.Location = new System.Drawing.Point(4, 73);
            this.tabExportBorrows.Name = "tabExportBorrows";
            this.tabExportBorrows.Padding = new System.Windows.Forms.Padding(3);
            this.tabExportBorrows.Size = new System.Drawing.Size(1010, 488);
            this.tabExportBorrows.TabIndex = 18;
            this.tabExportBorrows.Text = "Eksport danych o wypożyczeniach";
            this.tabExportBorrows.UseVisualStyleBackColor = true;
            // 
            // tabRepairBook
            // 
            this.tabRepairBook.Location = new System.Drawing.Point(4, 49);
            this.tabRepairBook.Name = "tabRepairBook";
            this.tabRepairBook.Padding = new System.Windows.Forms.Padding(3);
            this.tabRepairBook.Size = new System.Drawing.Size(1010, 512);
            this.tabRepairBook.TabIndex = 20;
            this.tabRepairBook.Text = "Reperacja książki";
            this.tabRepairBook.UseVisualStyleBackColor = true;
            // 
            // App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 582);
            this.Controls.Add(this.tabCtrl);
            this.Name = "App";
            this.Text = "Biblioteka: bibliotekarz";
            this.tabCtrl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCtrl;
        private System.Windows.Forms.TabPage tabLibrarians;
        private System.Windows.Forms.TabPage tabNewLibrarian;
        private System.Windows.Forms.TabPage tabRegisterReader;
        private System.Windows.Forms.TabPage tabBorrowBook;
        private System.Windows.Forms.TabPage tabNewEdition;
        private System.Windows.Forms.TabPage tabPublishers;
        private System.Windows.Forms.TabPage tabAddBooks;
        private System.Windows.Forms.TabPage tabEditions;
        private System.Windows.Forms.TabPage tabBooksByPublisher;
        private System.Windows.Forms.TabPage tabTitles;
        private System.Windows.Forms.TabPage tabAuthors;
        private System.Windows.Forms.TabPage tabTitleEditionsLastBorrow;
        private System.Windows.Forms.TabPage tabReturnBook;
        private System.Windows.Forms.TabPage tabReaders;
        private System.Windows.Forms.TabPage tabSummary;
        private System.Windows.Forms.TabPage tabReadersSummary;
        private System.Windows.Forms.TabPage tabBorrowsSummary;
        private System.Windows.Forms.TabPage tabExportBorrows;
        private System.Windows.Forms.TabPage tabProlongBorrow;
        private System.Windows.Forms.TabPage tabRepairBook;
    }
}

