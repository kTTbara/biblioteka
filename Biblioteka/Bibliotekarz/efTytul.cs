//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bibliotekarz
{
    using System;
    using System.Collections.Generic;
    
    public partial class efTytul
    {
        public efTytul()
        {
            this.Wydanie = new HashSet<efWydanie>();
        }
    
        public int IdTytulu { get; set; }
        public string NazwaTytulu { get; set; }
    
        public virtual ICollection<efWydanie> Wydanie { get; set; }
    }
}
