﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class WindowWithContent : Form
    {
        /// <summary>
        /// Proste okno wyświetlające podaną zawartość oraz przecisk potwierdzenia.
        /// Może służyć do wyboru danych. </summary>
        public WindowWithContent(Control content)
        {
            InitializeComponent();
            this.panelContent.Controls.Add(content);
            Content = content;
        }

        public Control Content { get; protected set; }

        private void btnConfirm_Click(object sender, EventArgs e)
        {

        }
    }
}
