﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class RepairBooksForm : UserControl
    {
        public RepairBooksForm()
        {
            InitializeComponent();
            ClearForms();
        }

        int BookId;


        private void btnConfirm_Click(object sender, EventArgs e)
        {
            int NewCondition = cbCondition.SelectedIndex;

            if (BookId < 1)
                Utils.ShowExclamationMsgBox("Wpisz identyfikator egzemplarza książki do naprawy.");
            else if (NewCondition == -1)
                Utils.ShowExclamationMsgBox("Oceń stan ksiażki po naprawie.");

            else
            {
                // Wstawienie danych o naprawie książki oraz uaktualnienie
                // stanu ksiażki używając transakcji w Ado.Net Entity Framework.
                // Dane zostały sprawdzone wcześniej.
                using (BiblEntities entities = new BiblEntities())
                {
                    try
                    {
                        // wstawienie informacji o naprawie
                        efReperacja reperacja = new efReperacja();
                        reperacja.IdEgzemplarza = BookId;
                        reperacja.Data = DateTime.Now;
                        entities.Reperacja.Add(reperacja);

                        // uaktualnienie stanu
                        var naprawianaKsiazka = entities.Egzemplarz
                            .Where(eg => eg.IdEgzemplarza == BookId)
                            .FirstOrDefault();
                        naprawianaKsiazka.Stan = (byte) NewCondition;

                        // przeprowadzenie transakcji
                        entities.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Utils.ShowExceptionMsgBox(ex);
                        Utils.ShowExclamationMsgBox(
                            "Nie udało się wstawić informacji o reperacji książki.");
                        return;
                    }
                }

                Utils.ShowInfoMsgBox("Wstawiono informacje o reperacji ksiązki.");
                ClearForms();
            }
        }

        private void ClearForms()
        {
            this.tbBookId.Text = String.Empty;
            this.cbCondition.SelectedIndex = -1;
            this.labBookInfo.Text = String.Empty;
        }

        private void tbBookId_Leave(object sender, EventArgs ev)
        {
            
            int parsedId;
            if (!Int32.TryParse(tbBookId.Text, out parsedId))
            {
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id książki.");
                BookId = 0;
                this.tbBookId.Text = string.Empty;
            }

            else
            {
                BookId = parsedId;

                /* wczytanie informacji o książce */
                using (BiblEntities entities = new BiblEntities())
                {
                    var bookInfo = (from e in entities.Egzemplarz
                                    where e.IdEgzemplarza == BookId
                                    select new
                                    {
                                        IdEgzemplarza = e.IdEgzemplarza,
                                        Tytuł = e.Wydanie.Tytul.NazwaTytulu,
                                        ISBN = e.Wydanie.ISBN,
                                        Stan = e.Stan,
                                        // data zwrotu ostatniego wypozyczenia:
                                        // jeśli równa null, tzn. że nie ma książki
                                        // w bibliotece -> nie mozna reperować
                                        OstatniZwrot = e.Wypozyczenie
                                            .OrderByDescending(w => w.IdWypozyczenia)
                                            .Select(w => w.DataZwrotu)
                                            .FirstOrDefault()
                                    }).FirstOrDefault();

                    if (bookInfo == null)
                    {
                        Utils.ShowExclamationMsgBox(
                            "Nie udało się znaleźć książki o id = " + BookId);
                        BookId = 0;
                        tbBookId.Text = String.Empty;
                        return;
                    }

                    if (bookInfo.OstatniZwrot == null)
                    {
                        Utils.ShowExclamationMsgBox(
                            "Książka o podanym id jest wypożyczona.");
                        BookId = 0;
                        tbBookId.Text = String.Empty;
                        return;
                    }

                    this.labBookInfo.Text = String.Format("[{0}] {1} {2} Stan: {3}",
                        bookInfo.IdEgzemplarza, bookInfo.Tytuł,
                        bookInfo.ISBN, bookInfo.Stan);
                }
            }
        }
    }
}
