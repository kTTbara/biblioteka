﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bibliotekarz
{
    public class Generator
    {
        static Generator()
        {
            DataPath = "..\\generator\\";
            Cities = System.IO.File.ReadAllLines(DataPath + "miasta.txt");
            // cities[0] ma być Poznań
            Streets = new String[Cities.Length][];

            for (int c = 0; c < Cities.Length; c++)
                Streets[c] = System.IO.File.ReadAllLines(DataPath + Cities[c] + ".txt");

            NamesMale = System.IO.File.ReadAllLines("..\\generator\\imiona-m.txt");
            NamesFemale = System.IO.File.ReadAllLines("..\\generator\\imiona-k.txt");
            Names = System.IO.File.ReadAllLines("..\\generator\\nazwiska.txt");
        }

        public static String DataPath { get; protected set; }
        public static String[] NamesMale { get; protected set; }
        public static String[] NamesFemale { get; protected set; }
        /// <summary>Nazwiska polskie</summary>
        public static String[] Names { get; protected set; }
        /// <summary>W pierwszej komórce: Poznań, w pozostałych okoliczne miejscowości</summary>
        public static String[] Cities { get; protected set; }
        /// <summary>Nazwy ulic, pierwszy indeks: id miasta</summary>
        public static String[][] Streets { get; protected set; }


        /// <summary>
        /// Wygeneruj i dodaj do bazy określoną liczbę czytelników.
        /// </summary>
        public static void AddUsers(int count)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            Random rand = new Random();
            // stałe dla wygenerowania daty urodzenia
            DateTime firstBornDate = new DateTime(1940, 1, 1);
            DateTime lastBornDate = new DateTime(2004, 1, 1);
            int birthDaysRange = (lastBornDate - firstBornDate).Days;
            String[] emailDomens = { "gmail.com", "wp.pl", "hotmail.com", "onet.pl", "o2.pl" };
            DateTime firstRegisteredDate = new DateTime(2010, 3, 1);
            DateTime lastRegisteredDate = DateTime.Now;
            int registerDaysRange = (lastRegisteredDate - firstRegisteredDate).Days;

            for (int i = 0; i < count; i++)
            {
                Czytelnik user = new Czytelnik();
                bool male = (rand.Next() & 1) == 0;
                if (male)
                {
                    user.Plec = 'm';
                    user.Imie = NamesMale[rand.Next(NamesMale.Length)];
                    user.Nazwisko = Names[rand.Next(Names.Length)];
                }
                else
                {
                    user.Plec = 'k';
                    user.Imie = NamesFemale[rand.Next(NamesFemale.Length)];
                    String name = Names[rand.Next(Names.Length)];
                    if (name[name.Length - 1] == 'i')
                        name = name.Substring(0, name.Length - 1) + 'a';
                    user.Nazwisko = name;
                }

                // 70% czytelników będzie z Poznania
                int cityId = (rand.Next(10) < 7) ? 0 : rand.Next(1, Cities.Length);

                user.Miasto = Cities[cityId];
                user.Ulica = Streets[cityId][rand.Next(Streets[cityId].Length)];
                user.NrPosesji = rand.Next(1, 50).ToString();

                user.DataUrodzenia = firstBornDate.AddDays(rand.Next(birthDaysRange));
                user.DataRejestracji =
                    firstRegisteredDate.AddDays(rand.Next(registerDaysRange))
                    .AddHours(8+rand.Next(8))
                    .AddMinutes(rand.Next(60));

                user.Email = String.Format("{0}.{1}@{2}", user.Imie[0], user.Nazwisko,
                    emailDomens[rand.Next(emailDomens.Length)]);

                user.NrTelefonu = rand.Next(50000000, 99999999).ToString() + rand.Next(10).ToString();

                db.Czytelniks.InsertOnSubmit(user);
            }
            db.SubmitChanges();
        }


        /// <summary>
        /// Wygeneruj i dodaj do bazy określoną liczbę bilbiotekarzy.
        /// </summary>
        public static void AddLibrarians(int count)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            Random rand = new Random();

            for (int i = 0; i < count; i++)
            {
                Bibliotekarz librarian = new Bibliotekarz();
                bool male = (rand.Next() & 1) == 0;
                if (male)
                {
                    librarian.Plec = 'm';
                    librarian.Imie = NamesMale[rand.Next(NamesMale.Length)];
                    librarian.Nazwisko = Names[rand.Next(Names.Length)];
                }
                else
                {
                    librarian.Plec = 'k';
                    librarian.Imie = NamesFemale[rand.Next(NamesFemale.Length)];
                    String name = Names[rand.Next(Names.Length)];
                    if (name[name.Length - 1] == 'i')
                        name = name.Substring(0, name.Length - 1) + 'a';
                    librarian.Nazwisko = name;
                }

                // 60% bibliotekarzy będzie z Poznania
                int cityId = (rand.Next(10) < 6) ? 0 : rand.Next(1, Cities.Length);

                librarian.Miasto = Cities[cityId];
                librarian.Ulica = Streets[cityId][rand.Next(Streets[cityId].Length)];
                librarian.NrPosesji = rand.Next(1, 50).ToString();

                librarian.Email = String.Format("{0}.{1}@naszabiblioteka.pl",
                    librarian.Imie, librarian.Nazwisko);

                librarian.NrTelefonu = rand.Next(50000000, 99999999).ToString() + rand.Next(10).ToString();

                db.Bibliotekarzs.InsertOnSubmit(librarian);
            }
            db.SubmitChanges();
        }
    }
}
