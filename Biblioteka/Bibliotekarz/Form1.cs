﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Bibliotekarz
{
    public partial class App : Form
    {
        public App()
        {
            InitializeComponent();
            tabs = this.tabCtrl;

            ConnectionString = 
                @"Data Source=(local);Initial Catalog=BIBLIOTEKA;
                 Integrated Security=SSPI;";
            Connection = new SqlConnection(ConnectionString);

            AddPage("tabSummary", new SummaryForm());
            AddPage("tabLibrarians", new LibrariansForm());
            AddPage("tabNewLibrarian", new NewLibrarianForm());
            AddPage("tabReaders", new ReadersForm());
            AddPage("tabRegisterReader", new RegisterReaderForm());
            AddPage("tabBorrowBook", new BorrowBookForm());
            AddPage("tabProlongBorrow", new ProlongBorrowForm());
            AddPage("tabReturnBook", new ReturnBookForm());
            AddPage("tabTitles", new SelectTitleForm());
            AddPage("tabNewEdition", new NewEditionForm());
            AddPage("tabEditions", new SelectEditionForm());
            AddPage("tabPublishers", new SelectPublisherForm());
            AddPage("tabAuthors", new SelectAuthorForm());
            AddPage("tabAddBooks", new AddBooksForm());
            AddPage("tabRepairBook", new RepairBooksForm());
            AddPage("tabBooksByPublisher", new BooksByPublisherForm());
            AddPage("tabTitleEditionsLastBorrow", new TitleEditionsLastBorrow());
            AddPage("tabReadersSummary", new ReadersSummaryForm());
            AddPage("tabBorrowsSummary", new BorrowsSummaryForm());
            AddPage("tabExportBorrows", new ExportBorrowsForm());
        }

        public static String DataBasePath { get { return "..\\db\\"; } }
        public static SqlConnection Connection { get; private set; }
        public static String ConnectionString { get; private set; }

        private static TabControl tabs;

        /// <summary>Dodaj formatkę jako zawartość wskazanej strony. </summary>
        private void AddPage(String pageName, Control pageContent)
        {
            TabPage tabPage = this.tabCtrl.TabPages[pageName];
            tabPage.Controls.Add(pageContent);
            pageContent.Location = new Point((this.Width-pageContent.Width)/2, 0);
            pageContent.Anchor = AnchorStyles.Top;
            tabPage.BackColor = Color.White;
        }

        public static bool OpenPage(String pageName)
        {
            try
            {
                tabs.SelectTab(pageName);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
