﻿namespace Bibliotekarz
{
    partial class RepairBooksForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbBookId = new System.Windows.Forms.TextBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labBookInfo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCondition = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // tbBookId
            // 
            this.tbBookId.Location = new System.Drawing.Point(216, 17);
            this.tbBookId.Name = "tbBookId";
            this.tbBookId.Size = new System.Drawing.Size(100, 20);
            this.tbBookId.TabIndex = 0;
            this.tbBookId.Leave += new System.EventHandler(this.tbBookId_Leave);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(102, 144);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(133, 23);
            this.btnConfirm.TabIndex = 2;
            this.btnConfirm.Text = "Zatwierdź naprawę";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Wpisz identyfikator reperowanej książki:";
            // 
            // labBookInfo
            // 
            this.labBookInfo.AutoSize = true;
            this.labBookInfo.Location = new System.Drawing.Point(15, 46);
            this.labBookInfo.Name = "labBookInfo";
            this.labBookInfo.Size = new System.Drawing.Size(115, 13);
            this.labBookInfo.TabIndex = 2;
            this.labBookInfo.Text = "<informacja o książce>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Określ stan książki po naprawie:";
            // 
            // cbCondition
            // 
            this.cbCondition.FormattingEnabled = true;
            this.cbCondition.Items.AddRange(new object[] {
            "0 (zniszczona)",
            "1 (mierny)",
            "2",
            "3",
            "4",
            "5 (idealny)"});
            this.cbCondition.Location = new System.Drawing.Point(180, 84);
            this.cbCondition.Name = "cbCondition";
            this.cbCondition.Size = new System.Drawing.Size(136, 21);
            this.cbCondition.TabIndex = 1;
            // 
            // RepairBooksForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbCondition);
            this.Controls.Add(this.labBookInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.tbBookId);
            this.Name = "RepairBooksForm";
            this.Size = new System.Drawing.Size(350, 206);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbBookId;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labBookInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCondition;
    }
}
