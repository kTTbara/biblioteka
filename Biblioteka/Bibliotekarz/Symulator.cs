﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bibliotekarz
{
    public class Symulator
    {
        public Symulator(DateTime startDate, DateTime endDate)
        {
            StartDate = startDate.Date;
            EndDate = endDate;
        }

        BiblEntities entities;
        IQueryable<efCzytelnik> Readers;
        efCzytelnik[] ReadersArray;
        int ReadersCount;
        efBibliotekarz[] LibrariansArray;
        public DateTime StartDate;
        public DateTime EndDate;
        private DateTime CurrentTime;
        Random Rand;

        public void Simulate()
        {
            try
            {
                using (entities = new BiblEntities())
                {
                    Rand = new Random();

                    LibrariansArray = entities.Bibliotekarz.ToArray();

                    const int MinutesOnMonth = 12480;     // 60 min * 8 h * 26 dni

                    CurrentTime = StartDate.AddHours(9);
                    while (CurrentTime < EndDate)
                    {
                        if (CurrentTime.DayOfWeek == DayOfWeek.Sunday)
                        {
                            CurrentTime = CurrentTime.AddDays(1);
                            continue;
                        }

                        if (CurrentTime.DayOfWeek == DayOfWeek.Wednesday || CurrentTime.DayOfWeek == DayOfWeek.Saturday)
                            RepairBooks();

                        Readers = entities.Czytelnik.Where(cz => cz.DataRejestracji < CurrentTime);
                        ReadersArray = Readers.ToArray();
                        ReadersCount = Readers.Count();

                        /* szansa na akcję w miesiącu */
                        int BorrowChance = ReadersCount / 2;        // średnio 0,5 wypozyczenia czytelnika w miesiącu po 2 książki
                        int ReservationChance = (ReadersCount / 2) + BorrowChance;  // j.w.
                        int ProlongChance = (ReadersCount / 4) + ReservationChance; // raz na 2 mies. przedłuża wyp. jednej książki
                        int ReturnChance = (ReadersCount * 4) + ProlongChance;      // co tydzień próba oddania (i/lub zapłaty kary)
                        int counter, choice;

                        while (true)
                        {
                            counter = 0;
                            while ((choice = Rand.Next(MinutesOnMonth)) >= ReturnChance)
                                ++counter;
                            CurrentTime = CurrentTime.AddMinutes(counter);
                            if (CurrentTime.Hour >= 17)
                                break;

                            if (choice < BorrowChance)
                                BorrowBooks();
                            else if (choice < ReservationChance)
                                ReserveBooks();
                            else if (choice < ProlongChance)
                                ProlongBorrow();
                            else// if (choice < ReturnChance)
                                ReturnBooks();
                        }

                        // po tygodniu rezerwacje stają się nieaktywne
                        DeactivateReservations();

                        CurrentTime = CurrentTime.AddHours(16);   // godzina 17.00 + 16 = godzina 9.00 dnia następnego
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMsgBox(ex);
                return;
            }
        }

        void ReserveBooks()
        {
            efCzytelnik reader = ReadersArray[Rand.Next(ReadersCount)];
            int booksToReserve = Rand.Next(1, 3);

            // znajdź dostepne ksiązki do rezerwacji (tj. wydania których istnieją niewypozyczone egzemplarze)
            int[] editionsIds = entities.Egzemplarz
                        .Where(e => e.Wypozyczenie.Any() == false ||
                            e.Wypozyczenie.OrderByDescending(es => es.IdEgzemplarza).FirstOrDefault().DataZwrotu != null)
                        .Select(e => e.IdWydania)
                        .ToArray();
            if (editionsIds == null || editionsIds.Length == 0)
                return;
            List<int> reservedBooksIds = new List<int>();
            int editionId;

            for (int i = 0; i < booksToReserve; i++)
            {
                while (true)
                {
                    editionId = editionsIds[Rand.Next(editionsIds.Length)];
                    if (reservedBooksIds.IndexOf(editionId) < 0)
                        break;
                }

                efRezerwacja reservation = new efRezerwacja();
                reservation.IdCzytelnika = reader.IdCzytelnika;
                reservation.IdWydania = editionId;
                reservation.DataRezerwacji = CurrentTime;
                entities.Rezerwacja.Add(reservation);
                reservedBooksIds.Add(editionId);
            }

            entities.SaveChanges();
        }

        void BorrowBooks()
        {
            efCzytelnik reader = ReadersArray[Rand.Next(ReadersCount)];
            efBibliotekarz librarian = LibrariansArray[Rand.Next(LibrariansArray.Length)];
            efRezerwacja[] reservations = null;
            efEgzemplarz[] books;
            efEgzemplarz book;

            int days = (new int[] { 14, 30, 60 })[Rand.Next(3)];
            bool fromReservation = true;

            int booksToBorrow = Rand.Next(1, 3);
            for (int i = 0; i < booksToBorrow; i++)
            {
                if (fromReservation)
                {
                    reservations = entities.Rezerwacja.Where(r => r.IdCzytelnika == reader.IdCzytelnika).ToArray();
                }

                // wypozycz z rezerwacji: szansa 2/3
                if (reservations.Length > 0 && Rand.Next(3) < 2)
                {
                    fromReservation = true;
                    efRezerwacja reservation = reservations[Rand.Next(reservations.Length)];
                    int rsrvdEditionId = reservation.IdWydania;
                    // wybiera ksiazke ktora nie jest wypozyczona (choc nie powinna byc skoro byla rezerwowana)
                    // sprawdza też czy nie jest zniszczona
                    books = entities.Egzemplarz
                        .Where(e => e.IdWydania == rsrvdEditionId && e.Stan > 0)
                        .Where(e => e.Wypozyczenie.Any() == false ||
                            e.Wypozyczenie.OrderByDescending(es => es.IdEgzemplarza).FirstOrDefault().DataZwrotu != null)
                        .ToArray();
                    if (books == null || books.Length == 0)
                    {
                        // brak zarezerwowanej ksiązki - to się może zdarzyć gdy np. zniszczona -> anuluj rezerwację
                        entities.Rezerwacja.Remove(reservation);
                        continue;
                    }
                    book = books[Rand.Next(books.Length)];
                }

                else
                {
                    fromReservation = false;
                    // znajdź niewypożyczoną i niezniszczoną ksiażkę
                    books = entities.Egzemplarz
                        .Where(e => e.Stan > 0 && (e.Wypozyczenie.Any() == false ||
                            e.Wypozyczenie.OrderByDescending(es => es.IdEgzemplarza).FirstOrDefault().DataZwrotu != null))
                        .ToArray();
                    if (books == null || books.Length == 0)
                        continue;
                    book = books[Rand.Next(books.Length)];
                }

                efWypozyczenie borrow = new efWypozyczenie();
                borrow.IdCzytelnika = reader.IdCzytelnika;
                borrow.IdBibliotekarza = librarian.IdBibliotekarza;
                borrow.IdEgzemplarza = book.IdEgzemplarza;
                borrow.DataWypozyczenia = CurrentTime;
                borrow.LiczbaDni = days;
                entities.Wypozyczenie.Add(borrow);

                entities.SaveChanges();
            }
        }

        void ProlongBorrow()
        {
            efCzytelnik reader = ReadersArray[Rand.Next(ReadersCount)];
            efWypozyczenie[] borrows = reader.Wypozyczenie.Where(w => w.DataZwrotu != null).ToArray();
            int librarianId = LibrariansArray[Rand.Next(LibrariansArray.Length)].IdBibliotekarza;

            if (borrows == null || borrows.Length == 0)
                return;

            // przedłużamy jedno wypożyczenie na raz
            efWypozyczenie borrow = borrows[Rand.Next(borrows.Length)];

            // nieprzedłuży jeśli minęła data zwrotu
            if (borrow.DataWypozyczenia.AddDays(borrow.LiczbaDni) < CurrentTime)
                return;

            // jeśli są zaległe kary -> odmowa przedłużenia
            if (entities.Kara.Where(k => k.IdCzytelnika == reader.IdCzytelnika && k.DataPlatnosci == null).Count() > 0)
                return;

            efPrzedluzenie prolong = new efPrzedluzenie();
            prolong.IdWypozyczenia = borrow.IdWypozyczenia;
            prolong.IdBibliotekarza = librarianId;
            prolong.Data = CurrentTime;
            prolong.LiczbaDni = (new int[] { 7, 14, 30 })[Rand.Next(3)];
            entities.Przedluzenie.Add(prolong);

            // do dni wypozyczenia tez dodaj liczbę dni
            borrow.LiczbaDni = borrow.LiczbaDni + prolong.LiczbaDni;

            entities.SaveChanges();
        }

        void ReturnBooks()
        {
            efCzytelnik reader = ReadersArray[Rand.Next(ReadersCount)];
            IEnumerable<efWypozyczenie> borrows = reader.Wypozyczenie.Where(w => w.DataZwrotu == null);
            int librarianId = LibrariansArray[Rand.Next(LibrariansArray.Length)].IdBibliotekarza;
            if (borrows == null)
                return;

            // zaplata za zaległe kary
            IEnumerable<efKara> penulties = reader.Kara.Where(k => k.DataPlatnosci == null);
            foreach (efKara penulty in penulties)
            {
                penulty.DataPlatnosci = CurrentTime;
            }

            // być może nic nie ma do zwrotu - wtedy tylko przyjdzie kary zapłacić
            foreach (efWypozyczenie borrow in borrows)
            {
                // czasem nie zwracamy wszystkich bo jeszcze czytamy
                if (Rand.Next(5) == 0) continue;
                borrow.DataZwrotu = CurrentTime;
                borrow.IdPrzyjmujacegoZwrot = librarianId;

                // kara za oddanie po terminie
                TimeSpan holdTime = CurrentTime.Subtract(borrow.DataWypozyczenia);
                if (holdTime.Days > borrow.LiczbaDni)
                {
                    efKara penalty = new efKara();
                    penalty.IdCzytelnika = reader.IdCzytelnika;
                    penalty.IdBibliotekarza = librarianId;
                    penalty.TypPrzewinienia = 1;        // typ kary: oddanie po terminie
                    penalty.DataNalozenia = CurrentTime;
                    penalty.Kwota = holdTime.Days - borrow.LiczbaDni;  // złotówka za dzień zwłoki
                    
                    // moze (ale nie musi) zapłacić od razu
                    if (Rand.Next(3) < 2)
                        penalty.DataPlatnosci = CurrentTime;

                    entities.Kara.Add(penalty);
                }

                // książka mogła się poniszczyć
                if (Rand.Next(6) == 0)
                {
                    efEgzemplarz book = borrow.Egzemplarz;
                    int newCondition;
                    if (Rand.Next(6) == 0)
                        newCondition = 0;   // zniszczenie ksiązki
                    else
                        newCondition = (int)book.Stan - 1;
                    if (newCondition < 0)
                        newCondition = 0;
                    book.Stan = (byte)newCondition;
                    
                    // kara za zniszczenie książki
                    if (newCondition == 0)
                    {
                        efKara penalty = new efKara();
                        penalty.IdCzytelnika = reader.IdCzytelnika;
                        penalty.IdBibliotekarza = librarianId;
                        penalty.TypPrzewinienia = 2;        // typ kary: zniszczenie książki
                        penalty.DataNalozenia = CurrentTime;
                        penalty.Kwota = 10 * Rand.Next(1,6);    // kwota za ksiązkę

                        // moze (ale nie musi) zapłacić od razu
                        if (Rand.Next(3) < 2)
                            penalty.DataPlatnosci = CurrentTime;

                        entities.Kara.Add(penalty);
                    }
                }
            }
            entities.SaveChanges();
        }

        void RepairBooks()
        {
            // naprawa książek dostępnych w biblotece jesli ich stan <= 1
            var booksToRepair = entities.Egzemplarz.Where(e => e.Stan <= 1 &&
                (e.Wypozyczenie.Any() == false || e.Wypozyczenie.OrderByDescending(es => es.IdEgzemplarza).FirstOrDefault().DataZwrotu != null));
            foreach (efEgzemplarz book in booksToRepair)
            {
                efReperacja reperation = new efReperacja();
                reperation.IdEgzemplarza = book.IdEgzemplarza;
                reperation.Data = CurrentTime;
                entities.Reperacja.Add(reperation);
                book.Stan = 4;
            }
            entities.SaveChanges();
        }

        void DeactivateReservations()
        {
            DateTime maxResDate = CurrentTime.AddDays(-7);
            var reservationsToDeact = entities.Rezerwacja.Where(r => r.DataRezerwacji < maxResDate);
            foreach (efRezerwacja res in reservationsToDeact)
                entities.Rezerwacja.Remove(res);
            entities.SaveChanges();
        }
    }
}
