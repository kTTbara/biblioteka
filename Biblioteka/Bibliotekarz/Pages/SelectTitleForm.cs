﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Bibliotekarz
{
    public partial class SelectTitleForm : UserControl
    {
        public SelectTitleForm()
        {
            InitializeComponent();
            SelectedTitleId = -1;
            LoadData();
        }

        public DataGridViewRow SelectedRow { get; protected set; }
        public int SelectedTitleId { get; protected set; }


        /// <summary>
        /// Pobierz z bazy tabelę Tytul przy pomocy ADO.NET Entities.
        /// Parametry metody są opcjonalne - jeśli podane, to zapytanie
        /// bedzie zawierać podane parametry.
        /// </summary>
        private void LoadData(int titleId = 0, String titleTxt = "")
        {
            using (BiblEntities entities = new BiblEntities())
            {
                var titles = entities.Tytul.AsQueryable();
                if (titleId != 0)
                    titles = titles.Where(t => t.IdTytulu == titleId);
                if (titleTxt.Length > 0)
                    // contains - jak w sql: like '%titleText%'
                    titles = titles.Where(t => t.NazwaTytulu.Contains(titleTxt));

                dataGridTitles.DataSource = titles
                    .Select(t => new { Id = t.IdTytulu, Tytuł = t.NazwaTytulu })
                    .ToList();
            }

            dataGridTitles.Columns[0].Width = 30;
            dataGridTitles.Columns[1].Width = 250;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (tbId.Text.Length > 0)
                if (Int32.TryParse(tbId.Text, out id) == false || id < 0)
                {
                    Utils.ShowExclamationMsgBox("Podano niepoprawny id tytułu.");
                    return;
                }

            LoadData(id, tbTitle.Text);
        }

        /// <summary>
        /// Dodaj tytuł do bazy (ADO.NET). Sprawdza czy duplikat.
        /// </summary>
        private void btnAddTitle_Click(object sender, EventArgs e)
        {
            if (tbTitle.Text.Length == 0)
            {
                Utils.ShowExclamationMsgBox("Nie podano tytułu.");
                return;
            }

            // sprawdź, czy w bazie jest już taki tytuł
            using (BiblEntities entities = new BiblEntities())
            {
                var title = entities.Tytul.Where(t => t.NazwaTytulu.Equals(tbTitle.Text)).FirstOrDefault();
                if (title != null)
                {
                    DialogResult userChoice = MessageBox.Show(String.Format(
                    "W bazie istnieje już tytuł {0}. Czy jesteś pewien, że chcesz wstawić ponownie taki tytuł?", tbTitle.Text),
                    "Tytuł jest już w bazie",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Exclamation);
                    if (userChoice != DialogResult.Yes)
                        return;
                }
            }

            // wstawienie tytułu do bazy
            using (BiblEntities entities = new BiblEntities())
            {
                efTytul newTitle = new efTytul();
                newTitle.NazwaTytulu = tbTitle.Text;
                entities.Tytul.Add(newTitle);
                int result = entities.SaveChanges();

                if (result > 0)
                    MessageBox.Show("Dodano do bazy tytuł: " + newTitle.NazwaTytulu);
                else
                    Utils.ShowErrorMsgBox("Nie udało się wstawić tytułu do bazy.");
            }
        }

        private void dataGridTitles_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridTitles.SelectedRows.Count > 0)
            {
                SelectedRow = dataGridTitles.SelectedRows[0];
                SelectedTitleId = (int)SelectedRow.Cells[0].Value;
            }
            else
            {
                SelectedRow = null;
                SelectedTitleId = -1;
            }
        }
    }
}
