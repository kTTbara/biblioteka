﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Objects;

namespace Bibliotekarz
{
    public partial class AddBooksForm : UserControl
    {
        public AddBooksForm()
        {
            InitializeComponent();
        }

        public int EditionId { get; protected set; }


        private void btnSelectEdition_Click(object sender, EventArgs e)
        {
            WindowWithContent dlg = new WindowWithContent(new SelectEditionForm());
            dlg.Text = "Wybierz wydanie";
            if (dlg.ShowDialog() != DialogResult.OK)
                return;
            SelectEditionForm form = (SelectEditionForm)dlg.Content;
            if (form.SelectedEditionId < 1)
                Utils.ShowExclamationMsgBox("Nie wybrano wydania");
            else
            {
                EditionId = form.SelectedEditionId;
                labEdition.Text = String.Format("{0} ({1}, {2})",
                    (String)form.SelectedRow.Cells["Tytuł"].Value,
                    (String)form.SelectedRow.Cells["ISBN"].Value,
                    (Int16)form.SelectedRow.Cells["Rok"].Value);
            }
        }

        /// <summary>
        /// Dodanie określonej liczby egzemplarzy wybranego wydania
        /// za pomocą procedury składowanej wywołanej poprzez ADO.NET.
        /// </summary>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            short booksNumber = 0;

            if (EditionId < 1)
                Utils.ShowExclamationMsgBox("Nie wybrano wydania!");

            else if (this.tbBooksNumber.Text.Length == 0 ||
                    !Int16.TryParse(tbBooksNumber.Text, out booksNumber) ||
                    booksNumber < 1)
                Utils.ShowExclamationMsgBox("Nie wpisano liczby egzemplarzy lub podana wartość nie jest liczbą dodatnią.");

            else
            {
                /* Procedura składowana do dodawania egzemplarzy danego wydania */
                // ADO.NET: procedura składowana
                try
                {
                    using (BiblEntities entities = new BiblEntities())
                    {
                        ObjectParameter result =
                            new ObjectParameter("DodanychEgzemplarzy", typeof(short));
                        entities.DodajEgzemplarze(EditionId, booksNumber, result);

                        short addedBooks = (short)result.Value;
                        if (addedBooks < 1)
                            Utils.ShowExclamationMsgBox(
                                "Nie udało się dodać egzemplarzy tego wydania.");
                        else
                            MessageBox.Show(String.Format(
                                "Dodano {0} z {1} egzemplarzy tego wydania do bazy.",
                                addedBooks, booksNumber));
                    }
                }
                catch (Exception ex)
                {
                    Utils.ShowExceptionMsgBox(ex);
                    return;
                }

                ClearData();
            }
        }

        public void ClearData()
        {
            EditionId = 0;
            tbBooksNumber.Text = String.Empty;
            labEdition.Text = String.Empty;
        }
    }
}
