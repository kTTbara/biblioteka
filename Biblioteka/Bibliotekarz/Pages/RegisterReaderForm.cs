﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class RegisterReaderForm : UserControl
    {
        public RegisterReaderForm()
        {
            InitializeComponent();
            this.cbGender.SelectedIndex = 0;
        }

        /// <summary>
        /// Dodaj czytelnika do bazy używając ADO.NET.
        /// </summary>
        private void btnAddReader_Click(object sender, EventArgs e)
        {
            if (tbFirstName.Text.Length == 0 || tbLastName.Text.Length == 0)
                MessageBox.Show("Musisz podać imię i nazwisko.");
            else if (tbCity.Text.Length != 0 && (tbStreet.Text.Length == 0 || tbNumber.Text.Length == 0))
                MessageBox.Show("Skoro podałeś miasto, to podaj pełny adres.");

            else
            {
                using (BiblEntities entities = new BiblEntities())
                {
                    efCzytelnik czyt = new efCzytelnik();
                    czyt.Imie = tbFirstName.Text;
                    czyt.Nazwisko = tbLastName.Text;

                    String gender = cbGender.Text;
                    czyt.Plec = (gender.Equals("Mężczyzna")) ? "m" : "k";

                    if (tbCity.Text.Length > 0) czyt.Miasto      = tbCity.Text;
                    if (tbStreet.Text.Length > 0) czyt.Ulica     = tbStreet.Text;
                    if (tbNumber.Text.Length > 0) czyt.NrPosesji = tbNumber.Text;
                    if (tbEmail.Text.Length > 0) czyt.Email      = tbEmail.Text;
                    if (tbPhone.Text.Length > 0) czyt.NrTelefonu = tbPhone.Text;

                    czyt.DataUrodzenia = datePickerBirthday.Value;
                    czyt.DataRejestracji = DateTime.Now;

                    entities.Czytelnik.Add(czyt);
                    entities.SaveChanges();
                }

                Utils.ShowInfoMsgBox(
                    String.Format("Zarejestrowano czytelnika: {0} {1}",
                    tbFirstName.Text, tbLastName.Text));

                ClearForms();
            }
        }

        private void ClearForms()
        {
            foreach (Control ctrl in this.Controls)
                if (ctrl.GetType() == typeof(TextBox))
                    ((TextBox)ctrl).Text = String.Empty;
            this.cbGender.SelectedIndex = 0;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearForms();
        }
    }
}
