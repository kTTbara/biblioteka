﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class LibrariansForm : UserControl
    {
        public LibrariansForm()
        {
            InitializeComponent();
            LoadData();
        }


        /// <summary>
        /// Wyświetl spis bibliotekarzy z użyciem ADO.NET i LINQ to Entities.
        /// </summary>
        private void LoadData()
        {
            using (BiblEntities entities = new BiblEntities())
            {
                var librarians = from b in entities.Bibliotekarz
                                 select new
                                 {
                                     ID = b.IdBibliotekarza,
                                     Imię = b.Imie,
                                     Nazwisko = b.Nazwisko,
                                     Miasto = b.Miasto,
                                     Ulica = b.Ulica,
                                     NrPosesji = b.NrPosesji
                                 };
                this.dataGrid.DataSource = librarians.ToList();

                /* statystyki bibliotekarzy */

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Liczba zatrudnionych bibliotekarzy: " + entities.Bibliotekarz.Count());
                sb.AppendLine("Liczba wszystkich wypożyczeń: " + entities.Wypozyczenie.Count());
                sb.AppendLine("Liczba wszystkich zwrotów: " + entities.Wypozyczenie.Where(w => w.DataZwrotu != null).Count());
                sb.AppendLine("Liczba wszystkich przedłużeń wypożyczeń: " + entities.Przedluzenie.Count());
                sb.AppendLine("Liczba wszystkich nałożonych kar: " + entities.Kara.Count());
                sb.AppendLine();

                var tbof =
                    new
                    {
                        NajWypozyczen = entities.Bibliotekarz
                            .Select(b => new { b.Imie, b.Nazwisko, Wypozyczen = b.Wypozyczenie.Count })
                            .OrderByDescending(bb => bb.Wypozyczen)
                            .First(),
                        NajPrzedluzen = entities.Bibliotekarz
                            .Select(b => new { b.Imie, b.Nazwisko, Wypozyczen = b.Przedluzenie.Count })
                            .OrderByDescending(b => b.Wypozyczen)
                            .First(),
                        NajKar = entities.Bibliotekarz
                            .Select(b => new { b.Imie, b.Nazwisko, Wypozyczen = b.Kara.Count })
                            .OrderByDescending(b => b.Wypozyczen)
                            .First()
                    };

                sb.AppendLine("Najwięcej obsłużonych wypożyczeń:");
                sb.AppendLine(String.Format("{0} {1} ({2})",
                    tbof.NajWypozyczen.Imie, tbof.NajWypozyczen.Nazwisko, tbof.NajWypozyczen.Wypozyczen));
                sb.AppendLine();
                sb.AppendLine("Najwięcej przedłużeń wypożyczeń:");
                sb.AppendLine(String.Format("{0} {1} ({2})",
                    tbof.NajPrzedluzen.Imie, tbof.NajPrzedluzen.Nazwisko, tbof.NajPrzedluzen.Wypozyczen));
                sb.AppendLine();
                sb.AppendLine("Najwięcej nałożonych kar:");
                sb.AppendLine(String.Format("{0} {1} ({2})",
                    tbof.NajKar.Imie, tbof.NajKar.Nazwisko, tbof.NajKar.Wypozyczen));

                this.labAllStats.Text = sb.ToString();
            }
        }

        /// <summary>
        /// Pokaż dane zaznaczonego bibliotekarza.
        /// </summary>
        private void dataGrid_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dataGrid.SelectedRows.Count == 0) return;

            using (BiblEntities entities = new BiblEntities())
            {
                int libId = (int) this.dataGrid.SelectedRows[0].Cells[0].Value;
                var librarian = entities.Bibliotekarz.Where(b => b.IdBibliotekarza == libId).First();

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(librarian.Imie + " " + librarian.Nazwisko);
                sb.AppendLine();
                sb.AppendLine("Płeć: " + (librarian.Plec.Equals("m") ? "mężczyzna" : "kobieta"));
                sb.Append("Adres: " + librarian.Miasto + ", ");
                sb.AppendLine(librarian.Ulica + " " + librarian.NrPosesji);
                sb.AppendLine();
                sb.AppendLine("Telefon: " + librarian.NrTelefonu);
                sb.AppendLine("Email: " + librarian.Email);

                sb.AppendLine();
                sb.AppendLine("----------------------------");
                sb.AppendLine();

                // obslugujący wypożyczenie: wg pola Wypozyczenie.IdBibliotekarza
                sb.AppendLine("Liczba wypożyczeń: " + librarian.Wypozyczenie.Count());
                // przyjmujący odbior: wg pola Wypozyczenie.IdPrzyjmujacegoZwrot - nie można z librarian wziąć
                sb.AppendLine("Liczba odbiorów: " + entities.Wypozyczenie
                    .Where(w => w.IdPrzyjmujacegoZwrot == libId)
                    .Count());
                sb.AppendLine("Liczba przedłużeń: " + librarian.Przedluzenie.Count());
                sb.AppendLine("Liczba nałożonych kar: " + librarian.Kara.Count());

                this.labLibrarianInfo.Text = sb.ToString();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
