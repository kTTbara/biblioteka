﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Bibliotekarz
{
    public partial class SelectEditionForm : UserControl
    {
        public SelectEditionForm()
        {
            InitializeComponent();
            SelectedEditionId = -1;
            RefreshGrid();
        }

        public DataGridViewRow SelectedRow { get; protected set; }
        public int SelectedEditionId { get; protected set; }

        /// <summary>
        /// Znajdź wydanie przy użyciu LINQ to SQL.
        /// Używa sparametryzowanego zapytania LINQ oraz operacji JOIN.
        /// </summary>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            // sprarametryzowane zapytanie LINQ
            // String.Contains() zadziała jak LIKE '%costam%' w SQL

            int editionId = 0;
            int titleId = 0;
            int publisherId = 0;
            short year = 0;
            int minBooksCount = -1;

            if (tbEditionId.Text.Length > 0)
                Int32.TryParse(tbEditionId.Text, out editionId);
            if (tbTitleId.Text.Length > 0)
                Int32.TryParse(tbTitleId.Text, out titleId);
            if (tbPublisherId.Text.Length > 0)
                Int32.TryParse(tbPublisherId.Text, out publisherId);
            if (tbYear.Text.Length > 0)
                Int16.TryParse(tbYear.Text, out year);
            if (tbMinBooksCount.Text.Length > 0)
                Int32.TryParse(tbMinBooksCount.Text, out minBooksCount);

            DataClassesDataContext db = new DataClassesDataContext();
            var query = from w in db.Wydanies
                        join t in db.Tytuls on w.IdTytulu equals t.IdTytulu
                        join wyd in db.Wydawcas on w.IdWydawcy equals wyd.IdWydawcy
                        select new
                        {
                            Id = w.IdWydania,
                            w.ISBN,
                            w.IdTytulu,
                            Tytuł = t.NazwaTytulu,
                            Autorów = w.Autorstwos.Count(),
                            w.IdWydawcy,
                            NazwaWydawcy = wyd.Nazwa,
                            w.Rok,
                            LiczbaEgzemplarzy = w.Egzemplarzs.Count(),
                            Autorzy = db.Autorstwos
                                        .Where(a => a.IdWydania == w.IdWydania)
                                        .Select(a => a.Autor.Imie + " " + a.Autor.Nazwisko)
                                        .ToArray()
                        };
            
            if (editionId != 0)
                query = query.Where(w => w.Id == editionId);
            if (titleId != 0)
                query = query.Where(w => w.IdTytulu == titleId);
            if (publisherId != 0)
                query = query.Where(w => w.IdWydawcy == publisherId);
            if (year != 0)
                query = query.Where(w => w.Rok == year);
            if (tbISBN.Text.Length > 0)
                query = query.Where(w => w.ISBN.Contains(tbISBN.Text));
            if (tbTitle.Text.Length > 0)
                query = query.Where(w => w.Tytuł.Contains(tbTitle.Text));
            if (tbPublisherName.Text.Length > 0)
                query = query.Where(w => w.NazwaWydawcy.Contains(tbPublisherName.Text));
            if (minBooksCount > -1)
                query = query.Where(w => w.LiczbaEgzemplarzy >= minBooksCount);
            if (tbAuthor.Text.Length > 0)
                query = query.Where(w => w.Autorzy.Any(a => a.Contains(tbAuthor.Text)));
            dataGridEditions.DataSource = query;

            dataGridEditions.Columns[0].Width /= 2;
            dataGridEditions.Columns[2].Width /= 2;
            dataGridEditions.Columns[4].Width /= 2;
        }

        private void dataGridEditions_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridEditions.SelectedRows.Count > 0)
            {
                SelectedRow = dataGridEditions.SelectedRows[0];
                SelectedEditionId = (int)SelectedRow.Cells[0].Value;

                /* wczytanie autorów wybranego wydania */
                if (SelectedEditionId < 1) return;
                DataClassesDataContext db = new DataClassesDataContext();
                var authors = db.Autorstwos
                    .Where(a => a.IdWydania == SelectedEditionId)
                    .Select(a => new { FirstName = a.Autor.Imie, LastName = a.Autor.Nazwisko })
                    .ToArray();
                this.labAuthors.Text = String.Empty;
                for (int a = 0; a < authors.Length; a++)
                    labAuthors.Text += String.Format("{0} {1}\n", authors[a].FirstName, authors[a].LastName);
            }
            else
            {
                SelectedRow = null;
                SelectedEditionId = -1;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (Control ctrl in this.Controls)
                if (ctrl.GetType() == typeof(TextBox))
                    ((TextBox)ctrl).Text = String.Empty;
        }

        private void dataGridEditions_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }
}
