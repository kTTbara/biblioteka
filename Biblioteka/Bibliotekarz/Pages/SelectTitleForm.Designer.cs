﻿namespace Bibliotekarz
{
    partial class SelectTitleForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddTitle = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridTitles = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTitles)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddTitle
            // 
            this.btnAddTitle.Location = new System.Drawing.Point(353, 171);
            this.btnAddTitle.Name = "btnAddTitle";
            this.btnAddTitle.Size = new System.Drawing.Size(149, 27);
            this.btnAddTitle.TabIndex = 4;
            this.btnAddTitle.Text = "Dodaj nowy";
            this.btnAddTitle.UseVisualStyleBackColor = true;
            this.btnAddTitle.Click += new System.EventHandler(this.btnAddTitle_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(353, 138);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(149, 27);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Wyszukaj";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(335, 91);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(181, 20);
            this.tbTitle.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(329, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Tytuł zawiera:";
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(363, 32);
            this.tbId.Name = "tbId";
            this.tbId.Size = new System.Drawing.Size(153, 20);
            this.tbId.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(332, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Id =";
            // 
            // dataGridTitles
            // 
            this.dataGridTitles.AllowUserToAddRows = false;
            this.dataGridTitles.AllowUserToDeleteRows = false;
            this.dataGridTitles.AllowUserToResizeRows = false;
            this.dataGridTitles.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridTitles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridTitles.Location = new System.Drawing.Point(3, 22);
            this.dataGridTitles.MultiSelect = false;
            this.dataGridTitles.Name = "dataGridTitles";
            this.dataGridTitles.ReadOnly = true;
            this.dataGridTitles.RowHeadersVisible = false;
            this.dataGridTitles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridTitles.Size = new System.Drawing.Size(320, 305);
            this.dataGridTitles.TabIndex = 0;
            this.dataGridTitles.SelectionChanged += new System.EventHandler(this.dataGridTitles_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(272, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Wyszukaj tytuł wg kryteriów i wybierz z tabelki (zaznacz)";
            // 
            // SelectTitleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnAddTitle);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridTitles);
            this.Controls.Add(this.label1);
            this.Name = "SelectTitleForm";
            this.Size = new System.Drawing.Size(519, 340);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTitles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddTitle;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridTitles;
        private System.Windows.Forms.Label label1;
    }
}
