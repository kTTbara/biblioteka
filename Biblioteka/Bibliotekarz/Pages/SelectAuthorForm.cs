﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Bibliotekarz
{

    public partial class SelectAuthorForm : UserControl
    {
        public SelectAuthorForm()
        {
            InitializeComponent();
            SelectedAuthorId = -1;

            LoadData();
        }

        public DataGridViewRow SelectedRow { get; protected set; }
        public int SelectedAuthorId { get; protected set; }


        /// <summary>
        /// Znajdź autora używając sparametryzowanego zapytania LINQ to SQL.
        /// </summary>
        private void LoadData()
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query = from autor in db.Autors
                        select new
                        {
                            Id       = autor.IdAutora,
                            Imię     = autor.Imie,
                            Nazwisko = autor.Nazwisko,
                            Płeć     = autor.Plec,
                            Kraj     = autor.Kraj,
                            WydanychTytułów = (from a in db.Autorstwos
                                        where a.IdAutora == autor.IdAutora
                                        select a.Wydanie.IdTytulu)
                                        .Distinct().Count()
                        };
            int id;

            if (tbId.Text.Length > 0 && Int32.TryParse(tbId.Text, out id))
                query = query.Where(a => a.Id == id);

            // dla tekstów: wyszukujemy te krotki, które zawierają wpisany tekst
            // w odpowiednich polach (nie równość tylko zawieranie, jak '%like%')
            if (tbFirstName.Text.Length > 0)
                query = query.Where(a => a.Imię.Contains(tbFirstName.Text));

            if (tbLastName.Text.Length > 0)
                query = query.Where(a => a.Nazwisko.Contains(tbLastName.Text));

            if (tbNationality.Text.Length > 0)
                query = query.Where(a => a.Kraj.Contains(tbNationality.Text));

            if (cbGender.SelectedIndex >= 0 && cbGender.SelectedIndex < 2)
                query = query.Where(a => a.Płeć == (cbGender.SelectedIndex == 0? 'k' : 'm'));

            dataGridAuthors.DataSource = query;
            dataGridAuthors.Columns[0].Width = 30;
        }


        /// <summary>  Dodaj autora do bazy przy użyciu LINQ to SQL. </summary>
        private void btnAddAuthor_Click(object sender, EventArgs e)
        {
            if (tbLastName.Text.Length == 0)
                MessageBox.Show("Wpisz przynajmniej nazwisko autora.");

            else
            {
                DataClassesDataContext db = new DataClassesDataContext();
                Autor autor = new Autor();

                autor.Nazwisko = tbLastName.Text;
                if (tbFirstName.Text.Length > 0) autor.Imie = tbFirstName.Text;
                if (cbGender.SelectedIndex == 0) autor.Plec = 'k';
                else if (cbGender.SelectedIndex == 1) autor.Plec = 'm';
                if (tbNationality.Text.Length > 0) autor.Kraj = tbNationality.Text;

                db.Autors.InsertOnSubmit(autor);
                db.SubmitChanges();

                MessageBox.Show("Dodano autora do bazy.");

                foreach (Control ctrl in this.Controls)
                    if (ctrl.GetType() == typeof(TextBox))
                        ((TextBox)ctrl).Text = String.Empty;
            }
        }
        
        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dataGridAuthors_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridAuthors.SelectedRows.Count > 0)
            {
                SelectedRow = dataGridAuthors.SelectedRows[0];
                SelectedAuthorId = (int)SelectedRow.Cells[0].Value;
            }
            else
            {
                SelectedRow = null;
                SelectedAuthorId = -1;
            }
        }
    }

}
