﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class ReturnBookForm : UserControl
    {
        public ReturnBookForm()
        {
            InitializeComponent();
            BooksToReturn = new List<BookInfo>();
            CurrentBookInfo = new BookInfo();

            ClearForms();

            /* wpisanie listy bibliotekarzy */
            DataClassesDataContext db = new DataClassesDataContext();
            var librarians = db.Bibliotekarzs.ToList();
            foreach (Bibliotekarz lib in librarians)
                this.cbLibrarian.Items.Add(String.Format("[{0}] {1} {2}",
                    lib.IdBibliotekarza, lib.Imie, lib.Nazwisko));
        }

        int ReaderId;
        int LibrarianId;

        struct BookInfo
        {
            public int Id, Condition, DamagedPenaltyHeight, ReturnDatePenaltyHeight;
            public String InfoText;
        }

        List<BookInfo> BooksToReturn;
        BookInfo CurrentBookInfo;


        private void btnConfirm_Click(object sender, EventArgs e)
        {
            int librarianId;
            if (!ReadSelectedLibrarianId(out librarianId))
                return;
            LibrarianId = librarianId;

            if (BooksToReturn.Count == 0)
            {
                Utils.ShowExclamationMsgBox("Nie wpisano żadnej książki do oddania.");
                return;
            }

            if (ReturnBooks())
                Utils.ShowInfoMsgBox("Pomyślnie zwrócono wszystkie książki.");
            else
                Utils.ShowExclamationMsgBox("Nie udało się zwrócić wszystkich książek.");

            ClearForms();
        }

        /// <summary>
        /// Zwrot książek z listy wybranych do zwrotu przez danego czytelnika.
        /// Uaktualnienie tabeli Wypozyczenie poprzez LINQ to SQL.
        /// </summary>
        private bool ReturnBooks()
        {
            int penaltiesSum = 0;
            bool result = true;

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                foreach (BookInfo bookInfo in BooksToReturn)
                {
                    // przeprowadzana jest transakcja dla każdej z książek
                    // z osobna, aby w razie błędu wycofać tylko te zwroty,
                    // które nie przebiegły pomyślnie

                    try
                    {
                        /* pobranie informacji o zwracanej ksiażce */
                        int bookId = bookInfo.Id;
                        Wypozyczenie borrow = db.Wypozyczenies
                            .Where(w => w.IdEgzemplarza == bookId
                                && w.IdCzytelnika == ReaderId
                                && w.DataZwrotu == null)
                            .FirstOrDefault();
                        if (borrow == null)
                        {
                            Utils.ShowErrorMsgBox(
                                "Nie znaleziono informacji o wypożyczeniu ksiażki o id = "
                                + bookId);
                            result = false;
                            continue;
                        }
                        
                        // Aktualizacja tabeli Wypożyczenie oraz wstawienie danych
                        // do tabeli Kara - wszystko w ramach jednej transakcji

                        borrow.DataZwrotu = DateTime.Now;
                        borrow.IdPrzyjmujacegoZwrot = LibrarianId;

                        /* nałożenie kar */

                        // kara za zwrócenie po terminie
                        if (bookInfo.ReturnDatePenaltyHeight > 0)
                        {
                            Kara penalty = new Kara();
                            penalty.IdCzytelnika = ReaderId;
                            penalty.IdBibliotekarza = LibrarianId;
                            penalty.DataNalozenia = DateTime.Now;
                            penalty.TypPrzewinienia = 1;
                            penalty.Kwota = bookInfo.ReturnDatePenaltyHeight;
                            db.Karas.InsertOnSubmit(penalty);
                            penaltiesSum += bookInfo.ReturnDatePenaltyHeight;
                        }

                        // kara za zniszczenie książki
                        if (bookInfo.DamagedPenaltyHeight > 0)
                        {
                            Kara penalty = new Kara();
                            penalty.IdCzytelnika = ReaderId;
                            penalty.IdBibliotekarza = LibrarianId;
                            penalty.DataNalozenia = DateTime.Now;
                            penalty.TypPrzewinienia = 2;
                            penalty.Kwota = bookInfo.DamagedPenaltyHeight;
                            db.Karas.InsertOnSubmit(penalty);
                            penaltiesSum += bookInfo.DamagedPenaltyHeight;
                        }

                        /* aktualizacja stanu technicznego książki */
                        {
                            var bookState = db.Egzemplarzs
                                .Where(e => e.IdEgzemplarza == bookId)
                                .First();
                            bookState.Stan = (byte)bookInfo.Condition;
                        }

                        db.SubmitChanges();
                    }

                    catch
                    {
                        Utils.ShowExclamationMsgBox(
                            "Nie udało się przeprowadzić zwrotu jednej z książek.");
                        result = false;

                        // nie musimy robić rollback, ponieważ SubmitChanges,
                        // w razie nie udanej operacji, sam cofnie zmiany
                    }
                }

                if (penaltiesSum > 0)
                    Utils.ShowInfoMsgBox("Nałożono na czytelnika kary w łącznej kwocie " + penaltiesSum + " zł.");

                return result;
            }
        }


        private void btnAddBook_Click(object sender, EventArgs ev)
        {
            int damagedPenaltyHeight = 0;

            if (cbCondition.SelectedIndex < 0)
                Utils.ShowExclamationMsgBox("Nie wybrano stanu książki.");

            else if (cbCondition.SelectedIndex == 0 &&
                    (!Int32.TryParse(tbPenaltyHeight.Text, out damagedPenaltyHeight) || damagedPenaltyHeight < 0))
                Utils.ShowExclamationMsgBox("Nie udało się odczytać wysokości kary za zniszczoną książkę.");

            else if (BooksToReturn.FindIndex(b => b.Id == CurrentBookInfo.Id) >= 0)
                Utils.ShowExclamationMsgBox("Książka jest już na liście.");

            else
            {
                CurrentBookInfo.Condition = cbCondition.SelectedIndex;
                CurrentBookInfo.DamagedPenaltyHeight = damagedPenaltyHeight;

                this.listBooksToReturn.Items.Add(CurrentBookInfo.InfoText);
                BooksToReturn.Add(CurrentBookInfo);

                this.btnAddBook.Enabled = false;
                this.tbBookId.Text = String.Empty;
                this.cbCondition.SelectedIndex = -1;
                this.labReturnDate.Text = String.Empty;
                this.labReturnDateExceeded.Visible = false;
                this.labReturnDatePenaltyHeight.Text = String.Empty;
            }
        }

        private void btnRemoveSelectedBook_Click(object sender, EventArgs e)
        {
            int selId = this.listBooksToReturn.SelectedIndex;
            if (selId < 0 || selId >= BooksToReturn.Count)
                return;
            this.listBooksToReturn.Items.RemoveAt(selId);
            BooksToReturn.RemoveAt(selId);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearForms();
        }

        private void ClearForms()
        {
            this.tbReaderId.Text = String.Empty;
            this.tbBookId.Text = String.Empty;
            this.cbLibrarian.SelectedIndex = -1;
            this.listBooksToReturn.Items.Clear();
            this.labReaderInfo.Text = String.Empty;
            this.listBorrowedBooks.Items.Clear();

            this.btnAddBook.Enabled = false;
            this.labReturnDate.Text = String.Empty;
            this.labReturnDateExceeded.Visible = false;
            this.labReturnDatePenaltyHeight.Text = String.Empty;
            this.cbCondition.SelectedIndex = -1;

            ReaderId = 0;
            BooksToReturn.Clear();
        }

        private bool ReadSelectedLibrarianId(out int id)
        {
            id = 0;
            if (this.cbLibrarian.SelectedIndex < 0)
            {
                Utils.ShowExclamationMsgBox("Nie wybrano bibliotekarza.");
                return false;
            }

            String libItem = this.cbLibrarian.SelectedItem.ToString();
            if (!Int32.TryParse(libItem.Substring(1, libItem.IndexOf(']') - 1), out id))
            {
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id bibliotekarza.");
                return false;
            }

            return true;
        }

        private void tbReaderId_Leave(object sender, EventArgs ev)
        {
            int parsedId;
            if (!Int32.TryParse(tbReaderId.Text, out parsedId))
            {
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id czytelnika.");
                ReaderId = 0;
                this.tbReaderId.Text = string.Empty;
            }
            else
            {
                /* wczytanie informacji o czytelniku */
                DataClassesDataContext db = new DataClassesDataContext();
                var readerInfo = (from r in db.Czytelniks
                                  where r.IdCzytelnika == parsedId
                                  select r).FirstOrDefault();
                if (readerInfo == null)
                {
                    Utils.ShowExclamationMsgBox("Nie znaleziono czytelnika o id = " + parsedId);
                    this.tbReaderId.Text = String.Empty;
                    return;
                }

                labReaderInfo.Text = String.Format("[{0}] {1} {2}{3}",
                    readerInfo.IdCzytelnika, readerInfo.Imie, readerInfo.Nazwisko,
                    (readerInfo.Miasto == null ? String.Empty : ", " + readerInfo.Miasto));

                ReaderId = parsedId;

                /* ksiązki wypozyczone przez czytelnika, które są do oddania */
                var borrowedBooks = (from w in db.Wypozyczenies
                                    where w.IdCzytelnika == ReaderId && w.DataZwrotu == null
                                    select new
                                    {
                                        BookId = w.IdEgzemplarza,
                                        Title = w.Egzemplarz.Wydanie.Tytul.NazwaTytulu,
                                        BorrowDate = w.DataWypozyczenia,
                                        BorrowLength = w.LiczbaDni
                                    }).ToArray();

                listBorrowedBooks.Items.Clear();
                for (int b = 0; b < borrowedBooks.Length; b++)
                {
                    var book = borrowedBooks[b];
                    listBorrowedBooks.Items.Add(String.Format("[{0}] {1} (wypożyczono: {2}) {3}",
                        book.BookId, book.Title, book.BorrowDate,
                        (DateTime.Now > book.BorrowDate.AddDays(book.BorrowLength) ? " !" : "")));
                }
            }
        }

        private void btnConfirmBookId_Click(object sender, EventArgs ev)
        {
            int bookId;
            if (tbBookId.Text.Length == 0 || !Int32.TryParse(tbBookId.Text, out bookId) || bookId < 0)
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id książki.");

            else if (ReaderId < 1)
                Utils.ShowExclamationMsgBox("Wprowadź id czytelnika.");

            else
            {
                /* Sprawdzenie, czy faktycznie była
                 * wypozyczona i nieoddana przez tego czytelnika. */
                DataClassesDataContext db = new DataClassesDataContext();
                var borrowInfo = (from w in db.Wypozyczenies
                                  where w.IdCzytelnika == ReaderId
                                     && w.IdEgzemplarza == bookId
                                     && w.DataZwrotu == null
                                  select w)
                             //.OrderByDescending(w => w.IdWypozyczenia)  // ten sam rezultat jak dla 'DataWypozyczenia'
                             .FirstOrDefault();
                if (borrowInfo == null)
                {
                    Utils.ShowExclamationMsgBox(
                        "Nie znaleziono wypożyczonej i niezwróconej książki o id = " + bookId +
                        " przez czytelnika o id = " + ReaderId +
                        ".\n\nByć może wpisano błedne id lub książka została już oddana.");
                    return;
                }

                /* Pobranie informacji o książce.  */
                var bookInfo = (from e in db.Egzemplarzs
                                where e.IdEgzemplarza == bookId
                                select new
                                {
                                    Title = e.Wydanie.Tytul.NazwaTytulu,
                                    ISBN = e.Wydanie.ISBN,
                                    Year = e.Wydanie.Rok,
                                    Condition = e.Stan
                                })
                               .FirstOrDefault();

                CurrentBookInfo = new BookInfo();
                CurrentBookInfo.Id = bookId;

                CurrentBookInfo.InfoText = String.Format("[{0}] {1} ({2}, {3}) [{4}]",
                    borrowInfo.IdWypozyczenia, bookInfo.Title, bookInfo.ISBN,
                    bookInfo.Year, bookId);
                
                this.cbCondition.SelectedIndex = bookInfo.Condition;
                this.labReturnDate.Text = (borrowInfo.DataWypozyczenia.AddDays(borrowInfo.LiczbaDni)).ToString("D");

                TimeSpan holdTime = DateTime.Now.Subtract(borrowInfo.DataWypozyczenia);
                if (holdTime.Days > borrowInfo.LiczbaDni)
                {
                    CurrentBookInfo.ReturnDatePenaltyHeight = holdTime.Days - borrowInfo.LiczbaDni;
                    this.labReturnDateExceeded.Visible = true;
                    this.labReturnDatePenaltyHeight.Text =
                        String.Format("{0} zł", CurrentBookInfo.ReturnDatePenaltyHeight.ToString());
                }
                else
                    CurrentBookInfo.ReturnDatePenaltyHeight = 0;

                this.btnAddBook.Enabled = true;
            }
        }

        private void cbCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.tbPenaltyHeight.Enabled = (this.cbCondition.SelectedIndex == 0);
            if (this.tbPenaltyHeight.Enabled == false)
                this.tbPenaltyHeight.Text = String.Empty;
        }
    }
}
