﻿namespace Bibliotekarz
{
    partial class ProlongBorrowForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBorrowedBooks = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.labReaderInfo = new System.Windows.Forms.Label();
            this.cbLibrarian = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbReaderId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnProlongBorrow = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbDays = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // listBorrowedBooks
            // 
            this.listBorrowedBooks.FormattingEnabled = true;
            this.listBorrowedBooks.Location = new System.Drawing.Point(17, 106);
            this.listBorrowedBooks.Name = "listBorrowedBooks";
            this.listBorrowedBooks.Size = new System.Drawing.Size(363, 56);
            this.listBorrowedBooks.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(281, 26);
            this.label6.TabIndex = 29;
            this.label6.Text = "Wypożyczone książki:\r\n(zaznacz ksiażkę której wypożyczenie chcesz przedłużyć)";
            // 
            // labReaderInfo
            // 
            this.labReaderInfo.AutoSize = true;
            this.labReaderInfo.Location = new System.Drawing.Point(14, 40);
            this.labReaderInfo.Name = "labReaderInfo";
            this.labReaderInfo.Size = new System.Drawing.Size(126, 13);
            this.labReaderInfo.TabIndex = 28;
            this.labReaderInfo.Text = "<informacje o czytelniku>";
            // 
            // cbLibrarian
            // 
            this.cbLibrarian.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLibrarian.FormattingEnabled = true;
            this.cbLibrarian.Location = new System.Drawing.Point(84, 184);
            this.cbLibrarian.Name = "cbLibrarian";
            this.cbLibrarian.Size = new System.Drawing.Size(200, 21);
            this.cbLibrarian.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Bibliotekarz:";
            // 
            // tbReaderId
            // 
            this.tbReaderId.Location = new System.Drawing.Point(89, 17);
            this.tbReaderId.Name = "tbReaderId";
            this.tbReaderId.Size = new System.Drawing.Size(200, 20);
            this.tbReaderId.TabIndex = 0;
            this.tbReaderId.Leave += new System.EventHandler(this.tbReaderId_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Id czytelnika:";
            // 
            // btnProlongBorrow
            // 
            this.btnProlongBorrow.Location = new System.Drawing.Point(84, 285);
            this.btnProlongBorrow.Name = "btnProlongBorrow";
            this.btnProlongBorrow.Size = new System.Drawing.Size(219, 29);
            this.btnProlongBorrow.TabIndex = 4;
            this.btnProlongBorrow.Text = "Przedłuż wypożyczenie wybranej książki";
            this.btnProlongBorrow.UseVisualStyleBackColor = true;
            this.btnProlongBorrow.Click += new System.EventHandler(this.btnProlongBorrow_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(295, 14);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(75, 23);
            this.btnConfirm.TabIndex = 30;
            this.btnConfirm.Text = "Zatwierdź";
            this.btnConfirm.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 228);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Liczba dni przedłużenia:";
            // 
            // cbDays
            // 
            this.cbDays.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDays.FormattingEnabled = true;
            this.cbDays.Items.AddRange(new object[] {
            "14",
            "30",
            "60"});
            this.cbDays.Location = new System.Drawing.Point(142, 225);
            this.cbDays.Name = "cbDays";
            this.cbDays.Size = new System.Drawing.Size(142, 21);
            this.cbDays.TabIndex = 3;
            // 
            // ProlongBorrowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnProlongBorrow);
            this.Controls.Add(this.listBorrowedBooks);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labReaderInfo);
            this.Controls.Add(this.cbDays);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbLibrarian);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbReaderId);
            this.Controls.Add(this.label1);
            this.Name = "ProlongBorrowForm";
            this.Size = new System.Drawing.Size(402, 331);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBorrowedBooks;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labReaderInfo;
        private System.Windows.Forms.ComboBox cbLibrarian;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbReaderId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnProlongBorrow;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbDays;
    }
}
