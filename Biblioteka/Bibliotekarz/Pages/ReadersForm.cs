﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class ReadersForm : UserControl
    {
        public ReadersForm()
        {
            InitializeComponent();
            cbGender.SelectedIndex = 0;
            LoadData();
            this.dataGridReaders.Columns["Id"].Width = 40;
            this.dataGridReaders.Columns["Płeć"].Width = 40;
            this.dataGridReaders.Columns["SumaNiezapłaconychKar"].DefaultCellStyle.Format = "0.00";
        }

        /// <summary>
        /// Wczytaj dane czytelników przy użyciu ADO.NET.
        /// </summary>
        private void LoadData()
        {
            using (BiblEntities entities = new BiblEntities())
            {
                var readers = entities.Czytelnik.Select(r => new
                    {
                        Id = r.IdCzytelnika,
                        Imię = r.Imie,
                        Nazwisko = r.Nazwisko,
                        Płeć = r.Plec,
                        Miasto = r.Miasto,
                        AktywnychWypożyczeń = r.Wypozyczenie.Where(w => w.DataZwrotu == null).Count(),
                        WszystkichWypożyczeń = r.Wypozyczenie.Count(),
                        AktywnychRezerwacji = r.Rezerwacja.Count(),
                        SumaNiezapłaconychKar = (Decimal?) r.Kara.Where(k => k.DataPlatnosci == null).Select(k => k.Kwota).Sum()
                    });

                /* filtry wyszukiwania */

                int minActiveBorrows, minReserves;

                if (tbFirstName.Text.Length > 0)
                    readers = readers.Where(a => a.Imię.Contains(tbFirstName.Text));
                if (tbLastName.Text.Length > 0)
                    readers = readers.Where(a => a.Nazwisko.Contains(tbLastName.Text));
                if (cbGender.SelectedIndex > 0)
                    readers = readers.Where(a => a.Płeć == (cbGender.SelectedIndex == 1 ? "m" : "k"));
                if (tbCity.Text.Length > 0)
                    readers = readers.Where(a => a.Miasto.Contains(tbCity.Text));
                if (tbMinActiveBorrows.Text.Length > 0 && Int32.TryParse(tbMinActiveBorrows.Text, out minActiveBorrows))
                    readers = readers.Where(a => a.AktywnychWypożyczeń >= minActiveBorrows);
                if (tbMinReserves.Text.Length > 0 && Int32.TryParse(tbMinReserves.Text, out minReserves))
                    readers = readers.Where(a => a.AktywnychRezerwacji >= minReserves);

                this.dataGridReaders.DataSource = readers.ToList();
            }
        }

        private void dataGrid_SelectionChanged(object sender, EventArgs e)
        {
            /* informacje o czytelniku */
            if (dataGridReaders.SelectedRows.Count < 1) return;
            int readerId = (int) dataGridReaders.SelectedRows[0].Cells[0].Value;
            efCzytelnik readerInfo = null;

            using (BiblEntities entities = new BiblEntities())
            {
                readerInfo = entities.Czytelnik.Where(c => c.IdCzytelnika == readerId).FirstOrDefault();
            }
            if (readerInfo == null)
            {
                Utils.ShowErrorMsgBox("Nie udało się pobrać danych czytelnika o id = " + readerId);
                return;
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(readerInfo.Imie + " " + readerInfo.Nazwisko);
            sb.AppendLine();
            sb.AppendLine("Płeć: " + (readerInfo.Plec.Equals("m") ? "mężczyzna" : "kobieta"));
            sb.AppendLine("Data urodzin: " + readerInfo.DataUrodzenia.Value.ToShortDateString());
            sb.AppendLine("Data rejestracji: " + readerInfo.DataRejestracji);
            sb.AppendLine();
            if (readerInfo.Miasto != null && readerInfo.Miasto.Length > 0)
            {
                sb.AppendLine(String.Format("{0}, {1}, {2}", readerInfo.Miasto, readerInfo.Ulica, readerInfo.NrPosesji));
            }
            sb.AppendLine("Telefon: " + readerInfo.NrTelefonu);
            sb.AppendLine("Email: " + readerInfo.Email);

            this.labReaderInfo.Text = sb.ToString();

            /* tabelka z aktywnymi wypożyczeniami */

            using (BiblEntities entities = new BiblEntities())
            {
                // pobranie aktywnych wypożyczeń czytelnika poprzez procedurę składowaną
                var borrows = entities.AktywneWypozyczenia(readerId);
                this.dataGridBorrows.DataSource = borrows.ToList();
            }
            this.dataGridBorrows.Columns[0].Width = 40;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearForms();
            LoadData();
        }

        private void ClearForms()
        {
            foreach (Control ctrl in this.Controls)
                if (ctrl.GetType() == typeof(TextBox))
                    ((TextBox)ctrl).Text = String.Empty;
            cbGender.SelectedIndex = 0;
        }
    }
}
