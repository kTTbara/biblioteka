﻿namespace Bibliotekarz
{
    partial class NewEditionForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbISBN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbYear = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelectTitle = new System.Windows.Forms.Button();
            this.labTitle = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSelectAuthor = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.labAuthors = new System.Windows.Forms.Label();
            this.btnSelectPublisher = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.labPublisher = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbAddedBooks = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbISBN
            // 
            this.tbISBN.BackColor = System.Drawing.Color.LightYellow;
            this.tbISBN.Location = new System.Drawing.Point(90, 48);
            this.tbISBN.MaxLength = 13;
            this.tbISBN.Name = "tbISBN";
            this.tbISBN.Size = new System.Drawing.Size(231, 20);
            this.tbISBN.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ISBN (13 cyfr):";
            // 
            // tbYear
            // 
            this.tbYear.Location = new System.Drawing.Point(90, 75);
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(100, 20);
            this.tbYear.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Rok wydania:";
            // 
            // btnSelectTitle
            // 
            this.btnSelectTitle.Location = new System.Drawing.Point(52, 112);
            this.btnSelectTitle.Name = "btnSelectTitle";
            this.btnSelectTitle.Size = new System.Drawing.Size(107, 23);
            this.btnSelectTitle.TabIndex = 2;
            this.btnSelectTitle.Text = "Wybierz tytuł";
            this.btnSelectTitle.UseVisualStyleBackColor = true;
            this.btnSelectTitle.Click += new System.EventHandler(this.btnSelectTitle_Click);
            // 
            // labTitle
            // 
            this.labTitle.AutoSize = true;
            this.labTitle.ForeColor = System.Drawing.Color.Maroon;
            this.labTitle.Location = new System.Drawing.Point(87, 138);
            this.labTitle.Name = "labTitle";
            this.labTitle.Size = new System.Drawing.Size(0, 13);
            this.labTitle.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Tytuł:";
            // 
            // btnSelectAuthor
            // 
            this.btnSelectAuthor.Location = new System.Drawing.Point(55, 235);
            this.btnSelectAuthor.Name = "btnSelectAuthor";
            this.btnSelectAuthor.Size = new System.Drawing.Size(120, 23);
            this.btnSelectAuthor.TabIndex = 4;
            this.btnSelectAuthor.Text = "Dodaj autora";
            this.btnSelectAuthor.UseVisualStyleBackColor = true;
            this.btnSelectAuthor.Click += new System.EventHandler(this.btnSelectAuthor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Autorzy:";
            // 
            // labAuthors
            // 
            this.labAuthors.AutoSize = true;
            this.labAuthors.BackColor = System.Drawing.Color.WhiteSmoke;
            this.labAuthors.ForeColor = System.Drawing.Color.Maroon;
            this.labAuthors.Location = new System.Drawing.Point(55, 278);
            this.labAuthors.MinimumSize = new System.Drawing.Size(200, 20);
            this.labAuthors.Name = "labAuthors";
            this.labAuthors.Size = new System.Drawing.Size(200, 20);
            this.labAuthors.TabIndex = 10;
            // 
            // btnSelectPublisher
            // 
            this.btnSelectPublisher.Location = new System.Drawing.Point(52, 169);
            this.btnSelectPublisher.Name = "btnSelectPublisher";
            this.btnSelectPublisher.Size = new System.Drawing.Size(107, 23);
            this.btnSelectPublisher.TabIndex = 3;
            this.btnSelectPublisher.Text = "Wybierz wydawcę";
            this.btnSelectPublisher.UseVisualStyleBackColor = true;
            this.btnSelectPublisher.Click += new System.EventHandler(this.btnSelectPublisher_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Wydawca:";
            // 
            // labPublisher
            // 
            this.labPublisher.AutoSize = true;
            this.labPublisher.ForeColor = System.Drawing.Color.Maroon;
            this.labPublisher.Location = new System.Drawing.Point(117, 199);
            this.labPublisher.Name = "labPublisher";
            this.labPublisher.Size = new System.Drawing.Size(0, 13);
            this.labPublisher.TabIndex = 13;
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(66, 451);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(246, 34);
            this.btnConfirm.TabIndex = 6;
            this.btnConfirm.Text = "Zatwierdź dane i dodaj wydanie";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 402);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(260, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Możesz wprowadzić liczbę dodawanych egzemplarzy:";
            // 
            // tbAddedBooks
            // 
            this.tbAddedBooks.Location = new System.Drawing.Point(301, 399);
            this.tbAddedBooks.MaxLength = 4;
            this.tbAddedBooks.Name = "tbAddedBooks";
            this.tbAddedBooks.Size = new System.Drawing.Size(82, 20);
            this.tbAddedBooks.TabIndex = 5;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(15, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(104, 23);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Wyczyść pola";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // NewEditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.tbAddedBooks);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.labPublisher);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSelectPublisher);
            this.Controls.Add(this.labAuthors);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSelectAuthor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labTitle);
            this.Controls.Add(this.btnSelectTitle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbYear);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbISBN);
            this.Name = "NewEditionForm";
            this.Size = new System.Drawing.Size(425, 502);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbISBN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbYear;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectTitle;
        private System.Windows.Forms.Label labTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSelectAuthor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labAuthors;
        private System.Windows.Forms.Button btnSelectPublisher;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labPublisher;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbAddedBooks;
        private System.Windows.Forms.Button btnClear;
    }
}
