﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Bibliotekarz
{
    public partial class ReadersSummaryForm : UserControl
    {
        public ReadersSummaryForm()
        {
            InitializeComponent();
            //LoadData();
        }

        int ReadersCount;


        public bool LoadData()
        {
            try
            {
                using (BiblEntities entities = new BiblEntities())
                {
                    ReadersCount = entities.Czytelnik.Count();
                    labRegisteredReaders.Text = "Zarejestrowanych czytelników: " + ReadersCount;

                    MostBorrows(entities);
                    MostProlongs(entities);
                    MostPenulties(entities);
                    OldestReaders(entities);
                    YoungestReaders(entities);
                    ReadersByCitiesChart(entities);
                }
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMsgBox(ex);
                return false;
            }
            return true;
        }


        private void MostBorrows(BiblEntities entities)
        {
            /* najwięcej wypożyczeń */
            var mostBorrows = entities.Czytelnik
                .OrderByDescending(r => r.Wypozyczenie.Count)
                .Take(3).Select(r => new
                    {
                        Wypożyczeń = r.Wypozyczenie.Count,
                        Imię       = r.Imie,
                        Nazwisko   = r.Nazwisko,
                        IdCzyt     = r.IdCzytelnika,
                        Miasto     = r.Miasto
                    }).ToList();

            labMostBorrows.Text = String.Empty;
            foreach (var resultRow in mostBorrows)
                labMostBorrows.Text +=
                    String.Format("{0} | {1} {2} ({3}), {4}\n",
                        resultRow.Wypożyczeń, resultRow.Imię,
                        resultRow.Nazwisko, resultRow.IdCzyt, resultRow.Miasto);
        }

        private void MostProlongs(BiblEntities entities)
        {
            /* najwięcej przedłużeń wypożyczeń */
            var mostProlongs = entities.Czytelnik
                .Select(r => new
                {
                    Przedłużeń = (entities.Przedluzenie
                        .Where(p => p.Wypozyczenie.IdCzytelnika == r.IdCzytelnika)
                        .Count()),
                    Imię = r.Imie,
                    Nazwisko = r.Nazwisko,
                    IdCzyt = r.IdCzytelnika,
                    Miasto = r.Miasto
                })
                .OrderByDescending(x => x.Przedłużeń)
                .Take(3).ToList();

            labMostProlongs.Text = String.Empty;
            foreach (var resultRow in mostProlongs)
                labMostProlongs.Text +=
                    String.Format("{0} | {1} {2} ({3}), {4}\n",
                        resultRow.Przedłużeń, resultRow.Imię,
                        resultRow.Nazwisko, resultRow.IdCzyt, resultRow.Miasto);
        }

        private void MostPenulties(BiblEntities entities)
        {
            /* najwięcej kar */
            var mostPenulties = entities.Czytelnik
                .OrderByDescending(r => r.Kara.Count)
                .Take(3).Select(r => new
                {
                    Kar = r.Kara.Count,
                    Imię = r.Imie,
                    Nazwisko = r.Nazwisko,
                    IdCzyt = r.IdCzytelnika,
                    Miasto = r.Miasto
                }).ToList();

            labMostPenulties.Text = String.Empty;
            foreach (var resultRow in mostPenulties)
                labMostPenulties.Text +=
                    String.Format("{0} | {1} {2} ({3}), {4}\n",
                        resultRow.Kar, resultRow.Imię,
                        resultRow.Nazwisko, resultRow.IdCzyt, resultRow.Miasto);
        }

        private void OldestReaders(BiblEntities entities)
        {
            /* najstarsi czytelnicy */
            var oldestReaders = entities.Czytelnik
                .OrderByDescending(r => r.DataUrodzenia)
                .Take(3).Select(r => new
                {
                    DataUrodzenia = r.DataUrodzenia,
                    Imię = r.Imie,
                    Nazwisko = r.Nazwisko,
                    IdCzyt = r.IdCzytelnika,
                    Miasto = r.Miasto
                }).ToList();

            labOldestReaders.Text = String.Empty;
            foreach (var resultRow in oldestReaders)
                labOldestReaders.Text +=
                    String.Format("{0} ({1}) | {2} {3} ({5}), {5}\n",
                        resultRow.DataUrodzenia.Value.ToString("dd.MM.yyyy"),
                        Utils.DatesDiffInYears(resultRow.DataUrodzenia.Value, DateTime.Now),
                        resultRow.Imię, resultRow.Nazwisko, resultRow.IdCzyt, resultRow.Miasto);
        }

        private void YoungestReaders(BiblEntities entities)
        {
            /* najmłodsi czytelnicy */
            var youngestReaders = entities.Czytelnik
                .OrderBy(r => r.DataUrodzenia)
                .Take(3).Select(r => new
                {
                    DataUrodzenia = r.DataUrodzenia,
                    Imię = r.Imie,
                    Nazwisko = r.Nazwisko,
                    IdCzyt = r.IdCzytelnika,
                    Miasto = r.Miasto
                }).ToList();

            labYoungestReaders.Text = String.Empty;
            foreach (var resultRow in youngestReaders)
                labYoungestReaders.Text +=
                    String.Format("{0} ({1}) | {2} {3} ({5}), {5}\n",
                        resultRow.DataUrodzenia.Value.ToString("dd.MM.yyyy"),
                        Utils.DatesDiffInYears(resultRow.DataUrodzenia.Value, DateTime.Now),
                        resultRow.Imię, resultRow.Nazwisko, resultRow.IdCzyt, resultRow.Miasto);
        }

        private void ReadersByCitiesChart(BiblEntities entities)
        {
            /* wykres miejsc zamieszkania */
            var readersInCities = entities.Czytelnik
                .Select(r => r.Miasto)
                .Distinct()
                .Select(x => new
                {
                    Miasto = x,
                    Czytelników = (entities.Czytelnik.Where(r => r.Miasto == x).Count())
                }).ToList();
            chartCities.Series[0].Points.Clear();

            foreach (var resultRow in readersInCities)
            {
                DataPoint point = new DataPoint();
                point.YValues = new double[] { resultRow.Czytelników };
                point.Label = String.Format("{0:##.0}%",
                    100 * resultRow.Czytelników / (double)ReadersCount);
                point.LegendText = String.Format("{0} ({1} - {2:##.0}%)",
                    resultRow.Miasto, resultRow.Czytelników,
                    100 * resultRow.Czytelników / (double)ReadersCount);
                point.LabelToolTip = point.LegendText;
                chartCities.Series[0].Points.Add(point);
            }
            chartCities.Visible = true;
        }


        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
