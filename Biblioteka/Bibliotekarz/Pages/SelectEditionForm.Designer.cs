﻿namespace Bibliotekarz
{
    partial class SelectEditionForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbMinBooksCount = new System.Windows.Forms.TextBox();
            this.tbYear = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPublisherName = new System.Windows.Forms.TextBox();
            this.tbPublisherId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.tbTitleId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbISBN = new System.Windows.Forms.TextBox();
            this.tbEditionId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridEditions = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.labAuthors = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbAuthor = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEditions)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(527, 404);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(200, 27);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Wyszukaj / odśwież";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbMinBooksCount
            // 
            this.tbMinBooksCount.Location = new System.Drawing.Point(448, 339);
            this.tbMinBooksCount.Name = "tbMinBooksCount";
            this.tbMinBooksCount.Size = new System.Drawing.Size(164, 20);
            this.tbMinBooksCount.TabIndex = 8;
            // 
            // tbYear
            // 
            this.tbYear.Location = new System.Drawing.Point(96, 365);
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(201, 20);
            this.tbYear.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(321, 342);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Min. liczba egzemplarzy:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 368);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Rok:";
            // 
            // tbPublisherName
            // 
            this.tbPublisherName.Location = new System.Drawing.Point(411, 313);
            this.tbPublisherName.Name = "tbPublisherName";
            this.tbPublisherName.Size = new System.Drawing.Size(201, 20);
            this.tbPublisherName.TabIndex = 6;
            // 
            // tbPublisherId
            // 
            this.tbPublisherId.Location = new System.Drawing.Point(96, 339);
            this.tbPublisherId.Name = "tbPublisherId";
            this.tbPublisherId.Size = new System.Drawing.Size(201, 20);
            this.tbPublisherId.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(321, 316);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Nazwa wydawcy:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 342);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Id wydawcy:";
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(411, 287);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(201, 20);
            this.tbTitle.TabIndex = 4;
            // 
            // tbTitleId
            // 
            this.tbTitleId.Location = new System.Drawing.Point(96, 313);
            this.tbTitleId.Name = "tbTitleId";
            this.tbTitleId.Size = new System.Drawing.Size(201, 20);
            this.tbTitleId.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(331, 290);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Tytuł zawiera:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 316);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Id tytułu:";
            // 
            // tbISBN
            // 
            this.tbISBN.Location = new System.Drawing.Point(96, 391);
            this.tbISBN.MaxLength = 13;
            this.tbISBN.Name = "tbISBN";
            this.tbISBN.Size = new System.Drawing.Size(201, 20);
            this.tbISBN.TabIndex = 2;
            // 
            // tbEditionId
            // 
            this.tbEditionId.Location = new System.Drawing.Point(96, 287);
            this.tbEditionId.Name = "tbEditionId";
            this.tbEditionId.Size = new System.Drawing.Size(201, 20);
            this.tbEditionId.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(61, 394);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "ISBN:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 290);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Id wydania:";
            // 
            // dataGridEditions
            // 
            this.dataGridEditions.AllowUserToAddRows = false;
            this.dataGridEditions.AllowUserToDeleteRows = false;
            this.dataGridEditions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridEditions.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridEditions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridEditions.Location = new System.Drawing.Point(6, 17);
            this.dataGridEditions.MultiSelect = false;
            this.dataGridEditions.Name = "dataGridEditions";
            this.dataGridEditions.ReadOnly = true;
            this.dataGridEditions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridEditions.Size = new System.Drawing.Size(856, 255);
            this.dataGridEditions.TabIndex = 0;
            this.dataGridEditions.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridEditions_CellContentClick);
            this.dataGridEditions.SelectionChanged += new System.EventHandler(this.dataGridEditions_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, -126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(290, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Wyszukaj wydanie wg kryteriów i wybierz z tabelki (zaznacz)";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(748, 406);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Wyczyść pola";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(647, 287);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(159, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Autorzy zaznaczonego wydania:";
            // 
            // labAuthors
            // 
            this.labAuthors.BackColor = System.Drawing.Color.White;
            this.labAuthors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labAuthors.Location = new System.Drawing.Point(650, 304);
            this.labAuthors.Name = "labAuthors";
            this.labAuthors.Size = new System.Drawing.Size(209, 81);
            this.labAuthors.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(370, 368);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Autor:";
            // 
            // tbAuthor
            // 
            this.tbAuthor.Location = new System.Drawing.Point(411, 365);
            this.tbAuthor.Name = "tbAuthor";
            this.tbAuthor.Size = new System.Drawing.Size(201, 20);
            this.tbAuthor.TabIndex = 9;
            // 
            // SelectEditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labAuthors);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.tbMinBooksCount);
            this.Controls.Add(this.tbYear);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbAuthor);
            this.Controls.Add(this.tbPublisherName);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbPublisherId);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.tbTitleId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbISBN);
            this.Controls.Add(this.tbEditionId);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridEditions);
            this.Controls.Add(this.label1);
            this.Name = "SelectEditionForm";
            this.Size = new System.Drawing.Size(867, 444);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEditions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbMinBooksCount;
        private System.Windows.Forms.TextBox tbYear;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPublisherName;
        private System.Windows.Forms.TextBox tbPublisherId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.TextBox tbTitleId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbISBN;
        private System.Windows.Forms.TextBox tbEditionId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridEditions;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labAuthors;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbAuthor;
    }
}
