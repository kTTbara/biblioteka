﻿namespace Bibliotekarz
{
    partial class AddBooksForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelectEdition = new System.Windows.Forms.Button();
            this.labEdition = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbBooksNumber = new System.Windows.Forms.TextBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wydanie:";
            // 
            // btnSelectEdition
            // 
            this.btnSelectEdition.Location = new System.Drawing.Point(20, 22);
            this.btnSelectEdition.Name = "btnSelectEdition";
            this.btnSelectEdition.Size = new System.Drawing.Size(154, 23);
            this.btnSelectEdition.TabIndex = 0;
            this.btnSelectEdition.Text = "Wybierz wydanie";
            this.btnSelectEdition.UseVisualStyleBackColor = true;
            this.btnSelectEdition.Click += new System.EventHandler(this.btnSelectEdition_Click);
            // 
            // labEdition
            // 
            this.labEdition.AutoSize = true;
            this.labEdition.Location = new System.Drawing.Point(17, 61);
            this.labEdition.Name = "labEdition";
            this.labEdition.Size = new System.Drawing.Size(0, 13);
            this.labEdition.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Podaj liczbę egzemplarzy:";
            // 
            // tbBooksNumber
            // 
            this.tbBooksNumber.Location = new System.Drawing.Point(20, 126);
            this.tbBooksNumber.Name = "tbBooksNumber";
            this.tbBooksNumber.Size = new System.Drawing.Size(128, 20);
            this.tbBooksNumber.TabIndex = 1;
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(23, 178);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(125, 23);
            this.btnConfirm.TabIndex = 2;
            this.btnConfirm.Text = "Dodaj książki";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // AddBooksForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.tbBooksNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labEdition);
            this.Controls.Add(this.btnSelectEdition);
            this.Controls.Add(this.label1);
            this.Name = "AddBooksForm";
            this.Size = new System.Drawing.Size(397, 254);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelectEdition;
        private System.Windows.Forms.Label labEdition;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbBooksNumber;
        private System.Windows.Forms.Button btnConfirm;
    }
}
