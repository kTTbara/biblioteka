﻿namespace Bibliotekarz
{
    partial class BorrowsSummaryForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.groupBoxAveBorTime = new System.Windows.Forms.GroupBox();
            this.label1_aveBorTime = new System.Windows.Forms.Label();
            this.labelAllBor = new System.Windows.Forms.Label();
            this.groupBoxRealAveBorTime = new System.Windows.Forms.GroupBox();
            this.label1_realAveBorTime = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBoxProcPrzedl = new System.Windows.Forms.GroupBox();
            this.labelProcPrzedl = new System.Windows.Forms.Label();
            this.chartProcPrzedl = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBoxAveBorTime.SuspendLayout();
            this.groupBoxRealAveBorTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBoxProcPrzedl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartProcPrzedl)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(3, 3);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(103, 23);
            this.buttonRefresh.TabIndex = 0;
            this.buttonRefresh.Text = "Odśwież dane";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // groupBoxAveBorTime
            // 
            this.groupBoxAveBorTime.Controls.Add(this.label1_aveBorTime);
            this.groupBoxAveBorTime.Location = new System.Drawing.Point(3, 32);
            this.groupBoxAveBorTime.Name = "groupBoxAveBorTime";
            this.groupBoxAveBorTime.Size = new System.Drawing.Size(211, 51);
            this.groupBoxAveBorTime.TabIndex = 1;
            this.groupBoxAveBorTime.TabStop = false;
            this.groupBoxAveBorTime.Text = "Średni czas wypożyczenia";
            // 
            // label1_aveBorTime
            // 
            this.label1_aveBorTime.AutoSize = true;
            this.label1_aveBorTime.Location = new System.Drawing.Point(6, 16);
            this.label1_aveBorTime.Name = "label1_aveBorTime";
            this.label1_aveBorTime.Size = new System.Drawing.Size(10, 13);
            this.label1_aveBorTime.TabIndex = 2;
            this.label1_aveBorTime.Text = "-";
            // 
            // labelAllBor
            // 
            this.labelAllBor.AutoSize = true;
            this.labelAllBor.Location = new System.Drawing.Point(202, 8);
            this.labelAllBor.Name = "labelAllBor";
            this.labelAllBor.Size = new System.Drawing.Size(131, 13);
            this.labelAllBor.TabIndex = 2;
            this.labelAllBor.Text = "Wszystkich wypożyczeń: -";
            // 
            // groupBoxRealAveBorTime
            // 
            this.groupBoxRealAveBorTime.Controls.Add(this.label1_realAveBorTime);
            this.groupBoxRealAveBorTime.Location = new System.Drawing.Point(3, 89);
            this.groupBoxRealAveBorTime.Name = "groupBoxRealAveBorTime";
            this.groupBoxRealAveBorTime.Size = new System.Drawing.Size(211, 51);
            this.groupBoxRealAveBorTime.TabIndex = 3;
            this.groupBoxRealAveBorTime.TabStop = false;
            this.groupBoxRealAveBorTime.Text = "Średni rzeczywisty czas wypożyczenia";
            // 
            // label1_realAveBorTime
            // 
            this.label1_realAveBorTime.AutoSize = true;
            this.label1_realAveBorTime.Location = new System.Drawing.Point(6, 16);
            this.label1_realAveBorTime.Name = "label1_realAveBorTime";
            this.label1_realAveBorTime.Size = new System.Drawing.Size(10, 13);
            this.label1_realAveBorTime.TabIndex = 2;
            this.label1_realAveBorTime.Text = "-";
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.AutoFitMinFontSize = 10;
            legend1.IsTextAutoFit = false;
            legend1.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row;
            legend1.Name = "Legend3";
            legend1.Position.Auto = false;
            legend1.Position.Height = 4.516304F;
            legend1.Position.Width = 40.79272F;
            legend1.Position.X = 9.207283F;
            legend1.Position.Y = 95.4837F;
            legend1.TextWrapThreshold = 0;
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 146);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.CustomProperties = "LabelStyle=Bottom";
            series1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            series1.Label = "#VALY";
            series1.LabelBackColor = System.Drawing.Color.Transparent;
            series1.LabelBorderColor = System.Drawing.Color.Transparent;
            series1.Legend = "Legend3";
            series1.LegendText = "1, 2, 3, ..., - kolejne miesiące licząc od stycznia";
            series1.Name = "test";
            series1.SmartLabelStyle.MovingDirection = System.Windows.Forms.DataVisualization.Charting.LabelAlignmentStyles.Center;
            series1.YValuesPerPoint = 4;
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(695, 300);
            this.chart1.TabIndex = 4;
            this.chart1.Text = "chart1";
            title1.Name = "Title1";
            title1.Text = "Liczba wypożyczeń wg miesięcy";
            this.chart1.Titles.Add(title1);
            this.chart1.Visible = false;
            // 
            // groupBoxProcPrzedl
            // 
            this.groupBoxProcPrzedl.Controls.Add(this.chartProcPrzedl);
            this.groupBoxProcPrzedl.Controls.Add(this.labelProcPrzedl);
            this.groupBoxProcPrzedl.Location = new System.Drawing.Point(229, 32);
            this.groupBoxProcPrzedl.Name = "groupBoxProcPrzedl";
            this.groupBoxProcPrzedl.Size = new System.Drawing.Size(200, 108);
            this.groupBoxProcPrzedl.TabIndex = 5;
            this.groupBoxProcPrzedl.TabStop = false;
            this.groupBoxProcPrzedl.Text = "Procent przedłużanych wypożyczeń";
            // 
            // labelProcPrzedl
            // 
            this.labelProcPrzedl.AutoSize = true;
            this.labelProcPrzedl.Location = new System.Drawing.Point(7, 15);
            this.labelProcPrzedl.Name = "labelProcPrzedl";
            this.labelProcPrzedl.Size = new System.Drawing.Size(10, 13);
            this.labelProcPrzedl.TabIndex = 0;
            this.labelProcPrzedl.Text = "-";
            // 
            // chartProcPrzedl
            // 
            chartArea2.Name = "ChartArea1";
            this.chartProcPrzedl.ChartAreas.Add(chartArea2);
            legend2.Enabled = false;
            legend2.Name = "Legend1";
            this.chartProcPrzedl.Legends.Add(legend2);
            this.chartProcPrzedl.Location = new System.Drawing.Point(10, 31);
            this.chartProcPrzedl.Name = "chartProcPrzedl";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.Legend = "Legend1";
            series2.Name = "SeriesPrzedlWyp";
            series2.YValuesPerPoint = 2;
            this.chartProcPrzedl.Series.Add(series2);
            this.chartProcPrzedl.Size = new System.Drawing.Size(172, 71);
            this.chartProcPrzedl.TabIndex = 1;
            this.chartProcPrzedl.Visible = false;
            // 
            // BorrowsSummaryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxProcPrzedl);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.groupBoxRealAveBorTime);
            this.Controls.Add(this.labelAllBor);
            this.Controls.Add(this.groupBoxAveBorTime);
            this.Controls.Add(this.buttonRefresh);
            this.Name = "BorrowsSummaryForm";
            this.Size = new System.Drawing.Size(945, 502);
            this.groupBoxAveBorTime.ResumeLayout(false);
            this.groupBoxAveBorTime.PerformLayout();
            this.groupBoxRealAveBorTime.ResumeLayout(false);
            this.groupBoxRealAveBorTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBoxProcPrzedl.ResumeLayout(false);
            this.groupBoxProcPrzedl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartProcPrzedl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.GroupBox groupBoxAveBorTime;
        private System.Windows.Forms.Label label1_aveBorTime;
        private System.Windows.Forms.Label labelAllBor;
        private System.Windows.Forms.GroupBox groupBoxRealAveBorTime;
        private System.Windows.Forms.Label label1_realAveBorTime;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox groupBoxProcPrzedl;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartProcPrzedl;
        private System.Windows.Forms.Label labelProcPrzedl;
    }
}
