﻿namespace Bibliotekarz
{
    partial class ReturnBookForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbReaderId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbLibrarian = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbBookId = new System.Windows.Forms.TextBox();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.listBooksToReturn = new System.Windows.Forms.ListBox();
            this.btnRemoveSelectedBook = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.labReaderInfo = new System.Windows.Forms.Label();
            this.labReturnDatePenaltyHeight = new System.Windows.Forms.Label();
            this.labReturnDateExceeded = new System.Windows.Forms.Label();
            this.labReturnDate = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbPenaltyHeight = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbCondition = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnConfirmBookId = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.listBorrowedBooks = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Id czytelnika:";
            // 
            // tbReaderId
            // 
            this.tbReaderId.Location = new System.Drawing.Point(95, 16);
            this.tbReaderId.Name = "tbReaderId";
            this.tbReaderId.Size = new System.Drawing.Size(200, 20);
            this.tbReaderId.TabIndex = 0;
            this.tbReaderId.Leave += new System.EventHandler(this.tbReaderId_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Bibliotekarz:";
            // 
            // cbLibrarian
            // 
            this.cbLibrarian.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLibrarian.FormattingEnabled = true;
            this.cbLibrarian.Location = new System.Drawing.Point(95, 71);
            this.cbLibrarian.Name = "cbLibrarian";
            this.cbLibrarian.Size = new System.Drawing.Size(200, 21);
            this.cbLibrarian.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Id zwracanej książki:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Lista zwracanych książek:";
            // 
            // tbBookId
            // 
            this.tbBookId.Location = new System.Drawing.Point(117, 31);
            this.tbBookId.Name = "tbBookId";
            this.tbBookId.Size = new System.Drawing.Size(164, 20);
            this.tbBookId.TabIndex = 2;
            // 
            // btnAddBook
            // 
            this.btnAddBook.Enabled = false;
            this.btnAddBook.Location = new System.Drawing.Point(127, 239);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(154, 23);
            this.btnAddBook.TabIndex = 6;
            this.btnAddBook.Text = "Dodaj książkę do listy";
            this.btnAddBook.UseVisualStyleBackColor = true;
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // listBooksToReturn
            // 
            this.listBooksToReturn.FormattingEnabled = true;
            this.listBooksToReturn.Location = new System.Drawing.Point(9, 62);
            this.listBooksToReturn.Name = "listBooksToReturn";
            this.listBooksToReturn.Size = new System.Drawing.Size(372, 108);
            this.listBooksToReturn.TabIndex = 7;
            // 
            // btnRemoveSelectedBook
            // 
            this.btnRemoveSelectedBook.Location = new System.Drawing.Point(9, 177);
            this.btnRemoveSelectedBook.Name = "btnRemoveSelectedBook";
            this.btnRemoveSelectedBook.Size = new System.Drawing.Size(188, 23);
            this.btnRemoveSelectedBook.TabIndex = 8;
            this.btnRemoveSelectedBook.Text = "Usuń zaznaczoną książkę z listy";
            this.btnRemoveSelectedBook.UseVisualStyleBackColor = true;
            this.btnRemoveSelectedBook.Click += new System.EventHandler(this.btnRemoveSelectedBook_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(484, 343);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(174, 33);
            this.btnConfirm.TabIndex = 9;
            this.btnConfirm.Text = "Zatwierdź zwrot książek";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(718, 348);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(103, 23);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "Wyczyść pola";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(300, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "[id_wypozyczenia] Tytuł (ISBN, rok wydania) [id_egzemplarza]";
            // 
            // labReaderInfo
            // 
            this.labReaderInfo.AutoSize = true;
            this.labReaderInfo.Location = new System.Drawing.Point(20, 39);
            this.labReaderInfo.Name = "labReaderInfo";
            this.labReaderInfo.Size = new System.Drawing.Size(126, 13);
            this.labReaderInfo.TabIndex = 9;
            this.labReaderInfo.Text = "<informacje o czytelniku>";
            // 
            // labReturnDatePenaltyHeight
            // 
            this.labReturnDatePenaltyHeight.AutoSize = true;
            this.labReturnDatePenaltyHeight.Location = new System.Drawing.Point(9, 214);
            this.labReturnDatePenaltyHeight.Name = "labReturnDatePenaltyHeight";
            this.labReturnDatePenaltyHeight.Size = new System.Drawing.Size(71, 13);
            this.labReturnDatePenaltyHeight.TabIndex = 18;
            this.labReturnDatePenaltyHeight.Text = "<kwota kary>";
            // 
            // labReturnDateExceeded
            // 
            this.labReturnDateExceeded.AutoSize = true;
            this.labReturnDateExceeded.ForeColor = System.Drawing.Color.Red;
            this.labReturnDateExceeded.Location = new System.Drawing.Point(9, 197);
            this.labReturnDateExceeded.Name = "labReturnDateExceeded";
            this.labReturnDateExceeded.Size = new System.Drawing.Size(322, 13);
            this.labReturnDateExceeded.TabIndex = 17;
            this.labReturnDateExceeded.Text = "Przekroczono termin zwrotu! Zostanie naliczona kara w wysokości:";
            this.labReturnDateExceeded.Visible = false;
            // 
            // labReturnDate
            // 
            this.labReturnDate.AutoSize = true;
            this.labReturnDate.ForeColor = System.Drawing.Color.Teal;
            this.labReturnDate.Location = new System.Drawing.Point(9, 165);
            this.labReturnDate.Name = "labReturnDate";
            this.labReturnDate.Size = new System.Drawing.Size(103, 13);
            this.labReturnDate.TabIndex = 16;
            this.labReturnDate.Text = "<max termin zwrotu>";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(163, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Ostateczny termin zwrotu książki:";
            // 
            // tbPenaltyHeight
            // 
            this.tbPenaltyHeight.Location = new System.Drawing.Point(217, 98);
            this.tbPenaltyHeight.Name = "tbPenaltyHeight";
            this.tbPenaltyHeight.Size = new System.Drawing.Size(100, 20);
            this.tbPenaltyHeight.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(214, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 26);
            this.label8.TabIndex = 14;
            this.label8.Text = "Jeśli została zniszczona, określ\r\nwysokość kary (w pełnych zł):";
            // 
            // cbCondition
            // 
            this.cbCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCondition.FormattingEnabled = true;
            this.cbCondition.Items.AddRange(new object[] {
            "0 (zniszczona)",
            "1 (mierny)",
            "2",
            "3",
            "4",
            "5 (idealny)"});
            this.cbCondition.Location = new System.Drawing.Point(9, 98);
            this.cbCondition.Name = "cbCondition";
            this.cbCondition.Size = new System.Drawing.Size(165, 21);
            this.cbCondition.TabIndex = 4;
            this.cbCondition.SelectedIndexChanged += new System.EventHandler(this.cbCondition_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(168, 26);
            this.label9.TabIndex = 11;
            this.label9.Text = "Określ stan książki:\r\n(teraz wpisany jest poprzedni stan)";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnConfirmBookId);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.labReturnDatePenaltyHeight);
            this.groupBox1.Controls.Add(this.tbBookId);
            this.groupBox1.Controls.Add(this.labReturnDateExceeded);
            this.groupBox1.Controls.Add(this.btnAddBook);
            this.groupBox1.Controls.Add(this.labReturnDate);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbPenaltyHeight);
            this.groupBox1.Controls.Add(this.cbCondition);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(23, 107);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(397, 269);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dane zwracanej książki";
            // 
            // btnConfirmBookId
            // 
            this.btnConfirmBookId.Location = new System.Drawing.Point(287, 29);
            this.btnConfirmBookId.Name = "btnConfirmBookId";
            this.btnConfirmBookId.Size = new System.Drawing.Size(94, 23);
            this.btnConfirmBookId.TabIndex = 3;
            this.btnConfirmBookId.Text = "Zatwierdź id";
            this.btnConfirmBookId.UseVisualStyleBackColor = true;
            this.btnConfirmBookId.Click += new System.EventHandler(this.btnConfirmBookId_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.listBooksToReturn);
            this.groupBox2.Controls.Add(this.btnRemoveSelectedBook);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(440, 107);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(397, 212);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Zwracane książki";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(360, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Wypożyczone książki:";
            // 
            // listBorrowedBooks
            // 
            this.listBorrowedBooks.FormattingEnabled = true;
            this.listBorrowedBooks.Location = new System.Drawing.Point(363, 32);
            this.listBorrowedBooks.Name = "listBorrowedBooks";
            this.listBorrowedBooks.Size = new System.Drawing.Size(458, 56);
            this.listBorrowedBooks.TabIndex = 23;
            // 
            // ReturnBookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listBorrowedBooks);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labReaderInfo);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.cbLibrarian);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbReaderId);
            this.Controls.Add(this.label1);
            this.Name = "ReturnBookForm";
            this.Size = new System.Drawing.Size(898, 397);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbReaderId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbLibrarian;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbBookId;
        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.ListBox listBooksToReturn;
        private System.Windows.Forms.Button btnRemoveSelectedBook;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labReaderInfo;
        private System.Windows.Forms.Label labReturnDatePenaltyHeight;
        private System.Windows.Forms.Label labReturnDateExceeded;
        private System.Windows.Forms.Label labReturnDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbPenaltyHeight;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbCondition;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnConfirmBookId;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox listBorrowedBooks;
    }
}
