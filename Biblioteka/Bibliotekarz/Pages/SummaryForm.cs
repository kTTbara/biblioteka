﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class SummaryForm : UserControl
    {
        public SummaryForm()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            App.OpenPage("tab" + ((LinkLabel)sender).Name);
        }
    }
}
