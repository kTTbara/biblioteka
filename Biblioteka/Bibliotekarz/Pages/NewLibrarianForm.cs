﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class NewLibrarianForm : UserControl
    {
        public NewLibrarianForm()
        {
            InitializeComponent();
            this.cbGender.SelectedIndex = 0;
        }

        /// <summary>  Dodaj bibliotekarza do bazy za pomocą LINQ to SQL. </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            if (tbFirstName.Text.Length == 0 || tbLastName.Text.Length == 0)
                MessageBox.Show("Musisz podać imię i nazwisko.");
            else if (tbCity.Text.Length != 0 && (tbStreet.Text.Length == 0 || tbNumber.Text.Length == 0))
                MessageBox.Show("Skoro podałeś miasto, to podaj pełny adres.");

            else
            {
                DataClassesDataContext db = new DataClassesDataContext();
                Bibliotekarz bibl = new Bibliotekarz();

                bibl.Imie = tbFirstName.Text;
                bibl.Nazwisko = tbLastName.Text;
                String gender = cbGender.Text;
                bibl.Plec = (gender.Equals("Mężczyzna")) ? 'm' : 'k';
                if (tbCity.Text.Length > 0) bibl.Miasto = tbCity.Text;
                if (tbStreet.Text.Length > 0) bibl.Ulica = tbStreet.Text;
                if (tbNumber.Text.Length > 0) bibl.NrPosesji = tbNumber.Text;
                if (tbEmail.Text.Length > 0) bibl.Email = tbEmail.Text;
                if (tbPhone.Text.Length > 0) bibl.NrTelefonu = tbPhone.Text;

                db.Bibliotekarzs.InsertOnSubmit(bibl);
                db.SubmitChanges();

                MessageBox.Show("Dodano bibliotekarza!");

                ClearForms();
            }
        }

        private void ClearForms()
        {
            foreach (Control ctrl in this.Controls)
                if (ctrl.GetType() == typeof(TextBox))
                    ((TextBox)ctrl).Text = String.Empty;
            this.cbGender.SelectedIndex = 0;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearForms();
        }
    }
}
