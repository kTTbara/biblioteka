﻿namespace Bibliotekarz
{
    partial class SummaryForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ReadersSummary = new System.Windows.Forms.LinkLabel();
            this.BorrowsSummary = new System.Windows.Forms.LinkLabel();
            this.BooksByPublisher = new System.Windows.Forms.LinkLabel();
            this.TitleEditionsLastBorrow = new System.Windows.Forms.LinkLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.NewLibrarian = new System.Windows.Forms.LinkLabel();
            this.Librarians = new System.Windows.Forms.LinkLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RegisterReader = new System.Windows.Forms.LinkLabel();
            this.Readers = new System.Windows.Forms.LinkLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ReturnBook = new System.Windows.Forms.LinkLabel();
            this.BorrowBook = new System.Windows.Forms.LinkLabel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Publishers = new System.Windows.Forms.LinkLabel();
            this.NewEdition = new System.Windows.Forms.LinkLabel();
            this.AddBooks = new System.Windows.Forms.LinkLabel();
            this.Editions = new System.Windows.Forms.LinkLabel();
            this.Authors = new System.Windows.Forms.LinkLabel();
            this.Titles = new System.Windows.Forms.LinkLabel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ExportBorrows = new System.Windows.Forms.LinkLabel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ReadersSummary);
            this.groupBox1.Controls.Add(this.BorrowsSummary);
            this.groupBox1.Controls.Add(this.BooksByPublisher);
            this.groupBox1.Controls.Add(this.TitleEditionsLastBorrow);
            this.groupBox1.Location = new System.Drawing.Point(26, 263);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(309, 116);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zestawienia";
            // 
            // ReadersSummary
            // 
            this.ReadersSummary.AutoSize = true;
            this.ReadersSummary.Location = new System.Drawing.Point(7, 75);
            this.ReadersSummary.Name = "ReadersSummary";
            this.ReadersSummary.Size = new System.Drawing.Size(110, 13);
            this.ReadersSummary.TabIndex = 0;
            this.ReadersSummary.TabStop = true;
            this.ReadersSummary.Text = "Statystyki czytelników";
            this.ReadersSummary.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // BorrowsSummary
            // 
            this.BorrowsSummary.AutoSize = true;
            this.BorrowsSummary.Location = new System.Drawing.Point(7, 94);
            this.BorrowsSummary.Name = "BorrowsSummary";
            this.BorrowsSummary.Size = new System.Drawing.Size(113, 13);
            this.BorrowsSummary.TabIndex = 0;
            this.BorrowsSummary.TabStop = true;
            this.BorrowsSummary.Text = "Statystyki wypożyczeń";
            this.BorrowsSummary.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // BooksByPublisher
            // 
            this.BooksByPublisher.AutoSize = true;
            this.BooksByPublisher.Location = new System.Drawing.Point(7, 25);
            this.BooksByPublisher.Name = "BooksByPublisher";
            this.BooksByPublisher.Size = new System.Drawing.Size(180, 13);
            this.BooksByPublisher.TabIndex = 0;
            this.BooksByPublisher.TabStop = true;
            this.BooksByPublisher.Text = "Wydawnictwa wg wydanych tytułów";
            this.BooksByPublisher.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // TitleEditionsLastBorrow
            // 
            this.TitleEditionsLastBorrow.AutoSize = true;
            this.TitleEditionsLastBorrow.Location = new System.Drawing.Point(7, 44);
            this.TitleEditionsLastBorrow.Name = "TitleEditionsLastBorrow";
            this.TitleEditionsLastBorrow.Size = new System.Drawing.Size(115, 13);
            this.TitleEditionsLastBorrow.TabIndex = 0;
            this.TitleEditionsLastBorrow.TabStop = true;
            this.TitleEditionsLastBorrow.Text = "Tytuły wg wypożyczeń";
            this.TitleEditionsLastBorrow.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.NewLibrarian);
            this.groupBox2.Controls.Add(this.Librarians);
            this.groupBox2.Location = new System.Drawing.Point(227, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(181, 85);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pracownicy";
            // 
            // NewLibrarian
            // 
            this.NewLibrarian.AutoSize = true;
            this.NewLibrarian.Location = new System.Drawing.Point(7, 37);
            this.NewLibrarian.Name = "NewLibrarian";
            this.NewLibrarian.Size = new System.Drawing.Size(110, 13);
            this.NewLibrarian.TabIndex = 1;
            this.NewLibrarian.TabStop = true;
            this.NewLibrarian.Text = "Zatrudnij bibliotekarza";
            this.NewLibrarian.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Librarians
            // 
            this.Librarians.AutoSize = true;
            this.Librarians.Location = new System.Drawing.Point(7, 20);
            this.Librarians.Name = "Librarians";
            this.Librarians.Size = new System.Drawing.Size(101, 13);
            this.Librarians.TabIndex = 0;
            this.Librarians.TabStop = true;
            this.Librarians.Text = "Wykaz bibliotekarzy";
            this.Librarians.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RegisterReader);
            this.groupBox3.Controls.Add(this.Readers);
            this.groupBox3.Location = new System.Drawing.Point(26, 42);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(181, 85);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Czytelnicy";
            // 
            // RegisterReader
            // 
            this.RegisterReader.AutoSize = true;
            this.RegisterReader.Location = new System.Drawing.Point(7, 38);
            this.RegisterReader.Name = "RegisterReader";
            this.RegisterReader.Size = new System.Drawing.Size(106, 13);
            this.RegisterReader.TabIndex = 0;
            this.RegisterReader.TabStop = true;
            this.RegisterReader.Text = "Zarejestruj czytelnika";
            this.RegisterReader.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Readers
            // 
            this.Readers.AutoSize = true;
            this.Readers.Location = new System.Drawing.Point(7, 19);
            this.Readers.Name = "Readers";
            this.Readers.Size = new System.Drawing.Size(98, 13);
            this.Readers.TabIndex = 0;
            this.Readers.TabStop = true;
            this.Readers.Text = "Wykaz czytelników";
            this.Readers.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ReturnBook);
            this.groupBox4.Controls.Add(this.BorrowBook);
            this.groupBox4.Location = new System.Drawing.Point(26, 147);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(181, 91);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Obsługa czytelnika";
            // 
            // ReturnBook
            // 
            this.ReturnBook.AutoSize = true;
            this.ReturnBook.Location = new System.Drawing.Point(7, 45);
            this.ReturnBook.Name = "ReturnBook";
            this.ReturnBook.Size = new System.Drawing.Size(73, 13);
            this.ReturnBook.TabIndex = 0;
            this.ReturnBook.TabStop = true;
            this.ReturnBook.Text = "Zwrot książek";
            this.ReturnBook.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // BorrowBook
            // 
            this.BorrowBook.AutoSize = true;
            this.BorrowBook.Location = new System.Drawing.Point(7, 26);
            this.BorrowBook.Name = "BorrowBook";
            this.BorrowBook.Size = new System.Drawing.Size(111, 13);
            this.BorrowBook.TabIndex = 0;
            this.BorrowBook.TabStop = true;
            this.BorrowBook.Text = "Wypożyczenie książki";
            this.BorrowBook.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Publishers);
            this.groupBox5.Controls.Add(this.NewEdition);
            this.groupBox5.Controls.Add(this.AddBooks);
            this.groupBox5.Controls.Add(this.Editions);
            this.groupBox5.Controls.Add(this.Authors);
            this.groupBox5.Controls.Add(this.Titles);
            this.groupBox5.Location = new System.Drawing.Point(424, 42);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(181, 166);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Książki, autorzy, wydawcy";
            // 
            // Publishers
            // 
            this.Publishers.AutoSize = true;
            this.Publishers.Location = new System.Drawing.Point(6, 89);
            this.Publishers.Name = "Publishers";
            this.Publishers.Size = new System.Drawing.Size(96, 13);
            this.Publishers.TabIndex = 1;
            this.Publishers.TabStop = true;
            this.Publishers.Text = "Wykaz wydawców";
            this.Publishers.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // NewEdition
            // 
            this.NewEdition.AutoSize = true;
            this.NewEdition.Location = new System.Drawing.Point(7, 120);
            this.NewEdition.Name = "NewEdition";
            this.NewEdition.Size = new System.Drawing.Size(77, 13);
            this.NewEdition.TabIndex = 1;
            this.NewEdition.TabStop = true;
            this.NewEdition.Text = "Dodaj wydanie";
            this.NewEdition.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // AddBooks
            // 
            this.AddBooks.AutoSize = true;
            this.AddBooks.Location = new System.Drawing.Point(7, 138);
            this.AddBooks.Name = "AddBooks";
            this.AddBooks.Size = new System.Drawing.Size(136, 13);
            this.AddBooks.TabIndex = 0;
            this.AddBooks.TabStop = true;
            this.AddBooks.Text = "Dodaj egzemplarze książek";
            this.AddBooks.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Editions
            // 
            this.Editions.AutoSize = true;
            this.Editions.Location = new System.Drawing.Point(6, 45);
            this.Editions.Name = "Editions";
            this.Editions.Size = new System.Drawing.Size(74, 13);
            this.Editions.TabIndex = 0;
            this.Editions.TabStop = true;
            this.Editions.Text = "Wykaz wydań";
            this.Editions.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Authors
            // 
            this.Authors.AutoSize = true;
            this.Authors.Location = new System.Drawing.Point(6, 67);
            this.Authors.Name = "Authors";
            this.Authors.Size = new System.Drawing.Size(81, 13);
            this.Authors.TabIndex = 0;
            this.Authors.TabStop = true;
            this.Authors.Text = "Wykaz autorów";
            this.Authors.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Titles
            // 
            this.Titles.AutoSize = true;
            this.Titles.Location = new System.Drawing.Point(6, 26);
            this.Titles.Name = "Titles";
            this.Titles.Size = new System.Drawing.Size(78, 13);
            this.Titles.TabIndex = 0;
            this.Titles.TabStop = true;
            this.Titles.Text = "Wykaz tytułów";
            this.Titles.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ExportBorrows);
            this.groupBox6.Location = new System.Drawing.Point(359, 266);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(246, 113);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Pozostałe";
            // 
            // ExportBorrows
            // 
            this.ExportBorrows.AutoSize = true;
            this.ExportBorrows.Location = new System.Drawing.Point(7, 20);
            this.ExportBorrows.Name = "ExportBorrows";
            this.ExportBorrows.Size = new System.Drawing.Size(193, 13);
            this.ExportBorrows.TabIndex = 0;
            this.ExportBorrows.TabStop = true;
            this.ExportBorrows.Text = "Eksportuj dane aktywnych wypożyczeń";
            this.ExportBorrows.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // SummaryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox2);
            this.Name = "SummaryForm";
            this.Size = new System.Drawing.Size(645, 395);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.LinkLabel NewLibrarian;
        private System.Windows.Forms.LinkLabel Librarians;
        private System.Windows.Forms.LinkLabel RegisterReader;
        private System.Windows.Forms.LinkLabel Readers;
        private System.Windows.Forms.LinkLabel ReturnBook;
        private System.Windows.Forms.LinkLabel BorrowBook;
        private System.Windows.Forms.LinkLabel NewEdition;
        private System.Windows.Forms.LinkLabel Titles;
        private System.Windows.Forms.LinkLabel Publishers;
        private System.Windows.Forms.LinkLabel Editions;
        private System.Windows.Forms.LinkLabel AddBooks;
        private System.Windows.Forms.LinkLabel Authors;
        private System.Windows.Forms.LinkLabel BooksByPublisher;
        private System.Windows.Forms.LinkLabel TitleEditionsLastBorrow;
        private System.Windows.Forms.LinkLabel ReadersSummary;
        private System.Windows.Forms.LinkLabel BorrowsSummary;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.LinkLabel ExportBorrows;
    }
}
