﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class ProlongBorrowForm : UserControl
    {
        public ProlongBorrowForm()
        {
            InitializeComponent();
            ClearForms();

            /* wpisanie listy bibliotekarzy */
            DataClassesDataContext db = new DataClassesDataContext();
            var librarians = db.Bibliotekarzs.ToList();
            foreach (Bibliotekarz lib in librarians)
                this.cbLibrarian.Items.Add(String.Format("[{0}] {1} {2}",
                    lib.IdBibliotekarza, lib.Imie, lib.Nazwisko));
        }

        int ReaderId;
        int LibrarianId;
        int BorrowId;
        int Days;


        /// <summary>
        /// Próbuj przedłużyć wybrane wypożyczenie. LINQ to SQL.
        /// Wykorzystanie procedury składowanej: PrzedluzWypoczenie.
        /// Wykorzystanie triggera: odrzuc_przedluzenie.
        /// </summary>
        private bool ProlongBorrow()
        {
            // zakładamy że dane zostały sparsowane i pola zostały ustawione

            // spróbuj przedłużyć wypożyczenie
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                try
                {
                    db.PrzedluzWypozyczenie(BorrowId, LibrarianId, Days);
                }
                catch
                {
                    Utils.ShowExclamationMsgBox(
                        "Nie udało się przedłużyć wypożyczenia.\n\n" +
                        "Prawdopodobnie przekroczono termin zwrotu lub " +
                        "sumaryczna liczba dni wypożyczenia przekroczyłaby 100 dni.");
                    return false;
                }
            }

            Utils.ShowInfoMsgBox("Dokonano przedłużenia wybranej książki.");
            return true;
        }


        /* reakcja na zdarzenia */

        private void btnProlongBorrow_Click(object sender, EventArgs e)
        {
            int librarianId;
            if (!ReadSelectedLibrarianId(out librarianId))
                return;
            LibrarianId = librarianId;

            int borrowId;
            if (!ReadSelectedBorrowId(out borrowId))
                return;
            BorrowId = borrowId;

            if (cbDays.SelectedIndex < 0)
            {
                Utils.ShowExclamationMsgBox("Nie wybrano liczby dni przedłużenia.");
                return;
            }
            Days = Int32.Parse(cbDays.SelectedItem.ToString());

            if (ProlongBorrow())
                ClearForms();
        }

        /* parsowanie */

        private bool ReadSelectedLibrarianId(out int id)
        {
            id = 0;
            if (this.cbLibrarian.SelectedIndex < 0)
            {
                Utils.ShowExclamationMsgBox("Nie wybrano bibliotekarza.");
                return false;
            }

            String libItem = this.cbLibrarian.SelectedItem.ToString();
            if (!Int32.TryParse(libItem.Substring(1, libItem.IndexOf(']') - 1), out id))
            {
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id bibliotekarza.");
                return false;
            }

            return true;
        }

        private bool ReadSelectedBorrowId(out int id)
        {
            id = 0;

            if (listBorrowedBooks.SelectedIndex < 0)
            {
                Utils.ShowExclamationMsgBox("Nie wybrano książki z listy.");
                return false;
            }

            String borrowItem = this.listBorrowedBooks.SelectedItem.ToString();
            if (!Int32.TryParse(borrowItem.Substring(1, borrowItem.IndexOf(']') - 1), out id))
            {
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id wypożyczenia.");
                return false;
            }

            return true;
        }

        private void tbReaderId_Leave(object sender, EventArgs e)
        {
            int parsedId;
            if (!Int32.TryParse(tbReaderId.Text, out parsedId))
            {
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id czytelnika.");
                ReaderId = 0;
                this.tbReaderId.Text = string.Empty;
            }
            else
            {
                ReaderId = parsedId;

                DataClassesDataContext db = new DataClassesDataContext();

                var readerInfo = db.Czytelniks
                            .Where(r => r.IdCzytelnika == ReaderId)
                            .FirstOrDefault();

                if (readerInfo == null)
                {
                    Utils.ShowExclamationMsgBox("Nie ma zarejestrowanego czytelnika o podanym id = " + ReaderId);
                    this.tbReaderId.Text = String.Empty;
                    ReaderId = 0;
                    return;
                }

                this.labReaderInfo.Text = String.Format("{0} {1}{2}",
                    readerInfo.Imie, readerInfo.Nazwisko,
                    (readerInfo.Miasto != null && readerInfo.Miasto.Length > 0 ?
                        ", " + readerInfo.Miasto : ""));


                /* wczytanie aktywnych wypozyczen tego czytelnika */
                var borrowedBooks = (from w in db.Wypozyczenies
                                     where w.IdCzytelnika == ReaderId && w.DataZwrotu == null
                                     select new
                                     {
                                         BorrowId = w.IdWypozyczenia,
                                         Title = w.Egzemplarz.Wydanie.Tytul.NazwaTytulu,
                                         ISBN = w.Egzemplarz.Wydanie.ISBN
                                     }).ToArray();

                listBorrowedBooks.Items.Clear();
                for (int b = 0; b < borrowedBooks.Length; b++)
                {
                    var borrow = borrowedBooks[b];
                    listBorrowedBooks.Items.Add(
                        String.Format("[{0}] {1} (ISBN: {2})",
                            borrow.BorrowId, borrow.Title, borrow.ISBN));
                }
            }
        }

        /* pozostałe */

        private void ClearForms()
        {
            this.tbReaderId.Text = String.Empty;
            this.cbLibrarian.SelectedIndex = -1;
            this.labReaderInfo.Text = String.Empty;
            this.listBorrowedBooks.Items.Clear();

            ReaderId = 0;
            LibrarianId = 0;
            BorrowId = 0;
        }
    }
}
