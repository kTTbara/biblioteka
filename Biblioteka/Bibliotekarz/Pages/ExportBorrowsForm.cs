﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class ExportBorrowsForm : UserControl
    {
        public ExportBorrowsForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Pobranie tabeli z informacjami o wypożyczeniach danego czytelnika
        /// za pomocą procedury składowanej wywołanej poprzez LINQ.
        /// </summary>
        private void labGenerate_Click(object sender, EventArgs e)
        {
            bool flaga = false;
            tbResult.Text = "";
            IEnumerable<AktywneWypozyczeniaResult> listaCzytZAktWyp;

            DataClassesDataContext db = new DataClassesDataContext();
            // pobranie danych o czytelnikach
            var result = (from r in db.Czytelniks
                          where r.Wypozyczenies.Where(w => w.DataZwrotu == null).Any()
                          orderby r.Nazwisko
                          select new { r.IdCzytelnika, r.Imie, r.Nazwisko, r.Miasto });

            // iteracja po pobranym zbiorze czytelników
            foreach (var temp in result)
            {
                listaCzytZAktWyp = db.AktywneWypozyczenia(temp.IdCzytelnika);

                // iteracja po każdym aktywnym wypożyczeniu
                foreach (var value in listaCzytZAktWyp)
                {
                    // wypisz dane czytelnika, jeśli nie zostały jeszcze wypisane
                    if (flaga == false)
                    {
                        tbResult.AppendText("______________________________________________________________" + Environment.NewLine + Environment.NewLine);
                        tbResult.AppendText(temp.Nazwisko + " " + temp.Imie + ", " + temp.Miasto + Environment.NewLine + Environment.NewLine);
                        flaga = true;
                    }

                    tbResult.AppendText(
                            "\t Tytuł: " + value.Tytuł + ", ISBN: " + value.ISBN + Environment.NewLine
                        + "\t Data wypożyczenia: " + value.DataWypożyczenia.ToLongDateString() + Environment.NewLine
                        + "\t Wypożyczono na " + value.Długość + " dni." + Environment.NewLine + Environment.NewLine);
                }
                flaga = false;
            }
        }


        private void btnSaveToFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Plik tekstowy (*.txt)|*.txt|Dowolny plik|*.*";
            if (dlg.ShowDialog() != DialogResult.OK)
                return;

            System.IO.File.WriteAllText(dlg.FileName, tbResult.Text);
        }
    }
}
