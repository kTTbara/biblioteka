﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class BooksByPublisherForm : UserControl
    {
        public BooksByPublisherForm()
        {
            InitializeComponent();
            LoadData();
        }

        /// <summary>
        /// Zestawienie wydawnictw wg wydanych tytułów, przedstawiające też
        /// liczby wydań i egzemplarzy książek dostępnych w bibliotece
        /// w LINQ to SQL.
        /// </summary>
        private void LoadData()
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query =
                (from wydawca in db.Wydawcas
                 select new
                 {
                     wydawca.IdWydawcy,
                     wydawca.Nazwa,
                     wydawca.Kraj,
                     wydawca.Miasto,
                     WydanychTytulow = (
                         from w in db.Wydanies
                         where w.IdWydawcy == wydawca.IdWydawcy
                         select w.IdTytulu
                         ).Distinct().Count(),
                     WszystkichWydan = wydawca.Wydanies.Count(),
                     WszystkichEgzemplarzy = (
                         from e in db.Egzemplarzs
                         where e.Wydanie.IdWydawcy == wydawca.IdWydawcy
                         select e
                         ).Count()
                 })
                .OrderByDescending(w => w.WydanychTytulow);

            dataGrid.DataSource = query;
        }


        private void btnSearch_Click(object sender, EventArgs ev)
        {
            LoadData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (Control ctrl in this.Controls)
                if (ctrl.GetType() == typeof(TextBox))
                    ((TextBox)ctrl).Text = String.Empty;
        }
    }
}
