﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class NewEditionForm : UserControl
    {
        public NewEditionForm()
        {
            InitializeComponent();
            Authors = new List<int>();
        }

        private int TitleId;
        private List<int> Authors;
        private int PublisherId;


        public void ClearData()
        {
            TitleId = 0;
            Authors.Clear();
            PublisherId = 0;

            tbISBN.Text = String.Empty;
            tbYear.Text = String.Empty;
            tbAddedBooks.Text = String.Empty;
            labAuthors.Text = String.Empty;
            labPublisher.Text = String.Empty;
            labTitle.Text = String.Empty;
        }


        /// <summary>
        /// Dodanie wydania tytułu do bazy oraz ewentualnie pewnej
        /// liczby egzemplarzy tego wydania za pomocą LINQ to SQL.
        /// </summary>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            String ISBN = tbISBN.Text;
            short year = 0, books = 0;

            if (ISBN.Length == 0)
                Utils.ShowExclamationMsgBox("Wprowadź ISBN.");

            else if ((ISBN.Length != 13 && ISBN.Length != 10) || ISBN.Where(c => !Char.IsDigit(c)).Count() > 0)
                Utils.ShowExclamationMsgBox("Numer ISBN musi składać się z 10 lub 13 cyfr.");

            else if (tbYear.Text.Length > 0 && (Int16.TryParse(tbYear.Text, out year) == false || year < 0 || year > 3000))
                Utils.ShowExclamationMsgBox("Wprowadzono niepoprawny rok wydania.");

            else if (tbAddedBooks.Text.Length > 0 && (Int16.TryParse(tbAddedBooks.Text, out books) == false || books < 0))
                Utils.ShowExclamationMsgBox("Wprowadzono niepoprawną ilość egzemplarzy do dodania.");

            else if (TitleId == 0 || PublisherId == 0 || Authors.Count == 0)
                Utils.ShowExclamationMsgBox("Wybierz tytuł, wydawcę i przynajmniej jednego autora.");

            else
            {
                DataClassesDataContext db = new DataClassesDataContext();
                Wydanie wyd = new Wydanie();
                wyd.IdTytulu = TitleId;
                wyd.IdWydawcy = PublisherId;
                wyd.ISBN = ISBN;
                if (year > 0) wyd.Rok = year;
                db.Wydanies.InsertOnSubmit(wyd);
                db.SubmitChanges();

                foreach (int authorId in Authors)
                {
                    Autorstwo autorstwo = new Autorstwo();
                    autorstwo.IdAutora = authorId;
                    autorstwo.IdWydania = wyd.IdWydania;
                    db.Autorstwos.InsertOnSubmit(autorstwo);
                }
                db.SubmitChanges();
                MessageBox.Show("Dodano wydanie do bazy.");

                /* Procedura składowana do dodawania egzemplarzy danego wydania */
                // LINQ to SQL: procedura składowana
                if (books > 0)
                {
                    short? addedBooks = 0;
                    db.DodajEgzemplarze(wyd.IdWydania, books, ref addedBooks);
                    if (addedBooks < 1)
                        Utils.ShowExclamationMsgBox(
                            "Nie udało się dodać egzemplarzy tego wydania.");
                    else
                        MessageBox.Show(String.Format(
                            "Dodano {0} z {1} egzemplarzy tego wydania do bazy.",
                            addedBooks, books));
                }
                ClearData();
            }
        }

        private void btnSelectTitle_Click(object sender, EventArgs e)
        {
            WindowWithContent dlg = new WindowWithContent(new SelectTitleForm());
            dlg.Text = "Wybierz tytuł";
            if (dlg.ShowDialog() != DialogResult.OK)
                return;
            SelectTitleForm form = (SelectTitleForm)dlg.Content;
            if (form.SelectedTitleId < 1)
                Utils.ShowExclamationMsgBox("Nie wybrano tytułu");
            else
            {
                TitleId = form.SelectedTitleId;
                labTitle.Text = (String)form.SelectedRow.Cells[1].Value;
            }
        }

        private void btnSelectAuthor_Click(object sender, EventArgs e)
        {
            WindowWithContent dlg = new WindowWithContent(new SelectAuthorForm());
            dlg.Text = "Wybierz autora";
            if (dlg.ShowDialog() != DialogResult.OK)
                return;
            int authorId = ((SelectAuthorForm)dlg.Content).SelectedAuthorId;
            if (Authors.IndexOf(authorId) >= 0)
            {
                Utils.ShowExclamationMsgBox("Ten autor został już wcześniej dodany!");
                return;
            }

            DataClassesDataContext db = new DataClassesDataContext();
            Autor author = (from a in db.Autors where a.IdAutora == authorId select a).SingleOrDefault();
            String authorName;
            if (author.Imie != null)
                authorName = author.Imie + " " + author.Nazwisko;
            else
                authorName = author.Nazwisko;

            Authors.Add(authorId);
            labAuthors.Text = labAuthors.Text + "\n" + authorName;
        }

        private void btnSelectPublisher_Click(object sender, EventArgs e)
        {
            WindowWithContent dlg = new WindowWithContent(new SelectPublisherForm());
            dlg.Text = "Wybierz wydawcę";
            if (dlg.ShowDialog() != DialogResult.OK)
                return;
            PublisherId = ((SelectPublisherForm)dlg.Content).SelectedPublisherId;
            labPublisher.Text = (String) ((SelectPublisherForm)dlg.Content).SelectedRow.Cells[1].Value;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
    }
}
