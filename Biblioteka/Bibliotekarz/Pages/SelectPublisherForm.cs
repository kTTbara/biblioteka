﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Bibliotekarz
{
    public partial class SelectPublisherForm : UserControl
    {
        public SelectPublisherForm()
        {
            InitializeComponent();
            SelectedPublisherId = -1;

            DataClassesDataContext db = new DataClassesDataContext();
            dataGridPublishers.DataSource = db.Wydawcas;
        }

        public DataGridViewRow SelectedRow { get; protected set; }
        public int SelectedPublisherId { get; protected set; }


        /// <summary> Znajdź wydawcę przy użyciu LINQ to SQL. </summary>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            // sprarametryzowane zapytanie LINQ
            // String.Contains() zadziała jak LIKE w SQL
            DataClassesDataContext db = new DataClassesDataContext();
            var query = db.Wydawcas.AsQueryable();
            if (tbName.Text != String.Empty)
                query = query.Where(w => w.Nazwa.Contains(tbName.Text));
            if (tbCountry.Text != String.Empty)
                query = query.Where(w => w.Kraj.Contains(tbCountry.Text));
            if (tbCity.Text.Length > 0)
                query = query.Where(w => w.Miasto.Contains(tbCity.Text));
            if (tbStreet.Text.Length > 0)
                query = query.Where(w => w.Ulica.Contains(tbStreet.Text));
            if (tbEstate.Text.Length > 0)
                query = query.Where(w => w.NrPosesji.Contains(tbEstate.Text));
            if (tbPostCode.Text.Length > 0)
                query = query.Where(w => w.KodPocztowy.Contains(tbPostCode.Text));
            if (tbEmail.Text.Length > 0)
                query = query.Where(w => w.Email.Contains(tbEmail.Text));
            if (tbPhone.Text.Length > 0)
                query = query.Where(w => w.NrTelefonu.Contains(tbPhone.Text));

            dataGridPublishers.DataSource = query;

        }

        /// <summary>  Dodaj wydawcę do bazy za pomocą ADO.NET. </summary>
        private void btnAddPublisher_Click(object sender, EventArgs e)
        {
            if (tbName.Text.Length == 0)
                MessageBox.Show("Wpisz przynajmniej nazwę wydawcy.");

            else
            {
                try
                {
                    efWydawca newPublisher = new efWydawca();
                    newPublisher.Nazwa = tbName.Text;
                    if (tbCountry.Text.Length > 0) newPublisher.Kraj = tbCountry.Text;
                    if (tbCity.Text.Length > 0) newPublisher .Miasto= tbCity.Text;
                    if (tbStreet.Text.Length > 0) newPublisher.Ulica = tbStreet.Text;
                    if (tbEstate.Text.Length > 0) newPublisher.NrPosesji = tbEstate.Text;
                    if (tbPostCode.Text.Length > 0) newPublisher.KodPocztowy = tbPostCode.Text;
                    if (tbEmail.Text.Length > 0) newPublisher.Email = tbEmail.Text;
                    if (tbPhone.Text.Length > 0) newPublisher.NrTelefonu = tbPhone.Text;

                    using (BiblEntities entities = new BiblEntities())
                    {
                        entities.Wydawca.Add(newPublisher);
                        entities.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    Utils.ShowExceptionMsgBox(ex);
                    return;
                }

                MessageBox.Show("Dodano wydawcę do bazy.");

                foreach (Control ctrl in this.Controls)
                    if (ctrl.GetType() == typeof(TextBox))
                        ((TextBox)ctrl).Text = String.Empty;
            }
        }

        private void dataGridPublishers_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridPublishers.SelectedRows.Count > 0)
            {
                SelectedRow = dataGridPublishers.SelectedRows[0];
                SelectedPublisherId = (int)SelectedRow.Cells[0].Value;
            }
            else
            {
                SelectedRow = null;
                SelectedPublisherId = -1;
            }
        }
    }
}
