﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{   
    public partial class BorrowsSummaryForm : UserControl
    {
        public int liczbaWyp;
        public BorrowsSummaryForm()
        {
            InitializeComponent();
        }

        public void refreshData()
        {
            pokazLiczbeIDlugosciWypozyczen();
            pokazWypozyczeniaWgMiesiecy();
            pokazIlePrzedluzenProcentowo();

        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            refreshData();
        }

        public void pokazLiczbeIDlugosciWypozyczen()
        {
            /************ liczba wszystkich wypożyczeń i ich średnia długość *************/

            DataClassesDataContext db = new DataClassesDataContext();
            var queryBorDays = from w in db.Wypozyczenies
                               select new
                               {
                                   w.LiczbaDni,
                                   w.DataWypozyczenia,
                                   w.DataZwrotu
                               };
            float sumaDni = 0;
            TimeSpan? faktycznaSumaDni = TimeSpan.Zero;
            liczbaWyp = queryBorDays.Count();
            int liczbaZwroconychWyp = db.Wypozyczenies.Where(w => w.DataZwrotu != null).Count();

            foreach (var result in queryBorDays)
            {
                sumaDni += result.LiczbaDni;

                if (result.DataZwrotu.HasValue)
                {
                    faktycznaSumaDni += result.DataZwrotu - result.DataWypozyczenia ;
                }
            }


            float sredniaLiczDni = sumaDni / liczbaWyp;
            double sredniaFaktycznaLiczDni = faktycznaSumaDni.Value.TotalDays / liczbaZwroconychWyp;
            

            labelAllBor.Text = "Wszystkich wypożyczeń: " + liczbaWyp;                       //liczba wszystkich wypożyczeń
            label1_aveBorTime.Text = String.Format("{0:N2}", sredniaLiczDni) + " dni(a)";  //średnia długość wypożyczeń
            label1_realAveBorTime.Text = String.Format("{0:N2}", sredniaFaktycznaLiczDni) + " dni(a)"; //średnia rzeczywista długość wypożyczeń

        }

        public void pokazWypozyczeniaWgMiesiecy()
        {
            int[] liczbaWypWMc = new int[12];
            DataClassesDataContext db_months = new DataClassesDataContext();
            var queryMonths = from wy in db_months.Wypozyczenies
                              orderby wy.DataWypozyczenia ascending
                               select new
                               {
                                   wy.DataWypozyczenia
                               }; 

            foreach (var resultM in queryMonths)
            {
                liczbaWypWMc[resultM.DataWypozyczenia.Month-1] += 1;
            }
            for (int i = 0; i < 12; i++)
            {
                chart1.Series["test"].Points.AddXY(i+1, liczbaWypWMc[i]);
            }

            chart1.Visible = true;
                   
        }

        public void pokazIlePrzedluzenProcentowo()
        {
            DataClassesDataContext db_przedl = new DataClassesDataContext();
            int wszystkichPrzedl = (from prz in db_przedl.Przedluzenies
                                         select prz.IdWypozyczenia).Distinct().Count();

            float wszystkichWyp = liczbaWyp;
            {
                chartProcPrzedl.Series[0].Points.AddY(wszystkichPrzedl);
                chartProcPrzedl.Series[0].Points.AddY(wszystkichWyp - wszystkichPrzedl);
            }
            labelProcPrzedl.Text = String.Format("{0:N2}",(wszystkichPrzedl / wszystkichWyp * 100)) + " %";

            chartProcPrzedl.Visible = true;
        }
            

    }
}
