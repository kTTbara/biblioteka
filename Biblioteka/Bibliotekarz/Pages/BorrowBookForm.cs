﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class BorrowBookForm : UserControl
    {
        public BorrowBookForm()
        {
            InitializeComponent();
            ClearForms();
            InitData();
        }

        public int EditionId { get; protected set; }
        public int ReaderId { get; protected set; }
        public int LibrarianId { get; protected set; }
        public int SelectedReservationId { get; protected set; }


        private void InitData()
        {
            cbBorrowLength.SelectedIndex = 1;

            using (BiblEntities entities = new BiblEntities())
            {
                var librarians = entities.Bibliotekarz.Select(l => l);
                foreach (var lib in librarians)
                    cbLibrarian.Items.Add(
                        String.Format("[{0}] {1} {2}",
                            lib.IdBibliotekarza, lib.Imie, lib.Nazwisko));
            }
        }

        private void btnSelectEdition_Click(object sender, EventArgs e)
        {
            WindowWithContent dlg = new WindowWithContent(new SelectEditionForm());
            dlg.Text = "Wybierz wydanie";
            if (dlg.ShowDialog() != DialogResult.OK)
                return;
            SelectEditionForm form = (SelectEditionForm)dlg.Content;
            if (form.SelectedEditionId < 1)
                Utils.ShowExclamationMsgBox("Nie wybrano książki");
            else
            {
                RefreshEditionAndBooksList(form.SelectedEditionId);
                SelectedReservationId = 0;
            }
        }

        /// <summary>
        /// Wypozyczenie książki przy uzyciu ADO.NET.
        /// </summary>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            /* parsowanie i sprawdzenie poprawności danych */
            int librarianId;

            if (cbLibrarian.SelectedIndex < 0)
            {
                Utils.ShowExclamationMsgBox("Nie wybrano bibliotekarza który wypożycza ksiązkę.");
                return;
            }

            String selectedLibrarianItem = cbLibrarian.SelectedItem.ToString();
            if (!Int32.TryParse(
                selectedLibrarianItem.Substring(1, selectedLibrarianItem.IndexOf(']')-1),
                out librarianId))
            {
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id bibliotekarza.");
                return;
            }
            LibrarianId = librarianId;

            if (listBooks.SelectedIndex < 0)
            {
                Utils.ShowExclamationMsgBox("Nie wybrano egzemplarza książki.");
                return;
            }
            int bookId;
            if (!Int32.TryParse(listBooks.SelectedItem.ToString(), out bookId))
            {
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id egzemplarza książki.");
                return;
            }

            if (cbBorrowLength.SelectedIndex < 0)
                Utils.ShowExclamationMsgBox("Nie wybrano liczby dni wypożyczenia.");

            /* przeprowadzenie wypozyczenia */
            else
            {
                try
                {
                    using (BiblEntities entities = new BiblEntities())
                    {
                        efWypozyczenie wyp = new efWypozyczenie();
                        wyp.IdCzytelnika = ReaderId;
                        wyp.IdEgzemplarza = bookId;
                        wyp.IdBibliotekarza = librarianId;
                        wyp.LiczbaDni = Int32.Parse(cbBorrowLength.SelectedItem.ToString());
                        wyp.DataWypozyczenia = DateTime.Now;

                        // jesli wypozyczono z rezerwacji -> rezerwacja jest usuwana (przez trigger)
                        // bo mamy odczytac stan - tylko przez wyjatek w entites.SaveChanges? moze tu sie da odczytac?
                        entities.Wypozyczenie.Add(wyp);
                        // jeśli czytelnik ma zaległe wypożyczenia -> to wypożyczenie jest blokowane
                        // poprzez trigger blokada_wypozyczenia
                        bool transactionRejected = false;
                        try
                        {
                            entities.SaveChanges();
                        }
                        catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
                        {
                            transactionRejected = true;
                        }

                        if (transactionRejected)
                            Utils.ShowExclamationMsgBox(
                                "Nie udało się dokonać wypożyczenia. " +
                                "Prawdopodobnie zostało odrzucone, ponieważ " +
                                "czytelnik zwleka ze zwrotem pewnych książek.");
                        else
                            Utils.ShowInfoMsgBox("Dokonano wypożyczenia.");

                        ClearForms();
                    }
                }
                catch (Exception ex)
                {
                    Utils.ShowExceptionMsgBox(ex);
                }
            }
        }

        /// <summary>
        /// Wybranie zarezerwowanych ksiażek przez czytelnika przy użyciu ADO.NET / LINQ to Entities.
        /// </summary>
        private void tbReaderId_Leave(object sender, EventArgs e)
        {
            int readerId;

            if (!Int32.TryParse(tbReaderId.Text, out readerId) || readerId < 1)
                Utils.ShowExclamationMsgBox("Niepoprawny id czytelnika.");

            else
            {
                ReaderId = readerId;
                listReservedEditions.Items.Clear();

                try
                {
                    using (BiblEntities entities = new BiblEntities())
                    {
                        var readerInfo = entities.Czytelnik
                            .Where(r => r.IdCzytelnika == readerId)
                            .FirstOrDefault();

                        if (readerInfo == null)
                        {
                            Utils.ShowExclamationMsgBox("Nie ma zarejestrowanego czytelnika o podanym id = " + readerId);
                            this.tbReaderId.Text = String.Empty;
                            return;
                        }

                        this.labReaderInfo.Text = String.Format("{0} {1}{2}",
                            readerInfo.Imie, readerInfo.Nazwisko,
                            (readerInfo.Miasto != null && readerInfo.Miasto.Length > 0 ?
                                ", " + readerInfo.Miasto : ""));

                        var query = from rez in entities.Rezerwacja
                                    join wyd in entities.Wydanie
                                    on rez.IdWydania equals wyd.IdWydania
                                    where rez.IdCzytelnika == ReaderId
                                    select new
                                    {
                                        rez.IdRezerwacji,
                                        wyd.Tytul.NazwaTytulu,
                                        wyd.IdWydania,
                                        wyd.Rok
                                    };

                        foreach (var item in query)
                            listReservedEditions.Items.Add(
                                String.Format("[{0}] {1} ({2}, {3})",
                                    item.IdRezerwacji,
                                    item.NazwaTytulu, item.IdWydania, item.Rok)
                                );
                    }
                }
                catch (Exception ex)
                {
                    Utils.ShowExceptionMsgBox(ex);
                }
            }
        }

        private void listReservedEditions_SelectedIndexChanged(object sender, EventArgs e)
        {
            /* odczytanie id wydania z zaznaczonego elementu */
            int selectedId = listReservedEditions.SelectedIndex;
            if (selectedId < 0) return;     // nic nie jest zaznaczone

            int parsedEditionId;
            String selectedItem = listReservedEditions.SelectedItem.ToString();
            int start = selectedItem.LastIndexOf('(')+1;
            String editionStr = selectedItem.Substring(start, selectedItem.LastIndexOf(',') - start);
            if (!Int32.TryParse(editionStr, out parsedEditionId) || parsedEditionId< 1)
            {
                Utils.ShowExclamationMsgBox("Nie udało się odczytać id wydania.");
                EditionId = 0;
                return;
            }

            int reservationId;
            Int32.TryParse(selectedItem.Substring(1, selectedItem.IndexOf(']') - 1), out reservationId);
            SelectedReservationId = reservationId;

            RefreshEditionAndBooksList(parsedEditionId);
        }

        /// <summary>
        /// Wybierz wszystkie niewypożyczone egzemplarze tego wydania (LINQ to SQL)
        /// w stanie, w którym można wypożyczyć (nie muszą być reperowane).
        /// </summary>
        private void RefreshEditionAndBooksList(int editionId)
        {
            EditionId = editionId;

            try
            {
                // LINQ to SQL
                DataClassesDataContext db = new DataClassesDataContext();
                var edition = (from w in db.Wydanies
                               where w.IdWydania == EditionId
                               select new { NazwaTytulu = w.Tytul.NazwaTytulu, ISBN = w.ISBN, Rok = w.Rok })
                              .FirstOrDefault();

                labEdition.Text = String.Format("{0} ({1}, {2})",
                    edition.NazwaTytulu, edition.ISBN, edition.Rok);

                var books = from e in db.Egzemplarzs
                            where e.IdWydania == EditionId
                            && e.Stan > 1
                            && (e.Wypozyczenies.Any() == false ||
                                e.Wypozyczenies.OrderByDescending(eg => eg.IdWypozyczenia).FirstOrDefault().DataZwrotu != null)
                            select e.IdEgzemplarza;
                listBooks.DataSource = books.ToList();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMsgBox(ex);
            }
        }

        private void ClearForms()
        {
            tbReaderId.Text = String.Empty;
            cbBorrowLength.SelectedIndex = 1;
            listBooks.DataSource = null;
            listReservedEditions.Items.Clear();
            labEdition.Text = String.Empty;
            labReaderInfo.Text = String.Empty;

            this.LibrarianId = 0;
            this.ReaderId = 0;
            this.EditionId = 0;
            this.SelectedReservationId = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearForms();
        }
    }
}
