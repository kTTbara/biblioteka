﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bibliotekarz
{
    public partial class TitleEditionsLastBorrow : UserControl
    {
        public TitleEditionsLastBorrow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Zestawienie tytułów i liczby ich wydań oraz liczby egzemplarzy
        /// i liczby wypożyczeń wg ostatniego wypożyczenia egzemplarza danego
        /// tytułu. ADO.NET (LINQ to Entities)
        /// </summary>
        private void LoadData()
        {
            using (BiblEntities entites = new BiblEntities())
            {
                var query = (from t in entites.Tytul
                             select new
                             {
                                 IdTytulu = t.IdTytulu,
                                 Tytul = t.NazwaTytulu,
                                 Wydan = t.Wydanie.Count(),
                                 Egzemplarzy = (
                                     from e in entites.Egzemplarz
                                     where e.Wydanie.IdTytulu == t.IdTytulu
                                     select e).Count(),
                                 Wypozyczen = (
                                     from w in entites.Wypozyczenie
                                     where w.Egzemplarz.Wydanie.IdTytulu == t.IdTytulu
                                     select w).Count(),
                                 DataOstatniegoWypozyczenia = (
                                     from w in entites.Wypozyczenie
                                     where w.Egzemplarz.Wydanie.IdTytulu == t.IdTytulu
                                     select w)
                                         .OrderByDescending(w => w.DataWypozyczenia)
                                         .Select(w => w.DataWypozyczenia)
                                         .FirstOrDefault(),
                                 OstatniCzytelnik = (
                                     from cz in entites.Czytelnik
                                     where cz.IdCzytelnika == (
                                         from w in entites.Wypozyczenie
                                         where w.Egzemplarz.Wydanie.IdTytulu == t.IdTytulu
                                         select w)
                                             .OrderByDescending(w => w.DataWypozyczenia)
                                             .Select(w => w.IdCzytelnika)
                                             .FirstOrDefault()
                                     select new
                                     {
                                         OstCzyt = cz.Imie + " " + cz.Nazwisko
                                     }).Select(o => o.OstCzyt).FirstOrDefault()
                             })
                            .OrderByDescending(x => x.Wypozyczen);
                this.dataGridView1.DataSource = query.ToList();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
