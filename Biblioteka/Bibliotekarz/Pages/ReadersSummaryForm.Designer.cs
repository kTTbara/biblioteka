﻿namespace Bibliotekarz
{
    partial class ReadersSummaryForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labMostBorrows = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labMostProlongs = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labMostPenulties = new System.Windows.Forms.Label();
            this.chartCities = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labRegisteredReaders = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labOldestReaders = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.labYoungestReaders = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartCities)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labMostBorrows);
            this.groupBox1.Location = new System.Drawing.Point(583, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(308, 90);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Najwięcej wypożyczeń";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Honeydew;
            this.label1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(295, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Wypożyczeń | Imię i Nazwisko (Id czyt.), Miasto\r\n--------------------------------" +
    "----------------";
            // 
            // labMostBorrows
            // 
            this.labMostBorrows.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labMostBorrows.BackColor = System.Drawing.Color.Honeydew;
            this.labMostBorrows.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labMostBorrows.Location = new System.Drawing.Point(6, 42);
            this.labMostBorrows.Name = "labMostBorrows";
            this.labMostBorrows.Size = new System.Drawing.Size(295, 45);
            this.labMostBorrows.TabIndex = 1;
            this.labMostBorrows.Text = "1. ...\r\n2. ...\r\n3. ...";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(3, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(89, 23);
            this.btnRefresh.TabIndex = 1;
            this.btnRefresh.Text = "Odśwież dane";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.labMostProlongs);
            this.groupBox2.Location = new System.Drawing.Point(583, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(308, 90);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Najwięcej przedłużeń wypożyczeń";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Lavender;
            this.label2.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(295, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Przedłużeń | Imię i Nazwisko (Id czyt.), Miasto\r\n--------------------------------" +
    "----------------";
            // 
            // labMostProlongs
            // 
            this.labMostProlongs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labMostProlongs.BackColor = System.Drawing.Color.Lavender;
            this.labMostProlongs.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labMostProlongs.Location = new System.Drawing.Point(6, 42);
            this.labMostProlongs.Name = "labMostProlongs";
            this.labMostProlongs.Size = new System.Drawing.Size(295, 45);
            this.labMostProlongs.TabIndex = 1;
            this.labMostProlongs.Text = "1. ...\r\n2. ...\r\n3. ...";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.labMostPenulties);
            this.groupBox4.Location = new System.Drawing.Point(583, 204);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(308, 90);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Najwięcej nałożonych kar";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.MistyRose;
            this.label4.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(295, 26);
            this.label4.TabIndex = 1;
            this.label4.Text = "Kar | Imię i Nazwisko (Id czyt.), Miasto\r\n---------------------------------------" +
    "---------";
            // 
            // labMostPenulties
            // 
            this.labMostPenulties.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labMostPenulties.BackColor = System.Drawing.Color.MistyRose;
            this.labMostPenulties.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labMostPenulties.Location = new System.Drawing.Point(6, 42);
            this.labMostPenulties.Name = "labMostPenulties";
            this.labMostPenulties.Size = new System.Drawing.Size(295, 45);
            this.labMostPenulties.TabIndex = 1;
            this.labMostPenulties.Text = "1. ...\r\n2. ...\r\n3. ...";
            // 
            // chartCities
            // 
            chartArea1.Name = "ChartArea1";
            this.chartCities.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartCities.Legends.Add(legend1);
            this.chartCities.Location = new System.Drawing.Point(3, 44);
            this.chartCities.Name = "chartCities";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartCities.Series.Add(series1);
            this.chartCities.Size = new System.Drawing.Size(574, 455);
            this.chartCities.TabIndex = 2;
            this.chartCities.Text = "chart1";
            title1.Name = "Title1";
            title1.Text = "Miejsce zamieszkania czytelników";
            this.chartCities.Titles.Add(title1);
            this.chartCities.Visible = false;
            // 
            // labRegisteredReaders
            // 
            this.labRegisteredReaders.AutoSize = true;
            this.labRegisteredReaders.Location = new System.Drawing.Point(147, 12);
            this.labRegisteredReaders.Name = "labRegisteredReaders";
            this.labRegisteredReaders.Size = new System.Drawing.Size(162, 13);
            this.labRegisteredReaders.TabIndex = 3;
            this.labRegisteredReaders.Text = "Zarejestrowanych czytelników: X";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.labOldestReaders);
            this.groupBox3.Location = new System.Drawing.Point(584, 300);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(350, 90);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Najstarsi czytelnicy";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.PeachPuff;
            this.label3.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(337, 26);
            this.label3.TabIndex = 1;
            this.label3.Text = "Data urodz. (Wiek) | Imię i Nazwisko (Id czyt.), Miasto\r\n------------------------" +
    "-------------------------------";
            // 
            // labOldestReaders
            // 
            this.labOldestReaders.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labOldestReaders.BackColor = System.Drawing.Color.PeachPuff;
            this.labOldestReaders.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labOldestReaders.Location = new System.Drawing.Point(6, 42);
            this.labOldestReaders.Name = "labOldestReaders";
            this.labOldestReaders.Size = new System.Drawing.Size(337, 45);
            this.labOldestReaders.TabIndex = 1;
            this.labOldestReaders.Text = "1. ...\r\n2. ...\r\n3. ...";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.labYoungestReaders);
            this.groupBox5.Location = new System.Drawing.Point(584, 396);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(350, 90);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Najmłodsi czytelnicy";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.LemonChiffon;
            this.label6.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(337, 26);
            this.label6.TabIndex = 1;
            this.label6.Text = "Data urodz. (Wiek) | Imię i Nazwisko (Id czyt.), Miasto\r\n------------------------" +
    "-------------------------------";
            // 
            // labYoungestReaders
            // 
            this.labYoungestReaders.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labYoungestReaders.BackColor = System.Drawing.Color.LemonChiffon;
            this.labYoungestReaders.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labYoungestReaders.Location = new System.Drawing.Point(6, 42);
            this.labYoungestReaders.Name = "labYoungestReaders";
            this.labYoungestReaders.Size = new System.Drawing.Size(337, 45);
            this.labYoungestReaders.TabIndex = 1;
            this.labYoungestReaders.Text = "1. ...\r\n2. ...\r\n3. ...";
            // 
            // ReadersSummaryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labRegisteredReaders);
            this.Controls.Add(this.chartCities);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "ReadersSummaryForm";
            this.Size = new System.Drawing.Size(945, 502);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartCities)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label labMostBorrows;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labMostProlongs;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labMostPenulties;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartCities;
        private System.Windows.Forms.Label labRegisteredReaders;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labOldestReaders;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labYoungestReaders;
    }
}
