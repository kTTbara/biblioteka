﻿namespace Bibliotekarz
{
    partial class BorrowBookForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSelectEdition = new System.Windows.Forms.Button();
            this.labEdition = new System.Windows.Forms.Label();
            this.listBooks = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbReaderId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbLibrarian = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.listReservedEditions = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cbBorrowLength = new System.Windows.Forms.ComboBox();
            this.labReaderInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSelectEdition
            // 
            this.btnSelectEdition.Location = new System.Drawing.Point(31, 65);
            this.btnSelectEdition.Name = "btnSelectEdition";
            this.btnSelectEdition.Size = new System.Drawing.Size(117, 23);
            this.btnSelectEdition.TabIndex = 1;
            this.btnSelectEdition.Text = "Wybierz książkę";
            this.btnSelectEdition.UseVisualStyleBackColor = true;
            this.btnSelectEdition.Click += new System.EventHandler(this.btnSelectEdition_Click);
            // 
            // labEdition
            // 
            this.labEdition.AutoSize = true;
            this.labEdition.Location = new System.Drawing.Point(31, 91);
            this.labEdition.Name = "labEdition";
            this.labEdition.Size = new System.Drawing.Size(0, 13);
            this.labEdition.TabIndex = 1;
            // 
            // listBooks
            // 
            this.listBooks.FormattingEnabled = true;
            this.listBooks.Location = new System.Drawing.Point(31, 245);
            this.listBooks.Name = "listBooks";
            this.listBooks.Size = new System.Drawing.Size(154, 56);
            this.listBooks.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 226);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Dostępne egzemplarze (zaznacz jeden):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Id czytelnika:";
            // 
            // tbReaderId
            // 
            this.tbReaderId.Location = new System.Drawing.Point(106, 10);
            this.tbReaderId.Name = "tbReaderId";
            this.tbReaderId.Size = new System.Drawing.Size(156, 20);
            this.tbReaderId.TabIndex = 0;
            this.tbReaderId.Leave += new System.EventHandler(this.tbReaderId_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 328);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Bibliotekarz:";
            // 
            // cbLibrarian
            // 
            this.cbLibrarian.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLibrarian.FormattingEnabled = true;
            this.cbLibrarian.Location = new System.Drawing.Point(106, 325);
            this.cbLibrarian.Name = "cbLibrarian";
            this.cbLibrarian.Size = new System.Drawing.Size(156, 21);
            this.cbLibrarian.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 360);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Liczba dni wypożyczenia:";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(58, 426);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(155, 23);
            this.btnConfirm.TabIndex = 6;
            this.btnConfirm.Text = "Wypożycz";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "lub wybierz z zarezerwowanych:";
            // 
            // listReservedEditions
            // 
            this.listReservedEditions.FormattingEnabled = true;
            this.listReservedEditions.Location = new System.Drawing.Point(34, 147);
            this.listReservedEditions.Name = "listReservedEditions";
            this.listReservedEditions.Size = new System.Drawing.Size(274, 56);
            this.listReservedEditions.TabIndex = 2;
            this.listReservedEditions.SelectedIndexChanged += new System.EventHandler(this.listReservedEditions_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(185, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "[id_rezerwacji] Tytuł (id_wydania, rok)";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(233, 437);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Wyczyść";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbBorrowLength
            // 
            this.cbBorrowLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBorrowLength.FormattingEnabled = true;
            this.cbBorrowLength.Items.AddRange(new object[] {
            "14",
            "30",
            "60"});
            this.cbBorrowLength.Location = new System.Drawing.Point(164, 357);
            this.cbBorrowLength.Name = "cbBorrowLength";
            this.cbBorrowLength.Size = new System.Drawing.Size(98, 21);
            this.cbBorrowLength.TabIndex = 9;
            // 
            // labReaderInfo
            // 
            this.labReaderInfo.AutoSize = true;
            this.labReaderInfo.Location = new System.Drawing.Point(28, 36);
            this.labReaderInfo.Name = "labReaderInfo";
            this.labReaderInfo.Size = new System.Drawing.Size(175, 13);
            this.labReaderInfo.TabIndex = 4;
            this.labReaderInfo.Text = "<imię i nazwisko czytelnika, miasto>";
            // 
            // BorrowBookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbBorrowLength);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listReservedEditions);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbLibrarian);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbReaderId);
            this.Controls.Add(this.labReaderInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBooks);
            this.Controls.Add(this.labEdition);
            this.Controls.Add(this.btnSelectEdition);
            this.Name = "BorrowBookForm";
            this.Size = new System.Drawing.Size(324, 463);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSelectEdition;
        private System.Windows.Forms.Label labEdition;
        private System.Windows.Forms.ListBox listBooks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbReaderId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbLibrarian;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listReservedEditions;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbBorrowLength;
        private System.Windows.Forms.Label labReaderInfo;
    }
}
