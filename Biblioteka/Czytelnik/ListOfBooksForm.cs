﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Czytelnik
{
    public partial class ListOfBooksForm : UserControl
    {
        public ListOfBooksForm()
        {
            InitializeComponent();
            SelectedTitle = "";
            pokazZawartoscGrida();
        }

        int aktualnieZaznaczoneWydanie;
        public DataGridViewRow SelectedRow { get; protected set; }
        public String SelectedTitle { get; protected set; }

        private void pokazZawartoscGrida()
        {
            DataClassesBooksDataContext db = new DataClassesBooksDataContext();
            var query = from w in db.Wydanies
                        select new
                        {
                            Tytuł = w.Tytul.NazwaTytulu,
                            Wydawca = w.Wydawca.Nazwa,
                            ISBN = w.ISBN,
                            Rok = w.Rok,
                            // liczba dostępnych książek w bibliotece danego wydania =
                            // = liczba niewypożyczonych egzemplarzy danego wydania -
                            // - liczba zniszczonych egzemplarzy (odłożonych do naprawy) -
                            // - liczba zarezerwowanych
                            Dostepnych = (
                                w.Egzemplarzs
                                .Where(
                                    e => (e.Wypozyczenies.Any() == false
                                        || e.Wypozyczenies.OrderByDescending(e2 => e2.DataWypozyczenia).First().DataZwrotu != null)
                                    && e.Stan >= 1)
                                .Count()
                                -
                                w.Rezerwacjas.Count()),
                            LiczbaEgzemplarzy = w.Egzemplarzs.Count()
                        };

            if (textBoxTitle.Text.Length > 0)
                query = query.Where(w => w.Tytuł.Contains(textBoxTitle.Text));
            if (textBoxPublisher.Text.Length > 0)
                query = query.Where(w => w.Wydawca.Contains(textBoxPublisher.Text));

            dataGridBooks.DataSource = query;
        }

        private void RefreshSelectedBookInfo(int rowIndex)
        {
            if (rowIndex < 0) return;

            String ISBN = "";
            DataGridViewRow row = dataGridBooks.Rows[rowIndex];

            ISBN = row.Cells[2].Value.ToString();

            DataClassesBooksDataContext db = new DataClassesBooksDataContext();

            var q = (from w in db.Wydanies
                     where w.ISBN.Equals(ISBN)
                     select new
                     {
                         w.Tytul.NazwaTytulu,
                         NazwaWydawcy = w.Wydawca.Nazwa,
                         w.Wydawca.Miasto,
                         w.Rok,
                         w.Autorstwos,
                         w.ISBN,
                         w.Egzemplarzs,
                         w.IdWydania,

                     }).FirstOrDefault();

            aktualnieZaznaczoneWydanie = q.IdWydania;

            int wolnychEgzemplarzy = (int) dataGridBooks.Rows[rowIndex].Cells["Dostepnych"].Value;
            buttonRezerwuj.Enabled = (wolnychEgzemplarzy >= 0);


            StringBuilder doTextBoxa = new StringBuilder();
            doTextBoxa.AppendLine(q.NazwaTytulu);
            doTextBoxa.AppendLine();
            for (int i = 0; i < q.Autorstwos.Count(); i++)
            {
                doTextBoxa.Append(q.Autorstwos[i].Autor.Imie);
                doTextBoxa.Append(" ");
                doTextBoxa.AppendLine(q.Autorstwos[i].Autor.Nazwisko);
            }

            doTextBoxa.AppendLine("------------------------------");
            doTextBoxa.AppendLine("Wydanie: " + q.NazwaWydawcy);
            if (q.Miasto != null)
                doTextBoxa.Append(q.Miasto + " ");
            if (q.Rok.HasValue)
                doTextBoxa.Append(q.Rok.Value.ToString());
            doTextBoxa.AppendLine();
            doTextBoxa.AppendLine("ISBN: " + ISBN);
            doTextBoxa.AppendLine("Dostępnych książek: " + wolnychEgzemplarzy);

            textBox_bookDetails.Text = doTextBoxa.ToString();
        }

        private void buttonRezerwuj_Click(object sender, EventArgs e)
        {
            if (aktualnieZaznaczoneWydanie == 0)
                return;

            DataClassesBooksDataContext db = new DataClassesBooksDataContext();
            Rezerwacja rezerw = new Rezerwacja();

            // sprawdzenie czy wprowadzony identyfikator jest liczbą naturalną
            int readerId;
            if ((!Int32.TryParse(textBoxIdentyfikator.Text, out readerId)) || readerId < 1)
            {
                MessageBox.Show("Wprowadzony identyfikator nie jest poprawny");
                return;
            }

            // sprawdzenie czy id jest w bazie
            int queryCzytId = (from c in db.Czytelniks where c.IdCzytelnika.Equals(readerId) select c).Count();

            if (queryCzytId != 1)
            {
                MessageBox.Show("Czytelnik o podanym ID nie został zarejestrowany");
                return;
            }

            // dodanie rezerwcji do bazy
            rezerw.IdCzytelnika = readerId;
            rezerw.DataRezerwacji = DateTime.Now;
            rezerw.IdWydania = aktualnieZaznaczoneWydanie;

            db.Rezerwacjas.InsertOnSubmit(rezerw);
            db.SubmitChanges();

            MessageBox.Show("Rezerwacja przebiegła pomyślnie.");

            pokazZawartoscGrida();
        }

        private void buttonFiltr_Click(object sender, EventArgs e)
        {
            pokazZawartoscGrida();
        }

        private void dataGridBooks_SelectionChanged_1(object sender, EventArgs e)
        {
            if (this.dataGridBooks.SelectedRows.Count < 1)
                return;

            RefreshSelectedBookInfo(this.dataGridBooks.SelectedRows[0].Index);
        }

    }
}