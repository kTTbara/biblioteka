﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Czytelnik
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            AddPage("tabListOfBooks", new ListOfBooksForm());
            AddPage("tabReaderDetails", new ReaderDetailsForm());
            AddPage("tabBooksListXML", new BooksListXMLForm());
        }

        /// <summary>Dodaj formatkę jako zawartość wskazanej strony. </summary>
        private void AddPage(String pageName, Control pageContent)
        {
            this.tabCtrl.TabPages[pageName].Controls.Add(pageContent);
            pageContent.Location = new Point((this.Width - pageContent.Width) / 2, 0);
            pageContent.Anchor = AnchorStyles.Top;
        }
    }
}
