﻿namespace Czytelnik
{
    partial class ListOfBooksForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridBooks = new System.Windows.Forms.DataGridView();
            this.textBox_bookDetails = new System.Windows.Forms.TextBox();
            this.buttonRezerwuj = new System.Windows.Forms.Button();
            this.textBoxIdentyfikator = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonFiltr = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.textBoxPublisher = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBooks)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridBooks
            // 
            this.dataGridBooks.AllowUserToAddRows = false;
            this.dataGridBooks.AllowUserToDeleteRows = false;
            this.dataGridBooks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridBooks.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridBooks.Location = new System.Drawing.Point(3, 33);
            this.dataGridBooks.MultiSelect = false;
            this.dataGridBooks.Name = "dataGridBooks";
            this.dataGridBooks.ReadOnly = true;
            this.dataGridBooks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridBooks.Size = new System.Drawing.Size(491, 321);
            this.dataGridBooks.TabIndex = 0;
            this.dataGridBooks.SelectionChanged += new System.EventHandler(this.dataGridBooks_SelectionChanged_1);
            // 
            // textBox_bookDetails
            // 
            this.textBox_bookDetails.AcceptsReturn = true;
            this.textBox_bookDetails.BackColor = System.Drawing.Color.White;
            this.textBox_bookDetails.Location = new System.Drawing.Point(500, 33);
            this.textBox_bookDetails.Multiline = true;
            this.textBox_bookDetails.Name = "textBox_bookDetails";
            this.textBox_bookDetails.ReadOnly = true;
            this.textBox_bookDetails.Size = new System.Drawing.Size(168, 241);
            this.textBox_bookDetails.TabIndex = 2;
            // 
            // buttonRezerwuj
            // 
            this.buttonRezerwuj.Enabled = false;
            this.buttonRezerwuj.Location = new System.Drawing.Point(593, 305);
            this.buttonRezerwuj.Name = "buttonRezerwuj";
            this.buttonRezerwuj.Size = new System.Drawing.Size(75, 23);
            this.buttonRezerwuj.TabIndex = 5;
            this.buttonRezerwuj.Text = "Rezerwuj";
            this.buttonRezerwuj.UseVisualStyleBackColor = true;
            this.buttonRezerwuj.Click += new System.EventHandler(this.buttonRezerwuj_Click);
            // 
            // textBoxIdentyfikator
            // 
            this.textBoxIdentyfikator.Location = new System.Drawing.Point(546, 307);
            this.textBoxIdentyfikator.MaxLength = 10;
            this.textBoxIdentyfikator.Name = "textBoxIdentyfikator";
            this.textBoxIdentyfikator.Size = new System.Drawing.Size(41, 20);
            this.textBoxIdentyfikator.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(500, 289);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Podaj swój identyfikator i rezerwuj:";
            // 
            // buttonFiltr
            // 
            this.buttonFiltr.Location = new System.Drawing.Point(236, 357);
            this.buttonFiltr.Name = "buttonFiltr";
            this.buttonFiltr.Size = new System.Drawing.Size(65, 48);
            this.buttonFiltr.TabIndex = 3;
            this.buttonFiltr.Text = "Wyszukaj";
            this.buttonFiltr.UseVisualStyleBackColor = true;
            this.buttonFiltr.Click += new System.EventHandler(this.buttonFiltr_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 360);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Tytuł lub jego fragment:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 388);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Wydawnictwo:";
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(130, 357);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(100, 20);
            this.textBoxTitle.TabIndex = 1;
            // 
            // textBoxPublisher
            // 
            this.textBoxPublisher.Location = new System.Drawing.Point(130, 385);
            this.textBoxPublisher.Name = "textBoxPublisher";
            this.textBoxPublisher.Size = new System.Drawing.Size(100, 20);
            this.textBoxPublisher.TabIndex = 2;
            // 
            // ListOfBooksForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.textBoxPublisher);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonFiltr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxIdentyfikator);
            this.Controls.Add(this.buttonRezerwuj);
            this.Controls.Add(this.textBox_bookDetails);
            this.Controls.Add(this.dataGridBooks);
            this.Name = "ListOfBooksForm";
            this.Size = new System.Drawing.Size(681, 415);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBooks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridBooks;
        private System.Windows.Forms.TextBox textBox_bookDetails;
        private System.Windows.Forms.Button buttonRezerwuj;
        private System.Windows.Forms.TextBox textBoxIdentyfikator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonFiltr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.TextBox textBoxPublisher;
    }
}
