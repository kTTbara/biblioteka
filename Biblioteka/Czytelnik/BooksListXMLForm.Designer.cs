﻿namespace Czytelnik
{
    partial class BooksListXMLForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labFileName = new System.Windows.Forms.Label();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.btnSaveFile = new System.Windows.Forms.Button();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.dataGridBooks = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRemoveBook = new System.Windows.Forms.Button();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.tbISBN = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.tbYearFilter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbTitleFilter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSortByPublisher = new System.Windows.Forms.Button();
            this.btnSortByYear = new System.Windows.Forms.Button();
            this.btnSortByTitle = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBooks)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Otwarty plik:";
            // 
            // labFileName
            // 
            this.labFileName.AutoSize = true;
            this.labFileName.Location = new System.Drawing.Point(71, 16);
            this.labFileName.Name = "labFileName";
            this.labFileName.Size = new System.Drawing.Size(0, 13);
            this.labFileName.TabIndex = 1;
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(3, 44);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenFile.TabIndex = 0;
            this.btnOpenFile.Text = "Otwórz plik";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // btnSaveFile
            // 
            this.btnSaveFile.Location = new System.Drawing.Point(84, 44);
            this.btnSaveFile.Name = "btnSaveFile";
            this.btnSaveFile.Size = new System.Drawing.Size(75, 23);
            this.btnSaveFile.TabIndex = 1;
            this.btnSaveFile.Text = "Zapisz plik";
            this.btnSaveFile.UseVisualStyleBackColor = true;
            this.btnSaveFile.Click += new System.EventHandler(this.btnSaveFile_Click);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(165, 44);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(75, 23);
            this.btnSaveAs.TabIndex = 2;
            this.btnSaveAs.Text = "Zapisz jako";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // dataGridBooks
            // 
            this.dataGridBooks.AllowUserToAddRows = false;
            this.dataGridBooks.AllowUserToDeleteRows = false;
            this.dataGridBooks.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridBooks.Location = new System.Drawing.Point(3, 88);
            this.dataGridBooks.Name = "dataGridBooks";
            this.dataGridBooks.ReadOnly = true;
            this.dataGridBooks.RowHeadersVisible = false;
            this.dataGridBooks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridBooks.Size = new System.Drawing.Size(437, 397);
            this.dataGridBooks.TabIndex = 12;
            this.dataGridBooks.SelectionChanged += new System.EventHandler(this.dataGridBooks_SelectionChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRemoveBook);
            this.groupBox1.Controls.Add(this.btnAddBook);
            this.groupBox1.Controls.Add(this.tbISBN);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(447, 88);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(172, 139);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edycja listy";
            // 
            // btnRemoveBook
            // 
            this.btnRemoveBook.Location = new System.Drawing.Point(10, 109);
            this.btnRemoveBook.Name = "btnRemoveBook";
            this.btnRemoveBook.Size = new System.Drawing.Size(153, 23);
            this.btnRemoveBook.TabIndex = 5;
            this.btnRemoveBook.Text = "Usuń zaznaczony element";
            this.btnRemoveBook.UseVisualStyleBackColor = true;
            this.btnRemoveBook.Click += new System.EventHandler(this.btnRemoveBook_Click);
            // 
            // btnAddBook
            // 
            this.btnAddBook.Location = new System.Drawing.Point(10, 62);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(153, 23);
            this.btnAddBook.TabIndex = 4;
            this.btnAddBook.Text = "Dodaj ksiażkę do listy";
            this.btnAddBook.UseVisualStyleBackColor = true;
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // tbISBN
            // 
            this.tbISBN.Location = new System.Drawing.Point(10, 36);
            this.tbISBN.MaxLength = 13;
            this.tbISBN.Name = "tbISBN";
            this.tbISBN.Size = new System.Drawing.Size(153, 20);
            this.tbISBN.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Podaj ISBN książki do dodania:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnFilter);
            this.groupBox2.Controls.Add(this.tbYearFilter);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.tbTitleFilter);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(447, 242);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(172, 118);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtrowanie listy";
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(9, 88);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(153, 23);
            this.btnFilter.TabIndex = 8;
            this.btnFilter.Text = "FIltruj listę";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // tbYearFilter
            // 
            this.tbYearFilter.Location = new System.Drawing.Point(85, 62);
            this.tbYearFilter.MaxLength = 4;
            this.tbYearFilter.Name = "tbYearFilter";
            this.tbYearFilter.Size = new System.Drawing.Size(78, 20);
            this.tbYearFilter.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Rok wydania:";
            // 
            // tbTitleFilter
            // 
            this.tbTitleFilter.Location = new System.Drawing.Point(9, 37);
            this.tbTitleFilter.Name = "tbTitleFilter";
            this.tbTitleFilter.Size = new System.Drawing.Size(154, 20);
            this.tbTitleFilter.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tytuł zawiera:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSortByPublisher);
            this.groupBox3.Controls.Add(this.btnSortByYear);
            this.groupBox3.Controls.Add(this.btnSortByTitle);
            this.groupBox3.Location = new System.Drawing.Point(447, 376);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(172, 109);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sortowanie";
            // 
            // btnSortByPublisher
            // 
            this.btnSortByPublisher.Location = new System.Drawing.Point(7, 78);
            this.btnSortByPublisher.Name = "btnSortByPublisher";
            this.btnSortByPublisher.Size = new System.Drawing.Size(155, 23);
            this.btnSortByPublisher.TabIndex = 11;
            this.btnSortByPublisher.Text = "Sortuj wg wydawcy";
            this.btnSortByPublisher.UseVisualStyleBackColor = true;
            this.btnSortByPublisher.Click += new System.EventHandler(this.btnSortByPublisher_Click);
            // 
            // btnSortByYear
            // 
            this.btnSortByYear.Location = new System.Drawing.Point(7, 49);
            this.btnSortByYear.Name = "btnSortByYear";
            this.btnSortByYear.Size = new System.Drawing.Size(155, 23);
            this.btnSortByYear.TabIndex = 10;
            this.btnSortByYear.Text = "Sortuj wg roku wydania";
            this.btnSortByYear.UseVisualStyleBackColor = true;
            this.btnSortByYear.Click += new System.EventHandler(this.btnSortByYear_Click);
            // 
            // btnSortByTitle
            // 
            this.btnSortByTitle.Location = new System.Drawing.Point(7, 20);
            this.btnSortByTitle.Name = "btnSortByTitle";
            this.btnSortByTitle.Size = new System.Drawing.Size(155, 23);
            this.btnSortByTitle.TabIndex = 9;
            this.btnSortByTitle.Text = "Sortuj wg tytułu";
            this.btnSortByTitle.UseVisualStyleBackColor = true;
            this.btnSortByTitle.Click += new System.EventHandler(this.btnSortByTitle_Click);
            // 
            // BooksListXMLForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridBooks);
            this.Controls.Add(this.btnSaveAs);
            this.Controls.Add(this.btnSaveFile);
            this.Controls.Add(this.btnOpenFile);
            this.Controls.Add(this.labFileName);
            this.Controls.Add(this.label1);
            this.Name = "BooksListXMLForm";
            this.Size = new System.Drawing.Size(632, 488);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBooks)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labFileName;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Button btnSaveFile;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.DataGridView dataGridBooks;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRemoveBook;
        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.TextBox tbISBN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.TextBox tbYearFilter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbTitleFilter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSortByPublisher;
        private System.Windows.Forms.Button btnSortByYear;
        private System.Windows.Forms.Button btnSortByTitle;
    }
}
