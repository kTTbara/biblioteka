﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Czytelnik
{
    public partial class BooksListXMLForm : UserControl
    {
        public BooksListXMLForm()
        {
            InitializeComponent();
            Document = new XDocument(new XElement("Books"));
        }

        public String FileName
        {
            get { return this.labFileName.Text; }
            set { this.labFileName.Text = value; }
        }

        public XDocument Document { get; protected set; }

        public DataGridViewRow SelectedRow { get; protected set; }
        public enum SortOrderType { Default, Title, Year, Publisher };
        public SortOrderType SortOrder;

        protected String TitleFilter, YearFilter;


        /// <summary>
        /// Spróbuj wczytać listę książek do datagridu.
        /// Zwraca false w razie niepowodzenia.
        /// </summary>
        protected bool TryParse(XDocument document)
        {
            XDocument tmpDoc = Document;
            try
            {
                Document = document;
                RefreshDataGrid();
                Document = tmpDoc;
                return true;
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMsgBox(ex);
                Document = tmpDoc;
                return false;
            }
        }

        /// <summary>
        /// Odśwież zawartość tabeli poprzez wczytanie danych z pola Document.
        /// Filtrowanie oraz sortowanie zostanie przeprowadzone.
        /// </summary>
        public void RefreshDataGrid()
        {
            /* projekcja */
            var data = Document.Root.Descendants("Book")
                .Select(b => new
                {
                    Tytuł   = (String)b.Element("Title"),
                    ISBN    = (String)b.Element("ISBN"),
                    Rok     = (String)b.Element("Year"),
                    Wydawca = (String)b.Element("Publisher")
                });
            
            /* filtrowanie */
            if (TitleFilter != null && TitleFilter.Length > 0)
                data = data.Where(b => b.Tytuł.ToLower().Contains(TitleFilter.ToLower()));
            if (YearFilter != null && YearFilter.Length > 0)
                data = data.Where(b => b.Rok.Equals(YearFilter));
            
            /* sortowanie */
            switch (SortOrder)
            {
                case SortOrderType.Title:
                    data = data.OrderBy(b => b.Tytuł); break;
                case SortOrderType.Year:
                    data = data.OrderBy(b => b.Rok); break;
                case SortOrderType.Publisher:
                    data = data.OrderBy(b => b.Wydawca); break;
            }

            // przez ToList() tracimy możliwość sortowania klikająć w nagłowek tabeli,
            // ale inaczej nie działa (nic nie wyświetla bez tego)
            dataGridBooks.DataSource = data.ToList();
        }


        /* obsługa zdarzeń */

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Plik XML|*.xml|Dowolny plik|*.*";
            dlg.Title = "Wybierz plik z listą książek";
            if (dlg.ShowDialog() != DialogResult.OK)
                return;

            this.FileName = String.Empty;
            this.Document = null;
            this.SortOrder = SortOrderType.Default;
            this.tbISBN.Text = String.Empty;
            this.tbTitleFilter.Text = String.Empty;
            this.tbYearFilter.Text = String.Empty;

            XDocument doc = XDocument.Load(dlg.FileName);
            if (TryParse(doc) == false)
            {
                Utils.ShowExclamationMsgBox("Nie udało się otworzyć i sparsować pliku: " + dlg.FileName);
                return;
            }

            this.FileName = dlg.FileName;
            this.Document = doc;
        }

        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            if (this.FileName == null || this.FileName.Length == 0 || this.Document == null)
                SaveAs();
            else if (Save(FileName))
                Utils.ShowInfoMsgBox("Zapisano dokument.");
            else
                Utils.ShowErrorMsgBox("Nie udało się zapisać dokumentu do pliku:\n" + FileName);
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            SaveAs();
        }

        private void SaveAs()
        {
            if (Document == null)
            {
                Utils.ShowExclamationMsgBox("Nie sporządzono listy!");
                return;
            }
            if (Document.Root.Elements().Count() == 0)
            {
                Utils.ShowExclamationMsgBox("Nie można zapisać pustej listy.");
                return;
            }

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Plik XML|*.xml|Dowolny plik|*.*";
            dlg.Title = "Zapisz listę książek";
            if (dlg.ShowDialog() != DialogResult.OK)
                return;

            if (Save(dlg.FileName) == false)
                return;

            this.FileName = dlg.FileName;
        }

        private bool Save(String filename)
        {
            if (Document.Elements().Count() == 0)
            {
                Utils.ShowExclamationMsgBox("Nie można zapisać pustej listy.");
                return false;
            }

            try
            {
                Document.Save(filename);
                return true;
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMsgBox(ex);
                return false;
            }
        }



        private void btnAddBook_Click(object sender, EventArgs ev)
        {
            // doklejenie spacji na końcu, jeśli isbn krótszy od 13 znaków (może miec 9 znaków)
            String isbn = this.tbISBN.Text + new String(' ', 13 - tbISBN.Text.Length);
            
            // sprawdź czy występuje już taki na liście - nie dodajemy duplikatów
            var duplicates = from e in this.Document.Root.Elements("Book")
                             where (String)e.Element("ISBN") == isbn
                             select e;
            if (duplicates.Count() > 0)
            {
                Utils.ShowExclamationMsgBox("Lista zawiera już książkę o ISBN: " + isbn);
                return;
            }

            // pobierz potrzebne informacje o książce i dodaj do listy
            try
            {
                DataClassesBooksDataContext db = new DataClassesBooksDataContext();
                var bookInfo = (from b in db.Wydanies
                                where b.ISBN.Equals(isbn)
                               select new
                               {
                                   Tytuł = b.Tytul.NazwaTytulu,
                                   ISBN = b.ISBN,
                                   Rok = b.Rok,
                                   Wydawca = b.Wydawca.Nazwa
                               }).FirstOrDefault();
                if (bookInfo == null)
                {
                    Utils.ShowExclamationMsgBox("Nie znaleziono w bazie książki o ISBN = " + isbn);
                    return;
                }

                Document.Root.Add(
                        new XElement("Book",
                            new XElement("Title", bookInfo.Tytuł),
                            new XElement("ISBN", bookInfo.ISBN),
                            new XElement("Year", bookInfo.Rok),
                            new XElement("Publisher", bookInfo.Wydawca)));

                RefreshDataGrid();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMsgBox(ex);
                Utils.ShowExclamationMsgBox("Nie udało się dodać książki do listy.");
            }
        }


        /// <summary>
        /// Usuń wybraną książkę z listy.
        /// </summary>
        private void btnRemoveBook_Click(object sender, EventArgs ev)
        {
            if (SelectedRow == null)
            {
                Utils.ShowInfoMsgBox("Najpierw zaznacz książkę z listy.");
                return;
            }

            // znajdź element w otwartym dokumencie do usunięcia
            String isbn = SelectedRow.Cells["ISBN"].Value.ToString();
            var elem =  (from e in this.Document.Root.Elements("Book")
                        where (String)e.Element("ISBN") == isbn
                        select e)
                        .FirstOrDefault();
            if (elem == null)
            {
                Utils.ShowErrorMsgBox("Nie znaleziono zaznaczonej książki w dokumencie.");
                return;
            }

            // usuń element z otartego dokumentu
            elem.Remove();
            RefreshDataGrid();
        }


        /// <summary>
        /// Filtruj listę wg wybranych kryteriów.
        /// </summary>
        private void btnFilter_Click(object sender, EventArgs e)
        {
            TitleFilter = this.tbTitleFilter.Text;
            YearFilter = this.tbYearFilter.Text;
            RefreshDataGrid();
        }


        private void dataGridBooks_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dataGridBooks.SelectedRows.Count < 1)
                SelectedRow = null;
            else
                SelectedRow = this.dataGridBooks.SelectedRows[0];
        }


        /* sortowanie */

        private void btnSortByTitle_Click(object sender, EventArgs e)
        {
            this.SortOrder = SortOrderType.Title;
            RefreshDataGrid();
        }

        private void btnSortByYear_Click(object sender, EventArgs e)
        {
            this.SortOrder = SortOrderType.Year;
            RefreshDataGrid();
        }

        private void btnSortByPublisher_Click(object sender, EventArgs e)
        {
            this.SortOrder = SortOrderType.Publisher;
            RefreshDataGrid();
        }
    }
}
