﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Czytelnik
{
    public partial class ReaderDetailsForm : UserControl
    {
        public ReaderDetailsForm()
        {
            InitializeComponent();
            this.labReaderInfo.Text = String.Empty;
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            int idCzyt;
            if ((!Int32.TryParse(textBoxID.Text, out idCzyt)) || idCzyt < 1)
            {
                MessageBox.Show("Wprowadzony identyfikator nie jest poprawny");
                return;
            }


            DataClassesBooksDataContext db = new DataClassesBooksDataContext();

            // sprawdzenie czy id jest w bazie oraz, jesli tak, pobranie o nim informacji
            var readerInfo = (from c in db.Czytelniks where c.IdCzytelnika.Equals(idCzyt) select c).FirstOrDefault();

            if (readerInfo == null)
            {
                MessageBox.Show("Czytelnik o podanym ID nie został zarejestrowany");
                return;
            }


            var q = (from r in db.Czytelniks
                     where r.IdCzytelnika.Equals(idCzyt)
                     select new
                     {
                         AktywnychWypożyczeń = r.Wypozyczenies.Where(w => w.DataZwrotu == null).Count(),
                         WszystkichWypożyczeń = r.Wypozyczenies.Count(),
                         AktywnychRezerwacji = r.Rezerwacjas.Count(),
                         SumaNiezapłaconychKar = (Decimal?)r.Karas.Where(k => k.DataPlatnosci == null).Select(k => k.Kwota).Sum()
                     });

            dataGridReadersCzyt.DataSource = q;


            /* informacje o czytelniku */
            this.labReaderInfo.Text = String.Format("{0} {1}{2}",
                readerInfo.Imie, readerInfo.Nazwisko,
                (readerInfo.Miasto != null ? ", " + readerInfo.Miasto : ""));


            /* wszystkie wypozyczenia */
            var borrows = db.Wypozyczenies
                .Where(w => w.IdCzytelnika == idCzyt)
                .Select(w => new
                {
                    Tytuł = w.Egzemplarz.Wydanie.Tytul.NazwaTytulu,
                    ISBN = w.Egzemplarz.Wydanie.ISBN,
                    DataWypożyczenia = w.DataWypozyczenia,
                    DataZwrotu = w.DataZwrotu
                })
                .OrderByDescending(w => w.DataWypożyczenia);
            this.dataGridHistory.DataSource = borrows;

            /* niezwrócone wypożyczenia */
            var notReturned = borrows
                .Where(w => w.DataZwrotu == null)
                .Select(w => new
                {
                    w.Tytuł,
                    w.ISBN,
                    w.DataWypożyczenia
                });
            this.dataGridBorrowed.DataSource = notReturned;

            /* aktywne rezerwacje */
            var reservations = db.Rezerwacjas
                .Where(r => r.IdCzytelnika == idCzyt)
                .Select(r => new
                {
                    Tytuł = r.Wydanie.Tytul.NazwaTytulu,
                    ISBN = r.Wydanie.ISBN,
                    DataRezerwacji = r.DataRezerwacji
                });
            this.dataGridReservations.DataSource = reservations;
        }
    }
}
