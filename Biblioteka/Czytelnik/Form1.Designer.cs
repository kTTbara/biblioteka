﻿namespace Czytelnik
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabCtrl = new System.Windows.Forms.TabControl();
            this.tabListOfBooks = new System.Windows.Forms.TabPage();
            this.tabReaderDetails = new System.Windows.Forms.TabPage();
            this.tabBooksListXML = new System.Windows.Forms.TabPage();
            this.tabCtrl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCtrl
            // 
            this.tabCtrl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabCtrl.Controls.Add(this.tabListOfBooks);
            this.tabCtrl.Controls.Add(this.tabReaderDetails);
            this.tabCtrl.Controls.Add(this.tabBooksListXML);
            this.tabCtrl.Location = new System.Drawing.Point(2, 13);
            this.tabCtrl.Name = "tabCtrl";
            this.tabCtrl.SelectedIndex = 0;
            this.tabCtrl.Size = new System.Drawing.Size(978, 591);
            this.tabCtrl.TabIndex = 0;
            // 
            // tabListOfBooks
            // 
            this.tabListOfBooks.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabListOfBooks.Location = new System.Drawing.Point(4, 22);
            this.tabListOfBooks.Name = "tabListOfBooks";
            this.tabListOfBooks.Padding = new System.Windows.Forms.Padding(3);
            this.tabListOfBooks.Size = new System.Drawing.Size(970, 565);
            this.tabListOfBooks.TabIndex = 0;
            this.tabListOfBooks.Text = "Wykaz książek";
            this.tabListOfBooks.UseVisualStyleBackColor = true;
            // 
            // tabReaderDetails
            // 
            this.tabReaderDetails.Location = new System.Drawing.Point(4, 22);
            this.tabReaderDetails.Name = "tabReaderDetails";
            this.tabReaderDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabReaderDetails.Size = new System.Drawing.Size(970, 565);
            this.tabReaderDetails.TabIndex = 1;
            this.tabReaderDetails.Text = "Informacje o czytelniku";
            this.tabReaderDetails.UseVisualStyleBackColor = true;
            // 
            // tabBooksListXML
            // 
            this.tabBooksListXML.Location = new System.Drawing.Point(4, 22);
            this.tabBooksListXML.Name = "tabBooksListXML";
            this.tabBooksListXML.Padding = new System.Windows.Forms.Padding(3);
            this.tabBooksListXML.Size = new System.Drawing.Size(970, 565);
            this.tabBooksListXML.TabIndex = 2;
            this.tabBooksListXML.Text = "Lista książek (XML)";
            this.tabBooksListXML.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 608);
            this.Controls.Add(this.tabCtrl);
            this.Name = "Form1";
            this.Text = "Biblioteka: czytelnik";
            this.tabCtrl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCtrl;
        private System.Windows.Forms.TabPage tabListOfBooks;
        private System.Windows.Forms.TabPage tabReaderDetails;
        private System.Windows.Forms.TabPage tabBooksListXML;
    }
}

