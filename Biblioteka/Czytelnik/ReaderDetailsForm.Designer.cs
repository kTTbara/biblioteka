﻿namespace Czytelnik
{
    partial class ReaderDetailsForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.labelId = new System.Windows.Forms.Label();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.dataGridReadersCzyt = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridBorrowed = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridReservations = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridHistory = new System.Windows.Forms.DataGridView();
            this.labReaderInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridReadersCzyt)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBorrowed)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridReservations)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(130, 15);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(40, 20);
            this.textBoxID.TabIndex = 0;
            // 
            // labelId
            // 
            this.labelId.AutoSize = true;
            this.labelId.Location = new System.Drawing.Point(3, 18);
            this.labelId.Name = "labelId";
            this.labelId.Size = new System.Drawing.Size(121, 13);
            this.labelId.TabIndex = 1;
            this.labelId.Text = "Podaj swój identyfikator:";
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Location = new System.Drawing.Point(176, 13);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(137, 23);
            this.buttonConfirm.TabIndex = 1;
            this.buttonConfirm.Text = "Wyświetl informacje";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // dataGridReadersCzyt
            // 
            this.dataGridReadersCzyt.AllowUserToAddRows = false;
            this.dataGridReadersCzyt.AllowUserToDeleteRows = false;
            this.dataGridReadersCzyt.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridReadersCzyt.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridReadersCzyt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridReadersCzyt.Location = new System.Drawing.Point(13, 51);
            this.dataGridReadersCzyt.Name = "dataGridReadersCzyt";
            this.dataGridReadersCzyt.ReadOnly = true;
            this.dataGridReadersCzyt.Size = new System.Drawing.Size(631, 49);
            this.dataGridReadersCzyt.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridBorrowed);
            this.groupBox1.Location = new System.Drawing.Point(6, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(324, 170);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Niezwrócone książki";
            // 
            // dataGridBorrowed
            // 
            this.dataGridBorrowed.AllowUserToAddRows = false;
            this.dataGridBorrowed.AllowUserToDeleteRows = false;
            this.dataGridBorrowed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridBorrowed.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridBorrowed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridBorrowed.Location = new System.Drawing.Point(7, 20);
            this.dataGridBorrowed.Name = "dataGridBorrowed";
            this.dataGridBorrowed.ReadOnly = true;
            this.dataGridBorrowed.RowHeadersVisible = false;
            this.dataGridBorrowed.Size = new System.Drawing.Size(311, 144);
            this.dataGridBorrowed.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridReservations);
            this.groupBox2.Location = new System.Drawing.Point(4, 292);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(326, 136);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Aktywne rezerwacje";
            // 
            // dataGridReservations
            // 
            this.dataGridReservations.AllowUserToAddRows = false;
            this.dataGridReservations.AllowUserToDeleteRows = false;
            this.dataGridReservations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridReservations.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridReservations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridReservations.Location = new System.Drawing.Point(9, 20);
            this.dataGridReservations.Name = "dataGridReservations";
            this.dataGridReservations.ReadOnly = true;
            this.dataGridReservations.RowHeadersVisible = false;
            this.dataGridReservations.Size = new System.Drawing.Size(311, 110);
            this.dataGridReservations.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridHistory);
            this.groupBox3.Location = new System.Drawing.Point(336, 116);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(348, 312);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Historia wypożyczeń";
            // 
            // dataGridHistory
            // 
            this.dataGridHistory.AllowUserToAddRows = false;
            this.dataGridHistory.AllowUserToDeleteRows = false;
            this.dataGridHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridHistory.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridHistory.Location = new System.Drawing.Point(7, 20);
            this.dataGridHistory.Name = "dataGridHistory";
            this.dataGridHistory.ReadOnly = true;
            this.dataGridHistory.RowHeadersVisible = false;
            this.dataGridHistory.Size = new System.Drawing.Size(335, 286);
            this.dataGridHistory.TabIndex = 0;
            // 
            // labReaderInfo
            // 
            this.labReaderInfo.AutoSize = true;
            this.labReaderInfo.Location = new System.Drawing.Point(333, 18);
            this.labReaderInfo.Name = "labReaderInfo";
            this.labReaderInfo.Size = new System.Drawing.Size(95, 13);
            this.labReaderInfo.TabIndex = 6;
            this.labReaderInfo.Text = "<info o czytelniku>";
            // 
            // ReaderDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labReaderInfo);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridReadersCzyt);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(this.labelId);
            this.Controls.Add(this.textBoxID);
            this.Name = "ReaderDetailsForm";
            this.Size = new System.Drawing.Size(684, 435);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridReadersCzyt)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBorrowed)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridReservations)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridHistory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label labelId;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.DataGridView dataGridReadersCzyt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridBorrowed;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridReservations;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridHistory;
        private System.Windows.Forms.Label labReaderInfo;
    }
}
