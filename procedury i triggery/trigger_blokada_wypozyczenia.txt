-- zablokuj wypozyczenie, jesli czytelnik zwkleka ze zwrotem ksi��ek
-- tj. istniej� niezwr�cone wypozyczenia tego czytelnika,
-- dla kt�rych przekroczono termin zwrotu
CREATE TRIGGER blokada_wypozyczenia
ON Wypozyczenie
FOR INSERT
AS 
	IF EXISTS (
		SELECT w.IdWypozyczenia
		FROM Wypozyczenie w
		WHERE w.IdCzytelnika = (select IdCzytelnika from inserted)
			 and w.DataZwrotu is null
			 and DATEADD(day,w.LiczbaDni,w.DataWypozyczenia) < SYSDATETIME()
		)
	BEGIN
		ROLLBACK
	END
GO




